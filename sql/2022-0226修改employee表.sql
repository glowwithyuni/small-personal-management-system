/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : localhost:3306
 Source Schema         : spm

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 26/02/2022 16:42:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee`  (
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工id',
  `username` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `Idcard` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工身份证号码',
  `gender` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工性别',
  `role` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工权限',
  `politicaltype` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工政治面貌',
  `nationId` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工民族',
  `address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工地址',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工电话号码',
  `education` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工最高学历',
  `educationsubject` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工专业',
  `school` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工最高学历毕业院校',
  `departmentId` tinyint(11) NULL DEFAULT NULL COMMENT '员工所属部门id',
  `departmentName` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工所属部门名称',
  `workId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工工号',
  `workName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工职位',
  `baseSalary` double(8, 2) NULL DEFAULT NULL COMMENT '员工基础工资',
  `workAge` tinyint(3) NULL DEFAULT NULL COMMENT '员工工龄',
  `conversionTime` date NULL DEFAULT NULL COMMENT '员工转正时间',
  `outworkdate` date NULL DEFAULT NULL COMMENT '员工离职日期',
  `contractterm` double(4, 0) NULL DEFAULT NULL COMMENT '员工合同期限',
  `contractstart` date NULL DEFAULT NULL COMMENT '员工合同起始日期',
  `contractend` date NULL DEFAULT NULL COMMENT '员工合同终止日期',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES ('aaa', 'admin', '12313', '1', 'admin', '3', '2', '4', '6', '7', '8', '9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `employee` VALUES ('b', '打工人b', '181818181818181818', '男', 'user', '群众', '汉', '下北泽大街11栋45层14号', '11112131321', '本科', '计科', '华广', 2, '技术部', '1', 'JAVA全栈开发工程师', 8500.00, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `employee` VALUES ('c', 'dep_01', '5', '1', 'department', '3', '2', '4', '6', '7', '8', '9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for employee_request
-- ----------------------------
DROP TABLE IF EXISTS `employee_request`;
CREATE TABLE `employee_request`  (
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '请求员工id',
  `requestoftype` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求类型',
  `checkcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求码（重新设置密码用）',
  `isavailable` tinyint(1) NULL DEFAULT NULL COMMENT '1为未处理 0为处理',
  `requestcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '返回的初始密码',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employee_request
-- ----------------------------

-- ----------------------------
-- Table structure for employee_train
-- ----------------------------
DROP TABLE IF EXISTS `employee_train`;
CREATE TABLE `employee_train`  (
  `userId` int(12) NOT NULL,
  `userName` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gender` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `departmentId` int(11) NULL DEFAULT NULL,
  `departmentName` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `workName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `trainskill` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '培训内容',
  `traindate` date NULL DEFAULT NULL COMMENT '培训日期',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employee_train
-- ----------------------------

-- ----------------------------
-- Table structure for employeesalary
-- ----------------------------
DROP TABLE IF EXISTS `employeesalary`;
CREATE TABLE `employeesalary`  (
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工id',
  `userName` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `departmentId` tinyint(11) NULL DEFAULT NULL COMMENT '部门id',
  `workId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '工号',
  `workName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工职位',
  `baseSalary` double(8, 2) NULL DEFAULT NULL COMMENT '基本工资',
  `translationSalary` double(6, 2) NULL DEFAULT NULL COMMENT '交通补贴',
  `punishmentSalary` double(8, 2) NULL DEFAULT NULL COMMENT '罚款（迟到未打卡）',
  `rewardSalary` double(6, 2) NULL DEFAULT NULL COMMENT '奖励薪资',
  `totalSalary` double(10, 2) NULL DEFAULT NULL COMMENT '总薪资',
  `lastmodify` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上次修改薪资的hr（员工不可见）',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employeesalary
-- ----------------------------

-- ----------------------------
-- Table structure for journal_transaction
-- ----------------------------
DROP TABLE IF EXISTS `journal_transaction`;
CREATE TABLE `journal_transaction`  (
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `userName` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `usedate` date NULL DEFAULT NULL,
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of journal_transaction
-- ----------------------------

-- ----------------------------
-- Table structure for role_management
-- ----------------------------
DROP TABLE IF EXISTS `role_management`;
CREATE TABLE `role_management`  (
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `roleId` tinyint(4) NULL DEFAULT NULL,
  `mobilephone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remark` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_management
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工id',
  `password` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工账号密码',
  `token` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码token',
  `username` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `role` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限等级（user，departmenter，admin升序降权）',
  `departmentName` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('aaa', '123', NULL, 'admin', 'admin', NULL);
INSERT INTO `user` VALUES ('b', '11111', NULL, 'emp_01', 'user', NULL);
INSERT INTO `user` VALUES ('c', '45', NULL, 'dep_01', 'department', NULL);

-- ----------------------------
-- Table structure for work_attendance
-- ----------------------------
DROP TABLE IF EXISTS `work_attendance`;
CREATE TABLE `work_attendance`  (
  `userId` tinyint(12) NOT NULL,
  `userName` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `departmentName` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `workId` int(12) NULL DEFAULT NULL,
  `workName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `workdate` date NULL DEFAULT NULL,
  `ismark` tinyint(4) NULL DEFAULT 0 COMMENT '是否打卡签到',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of work_attendance
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
