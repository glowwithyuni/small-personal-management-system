/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : localhost:3306
 Source Schema         : spm

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 19/03/2022 22:13:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department`  (
  `departmentId` tinyint(11) NOT NULL COMMENT '员工所属部门id',
  `departmentName` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工所属部门名称',
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工id',
  `username` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `empNum` tinyint(8) NULL DEFAULT NULL COMMENT '部门人数',
  PRIMARY KEY (`departmentId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES (1, 'test啊啊', 'c', 'dep_012', 2);
INSERT INTO `department` VALUES (2, '123123123', 'test', 'tests', 1);
INSERT INTO `department` VALUES (3, 'tests', 'b1', 'emp_4', 0);
INSERT INTO `department` VALUES (4, '2', '5', '6', 1);
INSERT INTO `department` VALUES (6, '测试部', '9', '10223', 2);

-- ----------------------------
-- Table structure for department_emp
-- ----------------------------
DROP TABLE IF EXISTS `department_emp`;
CREATE TABLE `department_emp`  (
  `departmentId` tinyint(11) UNSIGNED NULL DEFAULT NULL COMMENT '员工所属部门id',
  `departmentName` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工所属部门名称',
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工id',
  `username` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `workId` tinyint(8) NULL DEFAULT NULL COMMENT '员工工号',
  `workName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工职位',
  `role` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工权限',
  `istrain` tinyint(2) NULL DEFAULT 0 COMMENT '是否去培训(1y/0n)',
  `iswork` tinyint(2) UNSIGNED NULL DEFAULT 0 COMMENT '是否分配部门（1y/0n）',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of department_emp
-- ----------------------------
INSERT INTO `department_emp` VALUES (4, 'a', '4', 'dadwd', 111, 'asdad', 'user', 0, 1);
INSERT INTO `department_emp` VALUES (6, '测试部', '8', '9', 2, '啊啊啊', 'user', 0, 1);
INSERT INTO `department_emp` VALUES (6, '测试部', '9', '10223', 1, '经理', 'department', 0, 1);
INSERT INTO `department_emp` VALUES (3, 'tests', 'b1', 'emp_4', 1, '1', 'department', 0, 1);
INSERT INTO `department_emp` VALUES (1, 'test啊啊', 'c', 'dep_012', 1, '13', 'department', 1, 1);
INSERT INTO `department_emp` VALUES (123, '1231313', 't', 'aaa', 1, '2', 'user', 0, 1);
INSERT INTO `department_emp` VALUES (2, '123123123', 'test', 'tests', 123, 'assda', 'department', 0, 1);

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee`  (
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工id',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `idcard` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工身份证号码',
  `gender` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工性别',
  `role` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工权限',
  `politicaltype` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工政治面貌',
  `nationId` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工民族',
  `address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工地址',
  `email` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工邮箱（找回密码用）',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工电话号码',
  `education` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工最高学历',
  `educationsubject` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工专业',
  `school` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工最高学历毕业院校',
  `departmentId` tinyint(8) NULL DEFAULT NULL COMMENT '员工所属部门id',
  `departmentName` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工所属部门名称',
  `workId` tinyint(8) NULL DEFAULT NULL COMMENT '员工工号',
  `workName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工职位',
  `baseSalary` double(8, 2) NULL DEFAULT NULL COMMENT '员工基础工资',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES ('4', 'dadwd', '181818181818181818', '男', 'user', '群众', '汉', '阿八八八', '654763560@qq.com', '12345678901', '本科', '计科', '华广', 4, 'a', 111, 'asdad', NULL);
INSERT INTO `employee` VALUES ('8', '9', NULL, '男', 'user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '测试部', 2, '啊啊啊', NULL);
INSERT INTO `employee` VALUES ('9', '10223', NULL, '男', 'department', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '测试部', 1, '经理', NULL);
INSERT INTO `employee` VALUES ('aaa', 'admin1112', '552', '男', 'admin', '334', '2223', '443221313', 'www.654763560@qq.com', '6613', '772', '882', '99142123123', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `employee` VALUES ('b', '打工人b', '181818181818181818', '男', 'user', '群众', '汉', '下北泽大街11栋45层14号', '1479679118@qq.com', '11112131321', '本科', '计科', '华广', 2, '123123123', 2, 'JAVA全栈开发工程师', 8500.00);
INSERT INTO `employee` VALUES ('c', 'dep_012', '5', '男', 'department', '3', '2', '4', NULL, '6', '7', '8', '9', 1, 'test啊啊', 1, '13', 0.00);
INSERT INTO `employee` VALUES ('d', '1', '2', '3', '4', '6', '6', NULL, '6', '6', '6', NULL, '6', 1, 'test啊啊', NULL, '', NULL);
INSERT INTO `employee` VALUES ('test', 'tests', NULL, '女', 'department', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for employee_request
-- ----------------------------
DROP TABLE IF EXISTS `employee_request`;
CREATE TABLE `employee_request`  (
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '请求员工id',
  `requestoftype` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求类型',
  `checkcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求码（重新设置密码用）',
  `isavailable` tinyint(1) NULL DEFAULT NULL COMMENT '1为未处理 0为处理',
  `requestcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '返回的初始密码',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employee_request
-- ----------------------------

-- ----------------------------
-- Table structure for employee_train
-- ----------------------------
DROP TABLE IF EXISTS `employee_train`;
CREATE TABLE `employee_train`  (
  `trainrage` int(20) NOT NULL AUTO_INCREMENT COMMENT '培训队列（实在找不到适合做主键的惹）',
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工Id',
  `username` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `gender` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工性别',
  `trainskill` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '培训内容',
  `trainstartdate` datetime(0) NULL DEFAULT NULL COMMENT '培训开始日期',
  `trainenddate` datetime(0) NULL DEFAULT NULL COMMENT '培训结束日期',
  `ischeck` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '是否被审批通过',
  `isfinish` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '是否完成培训',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '无' COMMENT '报表内容（滑稽）',
  PRIMARY KEY (`trainrage`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employee_train
-- ----------------------------
INSERT INTO `employee_train` VALUES (3, 'testtrain', 'tests', '男', 'study', '2022-03-14 15:43:43', '2022-03-14 15:43:58', '1', '1', 'done');
INSERT INTO `employee_train` VALUES (19, 'test', 'tests', '女', NULL, '2022-03-10 00:00:00', '2022-03-15 22:34:13', '1', '1', '121312312331312313123131313123');
INSERT INTO `employee_train` VALUES (20, 'c', 'dep_012', '男', NULL, NULL, NULL, '0', '0', '无');

-- ----------------------------
-- Table structure for employeesalary
-- ----------------------------
DROP TABLE IF EXISTS `employeesalary`;
CREATE TABLE `employeesalary`  (
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工id',
  `username` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `baseSalary` double(8, 2) NULL DEFAULT NULL COMMENT '基本工资',
  `translationSalary` double(6, 2) NULL DEFAULT NULL COMMENT '交通补贴',
  `punishmentSalary` double(8, 2) NULL DEFAULT NULL COMMENT '罚款（迟到未打卡）',
  `rewardSalary` double(6, 2) NULL DEFAULT NULL COMMENT '奖励薪资',
  `totalSalary` double(10, 2) NULL DEFAULT NULL COMMENT '总薪资',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employeesalary
-- ----------------------------

-- ----------------------------
-- Table structure for journal_transaction
-- ----------------------------
DROP TABLE IF EXISTS `journal_transaction`;
CREATE TABLE `journal_transaction`  (
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `userName` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `usedate` date NULL DEFAULT NULL,
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of journal_transaction
-- ----------------------------

-- ----------------------------
-- Table structure for role_management
-- ----------------------------
DROP TABLE IF EXISTS `role_management`;
CREATE TABLE `role_management`  (
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `roleId` tinyint(4) NULL DEFAULT NULL,
  `mobilephone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remark` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_management
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工id',
  `password` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工账号密码',
  `token` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码token',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `email` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工邮箱（找回密码用）',
  `role` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限等级（user，departmenter，admin升序降权）',
  `departmentName` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('4', '11111', NULL, 'dadwd', '654763560@qq.com', 'user', NULL);
INSERT INTO `user` VALUES ('8', '11111', NULL, '9', NULL, 'user', NULL);
INSERT INTO `user` VALUES ('9', '11111', NULL, '10223', NULL, 'department', NULL);
INSERT INTO `user` VALUES ('aaa', '123', NULL, 'admin1112', 'www.654763560@qq.com', 'admin', NULL);
INSERT INTO `user` VALUES ('b', '11111', NULL, '打工人b', '1479679118@qq.com', 'user', NULL);
INSERT INTO `user` VALUES ('b1', '11111', NULL, 'emp_4', 'www.654763562@qq.com', 'department', NULL);
INSERT INTO `user` VALUES ('b2', '11111', NULL, 'emp_02', 'www.654763560@qq.com', 'user', NULL);
INSERT INTO `user` VALUES ('b3', '11111', NULL, 'emp_03', 'www.654763560@qq.com', 'user', NULL);
INSERT INTO `user` VALUES ('b4', '11111', NULL, 'emp_04', 'www.654763560@qq.com', 'user', NULL);
INSERT INTO `user` VALUES ('c', '45', NULL, 'dep_012', 'www.654763560@qq.com', 'department', NULL);
INSERT INTO `user` VALUES ('test', 'test', NULL, 'tests', NULL, 'department', NULL);

-- ----------------------------
-- Table structure for work_attendance
-- ----------------------------
DROP TABLE IF EXISTS `work_attendance`;
CREATE TABLE `work_attendance`  (
  `attendance` int(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '考勤序列',
  `userId` tinyint(12) NOT NULL COMMENT '员工id',
  `userName` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `checkdate` date NULL DEFAULT NULL COMMENT '考勤时间',
  `checkstarttime` time(0) NULL DEFAULT NULL COMMENT '考勤起始时分秒',
  `checkendtime` time(0) NULL DEFAULT NULL COMMENT '考勤结束时分秒',
  `checkstartdate` datetime(0) NULL DEFAULT NULL COMMENT '考勤检查开始时间',
  `checkenddate` datetime(0) NULL DEFAULT NULL COMMENT '考勤检查结束时间',
  `reststarttime` time(0) NULL DEFAULT NULL COMMENT '下班起始时分秒',
  `restendtime` time(0) NULL DEFAULT NULL COMMENT '下班结束时分秒',
  `reststartdate` datetime(0) NULL DEFAULT NULL COMMENT '下班时间',
  `restenddate` datetime(0) NULL DEFAULT NULL COMMENT '超过该下班时间为加班',
  `attendcheckdate` datetime(0) NULL DEFAULT NULL COMMENT '上班打卡时间',
  `attendenddate` datetime(0) NULL DEFAULT NULL COMMENT '下班打卡时间',
  `checktype` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '考勤类别：0/未签到；1/正常；2/请假 3/迟到',
  `resttype` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '下班类别：0/未打卡/；1/正常；2/加班 3/早退',
  `createdate` datetime(0) NULL DEFAULT NULL COMMENT '创建考勤的时间',
  PRIMARY KEY (`attendance`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of work_attendance
-- ----------------------------
INSERT INTO `work_attendance` VALUES (1, 1, '2', '2022-03-16', '16:42:57', '16:46:39', '2022-03-16 16:46:42', '2022-03-17 16:46:46', NULL, NULL, NULL, NULL, '2022-03-16 16:46:44', NULL, '1', '0', '2022-03-16 16:46:55');
INSERT INTO `work_attendance` VALUES (2, 21, '2', '2022-03-16', '16:42:57', '16:46:39', '2022-03-16 16:46:42', '2022-03-17 16:46:46', NULL, NULL, NULL, NULL, '2022-03-16 16:46:44', NULL, '0', '1', '2022-03-16 16:46:55');
INSERT INTO `work_attendance` VALUES (3, 3, '4', '2022-03-19', '17:19:03', '17:23:08', '2022-03-19 17:19:03', '2022-03-19 17:23:08', '18:19:51', '18:30:59', '2022-03-19 18:19:51', '2022-03-19 18:30:59', '2022-03-19 17:20:08', '2022-03-19 18:30:51', '1', '1', '2022-03-19 17:15:26');
INSERT INTO `work_attendance` VALUES (4, 4, '5', '2022-03-19', '21:00:00', '21:10:00', '2022-03-19 21:00:00', '2022-03-19 21:10:00', '21:10:00', '21:20:00', '2022-03-19 21:10:00', '2022-03-19 21:20:00', NULL, NULL, '0', '0', '2022-03-19 21:11:50');

-- ----------------------------
-- View structure for emp_deptrain
-- ----------------------------
DROP VIEW IF EXISTS `emp_deptrain`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `emp_deptrain` AS select `department_emp`.`departmentId` AS `departmentId`,`department_emp`.`departmentName` AS `departmentName`,`department_emp`.`workId` AS `workId`,`department_emp`.`workName` AS `workName`,`employee_train`.`userId` AS `userId`,`employee_train`.`username` AS `userName`,`employee_train`.`trainskill` AS `trainskill`,`employee_train`.`trainstartdate` AS `trainstartdate`,`employee_train`.`trainenddate` AS `trainenddate`,`employee_train`.`remark` AS `remark` from (`department_emp` join `employee_train`) where (`department_emp`.`userId` = `employee_train`.`userId`);

-- ----------------------------
-- View structure for trainemp
-- ----------------------------
DROP VIEW IF EXISTS `trainemp`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `trainemp` AS select `department_emp`.`departmentId` AS `departmentId`,`department_emp`.`departmentName` AS `departmentName`,`department_emp`.`userId` AS `userId`,`department_emp`.`username` AS `username`,`employee`.`gender` AS `gender`,`department_emp`.`workId` AS `workId`,`department_emp`.`workName` AS `workName`,`department_emp`.`role` AS `role`,`department_emp`.`istrain` AS `istrain` from (`employee` join `department_emp`) where ((`employee`.`userId` = `department_emp`.`userId`) and (`department_emp`.`iswork` = 1) and (`department_emp`.`istrain` = 0));

SET FOREIGN_KEY_CHECKS = 1;
