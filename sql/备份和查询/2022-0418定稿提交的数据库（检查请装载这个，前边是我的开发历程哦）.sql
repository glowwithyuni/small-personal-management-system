/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : localhost:3306
 Source Schema         : spm

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 18/04/2022 16:56:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department`  (
  `departmentId` tinyint(11) NOT NULL COMMENT '员工所属部门id',
  `departmentName` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工所属部门名称',
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工id',
  `username` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `empNum` tinyint(8) NULL DEFAULT NULL COMMENT '部门人数',
  PRIMARY KEY (`departmentId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES (1, '财政部', '5', '1', 2);

-- ----------------------------
-- Table structure for department_emp
-- ----------------------------
DROP TABLE IF EXISTS `department_emp`;
CREATE TABLE `department_emp`  (
  `departmentId` tinyint(11) UNSIGNED NULL DEFAULT NULL COMMENT '员工所属部门id',
  `departmentName` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工所属部门名称',
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工id',
  `username` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `workId` int(20) NULL DEFAULT NULL COMMENT '员工工号',
  `workName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工职位',
  `role` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工权限',
  `istrain` tinyint(2) NULL DEFAULT 0 COMMENT '是否去培训(1y/0n)',
  `iswork` tinyint(2) NULL DEFAULT 0 COMMENT '是否分配部门（1y/0n）',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of department_emp
-- ----------------------------
INSERT INTO `department_emp` VALUES (1, '财政部', '1', 'test1', 3, '5', 'user', 1, 1);
INSERT INTO `department_emp` VALUES (1, '财政部', '2', 'test1', 4, '5', 'user', 0, 1);
INSERT INTO `department_emp` VALUES (1, '财政部', '4', 'dadwd', 2, '混钱的', 'user', 0, 1);
INSERT INTO `department_emp` VALUES (1, '财政部', '5', '1', 1, '财政委员', 'department', 0, 1);
INSERT INTO `department_emp` VALUES (NULL, NULL, '6', '2', NULL, NULL, 'user', 0, 0);
INSERT INTO `department_emp` VALUES (NULL, NULL, 'aaa', 'admin1112', NULL, NULL, 'admin', 0, 0);

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee`  (
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工id',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `idcard` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工身份证号码',
  `gender` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工性别',
  `role` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工权限',
  `politicaltype` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工政治面貌',
  `nationId` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工民族',
  `address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工地址',
  `email` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工邮箱（找回密码用）',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工电话号码',
  `education` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工最高学历',
  `educationsubject` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工专业',
  `school` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工最高学历毕业院校',
  `departmentId` tinyint(8) NULL DEFAULT NULL COMMENT '员工所属部门id',
  `departmentName` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工所属部门名称',
  `workId` tinyint(8) NULL DEFAULT NULL COMMENT '员工工号',
  `workName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工职位',
  `baseSalary` double(10, 2) NULL DEFAULT 0.00 COMMENT '员工基础工资',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES ('1', 'test1', '123456789123456789', '男', 'user', '群众', '汉', '华广', '1479679118@qq.com', '12345678901', '本科', '计科', '华广', 1, '财政部', 3, '5', 11111.00);
INSERT INTO `employee` VALUES ('2', 'test1', '123456789123456789', '男', 'user', '群众', '汉', '华广', '1479679118@qq.com', '12345678901', '本科', '计科', '华广', 1, '财政部', 4, '5', 13000.00);
INSERT INTO `employee` VALUES ('4', 'dadwd', '123456789123456789', '男', 'user', '群众', '汉', '华广', '1479679118@qq.com', '12345678901', '本科', '计科', '华广', 1, '财政部', 2, '混钱的', 6000.00);
INSERT INTO `employee` VALUES ('5', '1', '', '男', 'department', '', '', '', '1479679118@qq.com', '', '', '', '', 1, '财政部', 1, '财政委员', 22222.00);
INSERT INTO `employee` VALUES ('6', '2', NULL, NULL, 'user', NULL, NULL, NULL, '123456789@qq.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13000.00);
INSERT INTO `employee` VALUES ('aaa', 'admin1112', '123456789123456789', '男', 'admin', '群众', '汉', '华广', 'www.654763560@qq.com', '1234567890', '本科', '计算机科学与技术', '华广', NULL, NULL, NULL, NULL, 0.00);

-- ----------------------------
-- Table structure for employee_request
-- ----------------------------
DROP TABLE IF EXISTS `employee_request`;
CREATE TABLE `employee_request`  (
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '请求员工id',
  `requestoftype` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求类型',
  `checkcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求码（重新设置密码用）',
  `isavailable` tinyint(1) NULL DEFAULT NULL COMMENT '1为未处理 0为处理',
  `requestcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '返回的初始密码',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employee_request
-- ----------------------------
INSERT INTO `employee_request` VALUES ('1', 'resetpassword', '123', 0, '99p6s0jx67');

-- ----------------------------
-- Table structure for employee_salary
-- ----------------------------
DROP TABLE IF EXISTS `employee_salary`;
CREATE TABLE `employee_salary`  (
  `salaryOrder` int(20) NOT NULL AUTO_INCREMENT COMMENT '薪资序列',
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工id',
  `username` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `role` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色权限',
  `salaryMonth` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '薪资月份',
  `baseSalary` double(10, 2) NULL DEFAULT 0.00 COMMENT '月基础工资',
  `allAttendMoney` double(10, 2) NULL DEFAULT 0.00 COMMENT '月全勤奖励',
  `runAttendanceMoney` double(10, 2) NULL DEFAULT 0.00 COMMENT '早退罚金',
  `restAttendanceMoney` double(10, 2) NULL DEFAULT 0.00 COMMENT '请假罚金',
  `lateAttendanceMoney` double(10, 2) NULL DEFAULT 0.00 COMMENT '迟到罚金',
  `absentAttendanceMoney` double(10, 2) NULL DEFAULT 0.00 COMMENT '缺勤罚金',
  `workMoney` double(10, 2) NULL DEFAULT 0.00 COMMENT '加班奖励',
  `transportSalary` double(10, 2) NULL DEFAULT 0.00 COMMENT '培训车旅费',
  `taxMoney` double(10, 2) NULL DEFAULT 0.00 COMMENT '税金',
  `onTimes` int(10) NULL DEFAULT 0 COMMENT '正常次数',
  `runAttendanceTimes` int(10) NULL DEFAULT 0 COMMENT '早退次数',
  `restAttendanceTimes` int(10) NULL DEFAULT 0 COMMENT '请假次数',
  `lateAttendanceTimes` int(10) NULL DEFAULT 0 COMMENT '迟到次数',
  `absentAttendanceTimes` int(10) NULL DEFAULT 0 COMMENT '缺勤次数',
  `workTimes` int(10) NULL DEFAULT 0 COMMENT '加班时长',
  `totalMoney` double(10, 2) NULL DEFAULT 0.00 COMMENT '月总结算金额',
  `issearch` int(1) NULL DEFAULT 0 COMMENT '是否被年度记录',
  PRIMARY KEY (`salaryOrder`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employee_salary
-- ----------------------------
INSERT INTO `employee_salary` VALUES (1, '5', '1', 'department', '2022', 22222.00, 1333.32, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 3533.30, 0, 0, 0, 0, 0, 0, 20022.02, 1);
INSERT INTO `employee_salary` VALUES (2, 'aaa', 'admin1112', 'admin', '2022', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0.00, 1);
INSERT INTO `employee_salary` VALUES (3, '5', '1', 'department', '2022-04', 22222.00, 1333.32, 0.00, 0.00, 0.00, 0.00, 0.00, 123.00, 3533.30, 3, 0, 0, 0, 0, 0, 20022.02, 1);
INSERT INTO `employee_salary` VALUES (4, '4', 'dadwd', 'user', '2022-04', 6000.00, 90.00, 0.00, 1636.36, 60.00, 480.00, 0.00, 135.00, 0.00, 2, 0, 3, 2, 4, 0, 3913.64, 1);
INSERT INTO `employee_salary` VALUES (7, '6', '2', 'department', '2022-04', 33333.00, 0.00, 0.00, 0.00, 0.00, 666.66, 0.00, 0.00, 6533.27, 0, 0, 0, 0, 1, 0, 26133.07, 1);
INSERT INTO `employee_salary` VALUES (8, '6', '2', 'user', '2022', 46333.00, 0.00, 0.00, 0.00, 0.00, 926.66, 0.00, 0.00, 7679.87, 0, 0, 0, 0, 2, 0, 37726.47, 1);
INSERT INTO `employee_salary` VALUES (9, '6', '2', 'department', '2022-03', 13000.00, 0.00, 0.00, 0.00, 0.00, 260.00, 0.00, 0.00, 1146.60, 0, 0, 0, 0, 1, 0, 11593.40, 1);
INSERT INTO `employee_salary` VALUES (10, '4', 'dadwd', 'user', '2021', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0.00, 1);
INSERT INTO `employee_salary` VALUES (11, '5', '1', 'department', '2021', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0.00, 1);
INSERT INTO `employee_salary` VALUES (12, '6', '2', 'department', '2021', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0.00, 1);
INSERT INTO `employee_salary` VALUES (13, 'aaa', 'admin1112', 'admin', '2021', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 0, 0, 0.00, 1);
INSERT INTO `employee_salary` VALUES (16, '4', 'dadwd', 'user', '2020-12', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 1, 0, 0.00, 1);
INSERT INTO `employee_salary` VALUES (18, '4', 'dadwd', 'user', '2020', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0, 0, 0, 1, 0, 0.00, 1);
INSERT INTO `employee_salary` VALUES (22, '4', 'dadwd', 'user', '2022', 6000.00, 90.00, 0.00, 1636.36, 60.00, 480.00, 0.00, 135.00, 0.00, 2, 0, 3, 2, 4, 0, 3913.64, 1);
INSERT INTO `employee_salary` VALUES (23, '1', 'test1', 'user', '2022-04', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 300.00, 0.00, 0, 0, 0, 0, 0, 0, 0.00, 1);
INSERT INTO `employee_salary` VALUES (24, '1', 'test1', 'user', '2022', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 300.00, 0.00, 0, 0, 0, 0, 0, 0, 0.00, 1);

-- ----------------------------
-- Table structure for employee_train
-- ----------------------------
DROP TABLE IF EXISTS `employee_train`;
CREATE TABLE `employee_train`  (
  `trainrage` int(20) NOT NULL AUTO_INCREMENT COMMENT '培训队列（实在找不到适合做主键的惹）',
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工Id',
  `username` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `gender` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工性别',
  `trainskill` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '培训内容',
  `trainstartdate` datetime(0) NULL DEFAULT NULL COMMENT '培训开始日期',
  `trainenddate` datetime(0) NULL DEFAULT NULL COMMENT '培训结束日期',
  `transportSalary` double(10, 2) NULL DEFAULT 0.00 COMMENT '培训车旅费',
  `ischeck` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '是否被审批通过',
  `isfinish` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '是否完成培训',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '无' COMMENT '报表内容（滑稽）',
  `isadd` int(1) NULL DEFAULT 0 COMMENT '是否被薪资审核',
  PRIMARY KEY (`trainrage`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 48 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employee_train
-- ----------------------------
INSERT INTO `employee_train` VALUES (34, '5', '1', '男', NULL, '2022-04-06 10:58:16', '2022-04-06 10:58:39', 123.00, '1', '1', '无', 1);
INSERT INTO `employee_train` VALUES (35, '4', 'dadwd', '男', NULL, '2022-04-06 10:58:58', '2022-04-06 10:59:01', 12.00, '1', '1', '无', 1);
INSERT INTO `employee_train` VALUES (45, '4', 'dadwd', '男', NULL, '2022-04-06 15:48:30', '2022-04-06 15:53:50', 123.00, '1', '1', '无', 1);
INSERT INTO `employee_train` VALUES (48, '1', 'test1', '男', NULL, '2022-04-16 14:19:58', NULL, 300.00, '1', '0', '无', 1);

-- ----------------------------
-- Table structure for jobneed
-- ----------------------------
DROP TABLE IF EXISTS `jobneed`;
CREATE TABLE `jobneed`  (
  `jobrage` int(20) NOT NULL AUTO_INCREMENT COMMENT '招聘序列',
  `departmentId` tinyint(11) NULL DEFAULT NULL COMMENT '员工所属部门id',
  `departmentName` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工所属部门名称',
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工id',
  `username` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `workName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工职位',
  `baseSalary` double(10, 2) NULL DEFAULT 0.00 COMMENT '员工基础工资',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '职位描述',
  `needNum` smallint(8) NULL DEFAULT NULL COMMENT '岗位需求人数',
  PRIMARY KEY (`jobrage`) USING BTREE,
  UNIQUE INDEX `workName`(`workName`) USING BTREE COMMENT '职位唯一'
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jobneed
-- ----------------------------
INSERT INTO `jobneed` VALUES (1, 1, '财政部', '3', '4', '5', 13000.00, 'sssss', 5);
INSERT INTO `jobneed` VALUES (3, 1, '财政部', '5', '1', 'test', 0.00, 'abababa', 2);
INSERT INTO `jobneed` VALUES (6, 1, '财政部', '5', '1', '混工资的啊', 3200.00, '年纪轻轻月薪到3200，得到这份工作你的眼界会很提高哦~啦啦啦啦', 2);

-- ----------------------------
-- Table structure for journal_transaction
-- ----------------------------
DROP TABLE IF EXISTS `journal_transaction`;
CREATE TABLE `journal_transaction`  (
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工Id',
  `userName` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `content` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志内容',
  `usedate` date NULL DEFAULT NULL COMMENT '记录日期',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of journal_transaction
-- ----------------------------

-- ----------------------------
-- Table structure for role_management
-- ----------------------------
DROP TABLE IF EXISTS `role_management`;
CREATE TABLE `role_management`  (
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `roleId` tinyint(4) NULL DEFAULT NULL,
  `mobilephone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remark` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_management
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工id',
  `password` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工账号密码',
  `token` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码token',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `email` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工邮箱（找回密码用）',
  `role` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限等级（user，departmenter，admin升序降权）',
  `departmentName` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '99p6s0jx67', 'asdadwdwa', 'test1', '1479679118@qq.com', 'user', NULL);
INSERT INTO `user` VALUES ('2', 'ihba3d31e9', NULL, 'test1', '1479679118@qq.com', 'user', NULL);
INSERT INTO `user` VALUES ('4', '11111', NULL, 'dadwd', '1479679118@qq.com', 'user', NULL);
INSERT INTO `user` VALUES ('5', '11111', 'yc3jp5dxg2bis44', '1', '1479679118@qq.com', 'department', NULL);
INSERT INTO `user` VALUES ('6', '11111', NULL, '2', '1479679118@qq.com', 'user', NULL);
INSERT INTO `user` VALUES ('7', '11110', NULL, '3', '1479679118@qq.com', 'user', NULL);
INSERT INTO `user` VALUES ('aaa', '123', 'uwjf4ixfdn3yw1d', 'admin1112', 'www.654763560@qq.com', 'admin', NULL);

-- ----------------------------
-- Table structure for wanted
-- ----------------------------
DROP TABLE IF EXISTS `wanted`;
CREATE TABLE `wanted`  (
  `wantedrage` int(12) NOT NULL AUTO_INCREMENT COMMENT '应聘序列',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `idcard` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工身份证号码',
  `gender` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工性别',
  `politicaltype` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工政治面貌',
  `nationId` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工民族',
  `address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工地址',
  `email` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工邮箱（找回密码用）',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工电话号码',
  `education` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工最高学历',
  `educationsubject` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工专业',
  `school` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工最高学历毕业院校',
  `departmentId` tinyint(8) NULL DEFAULT NULL COMMENT '员工所属部门id',
  `departmentName` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工所属部门名称',
  `workName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工职位',
  `baseSalary` double(10, 2) NULL DEFAULT 0.00 COMMENT '员工基础工资',
  `workExperience` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '求职工作经历、项目介绍',
  `isaccept` smallint(4) NULL DEFAULT 0 COMMENT '是否审批完毕（0/未审核 1/面试成功 2/审核通过正在面试 3/面试失败打包回家）',
  `wantedDate` datetime(0) NULL DEFAULT NULL COMMENT '求职时间',
  `getDate` datetime(0) NULL DEFAULT NULL COMMENT '入职时间',
  PRIMARY KEY (`wantedrage`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wanted
-- ----------------------------
INSERT INTO `wanted` VALUES (10, 'test1', '123456789123456789', '男', '群众', '汉', '华广', '1479679118@qq.com', '12345678901', '本科', '计科', '华广', 1, '财政部', '5', 11111.00, '阿巴阿巴阿巴。', 3, '2022-04-16 01:17:40', '2022-04-16 10:02:51');
INSERT INTO `wanted` VALUES (11, 'test1', '123456789123456789', '男', '群众', '汉', '华广', '1479679118@qq.com', '12345678901', '本科', '计科', '华广', 1, '财政部', '5', 13000.00, 'zaaa', 3, '2022-04-16 14:45:15', '2022-04-16 14:48:22');

-- ----------------------------
-- Table structure for work_attendance
-- ----------------------------
DROP TABLE IF EXISTS `work_attendance`;
CREATE TABLE `work_attendance`  (
  `attendance` int(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '考勤序列',
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工id',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `checkdate` date NULL DEFAULT NULL COMMENT '考勤时间',
  `checkstarttime` time(0) NULL DEFAULT NULL COMMENT '考勤起始时分秒',
  `checkendtime` time(0) NULL DEFAULT NULL COMMENT '考勤结束时分秒',
  `checkstartdate` datetime(0) NULL DEFAULT NULL COMMENT '考勤检查开始时间',
  `checkenddate` datetime(0) NULL DEFAULT NULL COMMENT '考勤检查结束时间',
  `attendcheckdate` datetime(0) NULL DEFAULT NULL COMMENT '上班打卡时间',
  `checktype` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '考勤类别：0/未签到；1/正常；2/请假 3/迟到 4/缺勤 5/早退 6/加班',
  `createdate` datetime(0) NULL DEFAULT NULL COMMENT '创建考勤的时间',
  `attendtype` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上班/下班',
  `workhour` int(10) NULL DEFAULT 0 COMMENT '加班时长（H）',
  `issearch` int(1) NULL DEFAULT 0 COMMENT '是否被记录薪资中',
  PRIMARY KEY (`attendance`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of work_attendance
-- ----------------------------
INSERT INTO `work_attendance` VALUES (1, '4', 'dadwd', '2022-04-06', '16:01:37', '16:06:43', '2022-04-06 16:01:37', '2022-04-06 16:06:43', '2022-04-06 16:02:22', '1', '2022-04-06 16:01:53', '上班', 0, 1);
INSERT INTO `work_attendance` VALUES (2, '5', '1', '2022-04-06', '16:01:37', '16:06:43', '2022-04-06 16:01:37', '2022-04-06 16:06:43', '2022-04-06 16:02:24', '1', '2022-04-06 16:01:53', '上班', 0, 1);
INSERT INTO `work_attendance` VALUES (4, '6', '2', '2022-04-06', '22:20:54', '22:20:57', '2022-04-06 22:20:54', '2022-04-06 22:20:57', NULL, '4', '2022-04-08 22:21:58', '上班', 0, 1);
INSERT INTO `work_attendance` VALUES (5, '6', '2', '2022-03-06', '22:24:42', '00:00:00', '2022-04-06 22:24:42', '2022-04-07 00:00:00', NULL, '4', '2022-04-08 22:25:05', '上班', 0, 1);
INSERT INTO `work_attendance` VALUES (6, '4', 'dadwd', '2022-04-09', '00:21:07', '00:29:10', '2022-04-09 00:21:07', '2022-04-09 00:29:10', '2022-04-09 00:22:46', '1', '2022-04-09 00:21:21', '上班', 0, 1);
INSERT INTO `work_attendance` VALUES (7, '5', '1', '2022-04-09', '00:21:07', '00:29:10', '2022-04-09 00:21:07', '2022-04-09 00:29:10', '2022-04-09 00:22:48', '1', '2022-04-09 00:21:21', '上班', 0, 1);
INSERT INTO `work_attendance` VALUES (8, '4', 'dadwd', '2022-04-09', '15:22:36', '15:23:38', '2022-04-09 15:22:36', '2022-04-09 15:23:38', '2022-04-09 15:22:57', '2', '2022-04-09 15:22:45', '上班', 0, 1);
INSERT INTO `work_attendance` VALUES (9, '5', '1', '2022-04-09', '15:22:36', '15:23:38', '2022-04-09 15:22:36', '2022-04-09 15:23:38', '2022-04-09 15:23:00', '1', '2022-04-09 15:22:45', '上班', 0, 1);
INSERT INTO `work_attendance` VALUES (10, '4', 'dadwd', '2020-12-31', '20:45:53', '23:00:00', '2020-12-31 20:45:53', '2020-12-31 23:00:00', NULL, '4', '2022-04-09 20:46:32', '上班', 0, 1);
INSERT INTO `work_attendance` VALUES (11, '4', 'dadwd', '2022-04-10', '04:23:42', '06:22:53', '2022-04-10 04:23:42', '2022-04-10 06:22:53', '2022-04-10 04:25:01', '2', '2022-04-10 04:24:55', '上班', 0, 1);
INSERT INTO `work_attendance` VALUES (12, '4', 'dadwd', '2022-04-12', '16:23:34', '16:25:36', '2022-04-12 16:23:34', '2022-04-12 16:25:36', '2022-04-12 16:24:09', '2', '2022-04-12 16:23:50', '上班', 0, 1);
INSERT INTO `work_attendance` VALUES (13, '4', 'dadwd', '2022-04-12', '18:36:44', '18:36:45', '2022-04-12 18:36:44', '2022-04-12 18:36:45', '2022-04-12 18:36:53', '3', '2022-04-12 18:36:48', '上班', 0, 1);
INSERT INTO `work_attendance` VALUES (14, '4', 'dadwd', '2022-04-12', '18:52:38', '18:52:40', '2022-04-12 18:52:38', '2022-04-12 18:52:40', NULL, '4', '2022-04-12 18:52:45', '上班', 0, 1);
INSERT INTO `work_attendance` VALUES (15, '4', 'dadwd', '2022-04-12', '19:03:53', '19:03:55', '2022-04-12 19:03:53', '2022-04-12 19:03:55', NULL, '4', '2022-04-12 19:03:58', '上班', 0, 1);
INSERT INTO `work_attendance` VALUES (16, '4', 'dadwd', '2022-04-12', '19:15:52', '19:15:55', '2022-04-12 19:15:52', '2022-04-12 19:15:55', NULL, '4', '2022-04-12 19:16:09', '上班', 0, 1);
INSERT INTO `work_attendance` VALUES (17, '4', 'dadwd', '2022-04-12', '21:05:30', '21:05:32', '2022-04-12 21:05:30', '2022-04-12 21:05:32', NULL, '4', '2022-04-12 21:05:37', '上班', 0, 1);
INSERT INTO `work_attendance` VALUES (18, '4', 'dadwd', '2022-04-12', '21:51:19', '21:51:20', '2022-04-12 21:51:19', '2022-04-12 21:51:20', '2022-04-12 21:54:30', '3', '2022-04-12 21:51:24', '上班', 0, 1);
INSERT INTO `work_attendance` VALUES (19, '1', 'test1', '2022-04-16', '14:17:10', '14:39:15', '2022-04-16 14:17:10', '2022-04-16 14:39:15', NULL, '0', '2022-04-16 14:17:36', '上班', 0, 0);
INSERT INTO `work_attendance` VALUES (20, '4', 'dadwd', '2022-04-16', '14:17:10', '14:39:15', '2022-04-16 14:17:10', '2022-04-16 14:39:15', NULL, '0', '2022-04-16 14:17:36', '上班', 0, 0);
INSERT INTO `work_attendance` VALUES (21, '5', '1', '2022-04-16', '14:17:10', '14:39:15', '2022-04-16 14:17:10', '2022-04-16 14:39:15', NULL, '0', '2022-04-16 14:17:36', '上班', 0, 0);
INSERT INTO `work_attendance` VALUES (22, '6', '2', '2022-04-16', '14:17:10', '14:39:15', '2022-04-16 14:17:10', '2022-04-16 14:39:15', NULL, '0', '2022-04-16 14:17:36', '上班', 0, 0);

-- ----------------------------
-- View structure for emp_deptrain
-- ----------------------------
DROP VIEW IF EXISTS `emp_deptrain`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `emp_deptrain` AS select `department_emp`.`departmentId` AS `departmentId`,`department_emp`.`departmentName` AS `departmentName`,`department_emp`.`workId` AS `workId`,`department_emp`.`workName` AS `workName`,`employee_train`.`userId` AS `userId`,`employee_train`.`username` AS `userName`,`employee_train`.`trainskill` AS `trainskill`,`employee_train`.`trainstartdate` AS `trainstartdate`,`employee_train`.`trainenddate` AS `trainenddate`,`employee_train`.`remark` AS `remark` from (`department_emp` join `employee_train`) where (`department_emp`.`userId` = `employee_train`.`userId`);

-- ----------------------------
-- View structure for trainemp
-- ----------------------------
DROP VIEW IF EXISTS `trainemp`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `trainemp` AS select `department_emp`.`departmentId` AS `departmentId`,`department_emp`.`departmentName` AS `departmentName`,`department_emp`.`userId` AS `userId`,`department_emp`.`username` AS `username`,`employee`.`gender` AS `gender`,`department_emp`.`workId` AS `workId`,`department_emp`.`workName` AS `workName`,`department_emp`.`role` AS `role`,`department_emp`.`istrain` AS `istrain` from (`employee` join `department_emp`) where ((`employee`.`userId` = `department_emp`.`userId`) and (`department_emp`.`iswork` = 1) and (`department_emp`.`istrain` = 0));

SET FOREIGN_KEY_CHECKS = 1;
