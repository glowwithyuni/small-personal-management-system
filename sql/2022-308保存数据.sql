/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : localhost:3306
 Source Schema         : spm

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 08/03/2022 22:53:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department`  (
  `departmentId` tinyint(11) NOT NULL COMMENT '员工所属部门id',
  `departmentName` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工所属部门名称',
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工id',
  `username` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `empNum` tinyint(8) NULL DEFAULT NULL COMMENT '部门人数',
  PRIMARY KEY (`departmentId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES (1, 'test', 'c', 'dep_01', 2);
INSERT INTO `department` VALUES (2, '123123123', 'test', 'tests', 1);
INSERT INTO `department` VALUES (3, 'tests', 'b1', 'emp_4', 0);
INSERT INTO `department` VALUES (4, '2', '5', '6', 1);
INSERT INTO `department` VALUES (6, '7', '8', '9', 2);

-- ----------------------------
-- Table structure for department_emp
-- ----------------------------
DROP TABLE IF EXISTS `department_emp`;
CREATE TABLE `department_emp`  (
  `departmentId` tinyint(11) UNSIGNED NULL DEFAULT NULL COMMENT '员工所属部门id',
  `departmentName` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工所属部门名称',
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工id',
  `username` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `workId` tinyint(8) NULL DEFAULT NULL COMMENT '员工工号',
  `workName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工职位',
  `role` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工权限',
  `istrain` tinyint(2) NULL DEFAULT 0 COMMENT '是否去培训(1y/0n)',
  `iswork` tinyint(2) UNSIGNED NULL DEFAULT 0 COMMENT '是否分配部门（1y/0n）',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of department_emp
-- ----------------------------
INSERT INTO `department_emp` VALUES (4, 'a', '4', 'dadwd', 111, 'asdad', 'user', 0, 1);
INSERT INTO `department_emp` VALUES (6, '7', '8', '9', 10, '1', 'department', 0, 1);
INSERT INTO `department_emp` VALUES (6, '7', '9', '10', 11, '2', 'user', 0, 1);
INSERT INTO `department_emp` VALUES (3, 'tests', 'b1', 'emp_4', 1, '1', 'department', 0, 1);
INSERT INTO `department_emp` VALUES (1, 'test', 'c', 'dep_01', 1, '1', 'department', 0, 1);
INSERT INTO `department_emp` VALUES (5, 'test5', 'sb', '123', 2, '1', '3', 0, 1);
INSERT INTO `department_emp` VALUES (123, '1231313', 't', 'aaa', 1, '2', '3', 0, 0);
INSERT INTO `department_emp` VALUES (2, '123123123', 'test', 'tests', 123, 'assda', 'department', 0, 123);

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee`  (
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工id',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `idcard` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工身份证号码',
  `gender` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工性别',
  `role` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工权限',
  `politicaltype` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工政治面貌',
  `nationId` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工民族',
  `address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工地址',
  `email` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工邮箱（找回密码用）',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工电话号码',
  `education` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工最高学历',
  `educationsubject` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工专业',
  `school` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工最高学历毕业院校',
  `departmentId` tinyint(8) NULL DEFAULT NULL COMMENT '员工所属部门id',
  `departmentName` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工所属部门名称',
  `workId` tinyint(8) NULL DEFAULT NULL COMMENT '员工工号',
  `workName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工职位',
  `baseSalary` double(8, 2) NULL DEFAULT NULL COMMENT '员工基础工资',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES ('4', 'dadwd', '181818181818181818', '男', 'user', '群众', '汉', '阿八八八', '654763560@qq.com', '12345678901', '本科', '计科', '华广', 4, 'a', 111, 'asdad', NULL);
INSERT INTO `employee` VALUES ('8', '9', NULL, NULL, 'department', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '7', 10, '1', NULL);
INSERT INTO `employee` VALUES ('9', '10', NULL, NULL, 'user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, '7', 11, '2', NULL);
INSERT INTO `employee` VALUES ('aaa', 'admin1112', '552', '男', 'admin', '334', '2223', '443221313', 'www.654763560@qq.com', '6613', '772', '882', '99142123123', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `employee` VALUES ('b', '打工人b', '181818181818181818', '男', 'user', '群众', '汉', '下北泽大街11栋45层14号', '1479679118@qq.com', '11112131321', '本科', '计科', '华广', 2, '123123123', 2, 'JAVA全栈开发工程师', 8500.00);
INSERT INTO `employee` VALUES ('c', 'dep_01', '5', '男', 'department', '3', '2', '4', NULL, '6', '7', '8', '9', 1, 'test', 1, '1', 0.00);
INSERT INTO `employee` VALUES ('d', '1', '2', '3', '4', '6', '6', NULL, '6', '6', '6', NULL, '6', 1, 'test', NULL, '', NULL);
INSERT INTO `employee` VALUES ('test', 'tests', NULL, NULL, 'department', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, '123123123', 123, 'assda', NULL);

-- ----------------------------
-- Table structure for employee_request
-- ----------------------------
DROP TABLE IF EXISTS `employee_request`;
CREATE TABLE `employee_request`  (
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '请求员工id',
  `requestoftype` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求类型',
  `checkcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求码（重新设置密码用）',
  `isavailable` tinyint(1) NULL DEFAULT NULL COMMENT '1为未处理 0为处理',
  `requestcode` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '返回的初始密码',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employee_request
-- ----------------------------

-- ----------------------------
-- Table structure for employee_train
-- ----------------------------
DROP TABLE IF EXISTS `employee_train`;
CREATE TABLE `employee_train`  (
  `trainrage` int(20) NOT NULL AUTO_INCREMENT COMMENT '培训队列（实在找不到适合做主键的惹）',
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工Id',
  `userName` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `gender` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工性别',
  `trainskill` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '培训内容',
  `trainstartdate` datetime(0) NULL DEFAULT NULL COMMENT '培训开始日期',
  `trainenddate` datetime(0) NULL DEFAULT NULL COMMENT '培训结束日期',
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '报表内容（滑稽）',
  PRIMARY KEY (`trainrage`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employee_train
-- ----------------------------
INSERT INTO `employee_train` VALUES (1, '1', '2', '男', '8', '2022-03-05 00:00:00', '2022-03-06 00:00:00', '1');
INSERT INTO `employee_train` VALUES (2, '4', 'dadwd', '男', 'test', '2022-03-05 09:26:31', '2022-03-05 09:26:33', '21231');

-- ----------------------------
-- Table structure for employeesalary
-- ----------------------------
DROP TABLE IF EXISTS `employeesalary`;
CREATE TABLE `employeesalary`  (
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工id',
  `username` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `baseSalary` double(8, 2) NULL DEFAULT NULL COMMENT '基本工资',
  `translationSalary` double(6, 2) NULL DEFAULT NULL COMMENT '交通补贴',
  `punishmentSalary` double(8, 2) NULL DEFAULT NULL COMMENT '罚款（迟到未打卡）',
  `rewardSalary` double(6, 2) NULL DEFAULT NULL COMMENT '奖励薪资',
  `totalSalary` double(10, 2) NULL DEFAULT NULL COMMENT '总薪资',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employeesalary
-- ----------------------------

-- ----------------------------
-- Table structure for journal_transaction
-- ----------------------------
DROP TABLE IF EXISTS `journal_transaction`;
CREATE TABLE `journal_transaction`  (
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `userName` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `usedate` date NULL DEFAULT NULL,
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of journal_transaction
-- ----------------------------

-- ----------------------------
-- Table structure for role_management
-- ----------------------------
DROP TABLE IF EXISTS `role_management`;
CREATE TABLE `role_management`  (
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `roleId` tinyint(4) NULL DEFAULT NULL,
  `mobilephone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remark` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_management
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `userId` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工id',
  `password` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工账号密码',
  `token` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码token',
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工姓名',
  `email` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工邮箱（找回密码用）',
  `role` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限等级（user，departmenter，admin升序降权）',
  `departmentName` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('4', '11111', NULL, 'dadwd', '654763560@qq.com', 'user', NULL);
INSERT INTO `user` VALUES ('8', '11111', NULL, '9', NULL, 'department', NULL);
INSERT INTO `user` VALUES ('9', '11111', NULL, '10', NULL, 'user', NULL);
INSERT INTO `user` VALUES ('aaa', '123', NULL, 'admin1112', 'www.654763560@qq.com', 'admin', NULL);
INSERT INTO `user` VALUES ('b', '11111', NULL, '打工人b', '1479679118@qq.com', 'user', NULL);
INSERT INTO `user` VALUES ('b1', '11111', NULL, 'emp_4', 'www.654763562@qq.com', 'department', NULL);
INSERT INTO `user` VALUES ('b2', '11111', NULL, 'emp_02', 'www.654763560@qq.com', 'user', NULL);
INSERT INTO `user` VALUES ('b3', '11111', NULL, 'emp_03', 'www.654763560@qq.com', 'user', NULL);
INSERT INTO `user` VALUES ('b4', '11111', NULL, 'emp_04', 'www.654763560@qq.com', 'user', NULL);
INSERT INTO `user` VALUES ('c', '45', NULL, 'dep_01', 'www.654763560@qq.com', 'department', NULL);
INSERT INTO `user` VALUES ('test', 'test', NULL, 'tests', NULL, 'department', NULL);

-- ----------------------------
-- Table structure for work_attendance
-- ----------------------------
DROP TABLE IF EXISTS `work_attendance`;
CREATE TABLE `work_attendance`  (
  `userId` tinyint(12) NOT NULL,
  `userName` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `departmentName` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `workId` int(12) NULL DEFAULT NULL,
  `workName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `workdate` date NULL DEFAULT NULL,
  `ismark` tinyint(4) NULL DEFAULT 0 COMMENT '是否打卡签到',
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of work_attendance
-- ----------------------------

-- ----------------------------
-- View structure for emp_deptrain
-- ----------------------------
DROP VIEW IF EXISTS `emp_deptrain`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `emp_deptrain` AS select `department_emp`.`departmentId` AS `departmentId`,`department_emp`.`departmentName` AS `departmentName`,`department_emp`.`workId` AS `workId`,`department_emp`.`workName` AS `workName`,`employee_train`.`userId` AS `userId`,`employee_train`.`userName` AS `userName`,`employee_train`.`trainskill` AS `trainskill`,`employee_train`.`trainstartdate` AS `trainstartdate`,`employee_train`.`trainenddate` AS `trainenddate`,`employee_train`.`remark` AS `remark` from (`department_emp` join `employee_train`) where (`department_emp`.`userId` = `employee_train`.`userId`);

SET FOREIGN_KEY_CHECKS = 1;
