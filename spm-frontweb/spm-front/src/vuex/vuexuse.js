import vue from 'vue'
import vuex from 'vuex'
vue.use(vuex)
export default new vuex.Store({
  state:{
    userId:"",
    role:"",
    departmentId:"",
    viewPage:1,
    routes: [],
    flushemp:[],
  },
  mutations:{
    setuserId(state,sources)
    {
      state.userId = sources
    },
    setrole(state,sources)
    {
      state.role = sources
    },
    setflushemp(state,sources)
    {
      state.flushemp = sources
    },
    setdepartmentId(state,sources)
    {
      state.departmentId = sources
    },
    cleardepartmentId(state)
    {
      state.departmentId = ""
    },
    clearrole(state)
    {
      state.role = ""
    },
    clearuserId(state)
    {
      state.userId = ""
    },
    setviewPage(state,sources)
    {
      state.viewPage = sources
    },
    clearviewPage(state)
    {
      state.viewPage = 1
    },
    initRoutes(state, data) {
      state.routes = data;
    },
  }
})
