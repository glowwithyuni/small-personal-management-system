const cssLoader = {
  loader: 'css-loader',
  options: {
    sourceMap: options.sourceMap
  }
}

const px2remLoader = {
  loader: 'px2rem-loader',
  options: {
    remUnit: 192
  }
}
function generateLoaders (loader, loaderOptions) {
  const loaders = options.usePostCSS ? [cssLoader, postcssLoader, px2remLoader] : [cssLoader, px2remLoader]

  if (loader) {
    loaders.push({
      loader: loader + '-loader',
      options: Object.assign({}, loaderOptions, {
        sourceMap: options.sourceMap
      })
    })
  }
}
