import Vue from 'vue'


import Router from 'vue-router'
import register from "../components/login/resetpassword";
import Userlogin from "../components/login/Userlogin";
import mainView from "../components/mainview/mainView";
import resetpassword from "../components/login/resetpassword";
import RequestFindPassword from "../components/login/RequestFindPassword";
import searchpasswordView from "../components/login/searchpasswordView";
import personal_information from "../components/mainview/view/personal_information";
import changepassword from "../components/mainview/view/changepassword";
import firstView from "../components/mainview/view/firstView";
import person_information_change from "../components/mainview/view/person_information_change";
import HelloWorld from "../components/HelloWorld";
import create_person from "../components/mainview/view/employee/create_person";
import self_employee from "../components/mainview/view/employee/self_employee";
import employee_management from "../components/mainview/view/employee/employee_management";
import department_management from "../components/mainview/view/department/department_management";
import department_summary from "../components/mainview/view/department/department_summary";
import train_management from "../components/mainview/view/train/train_management";
import AdminAttendanceManagement from "../components/mainview/view/workAttendance/AdminAttendanceManagement";
import AdminSalaryManagement from "../components/mainview/view/salary/AdminSalaryManagement";
import AdminSalaryYearManagement from "../components/mainview/view/salary/AdminSalaryYearManagement";
import user_department from "../components/mainview/view/department/user_department";
import dep_employee from "../components/mainview/view/employee/dep_employee";
import department_train from "../components/mainview/view/train/department_train";
import departmentWorkAttendance from "../components/mainview/view/workAttendance/departmentWorkAttendance";
import DepYearManagement from "../components/mainview/view/salary/DepYearManagement";
import DepMonthManagement from "../components/mainview/view/salary/DepMonthManagement";
import depjob from "../components/mainview/view/job/depjob";
import adminjob from "../components/mainview/view/job/adminjob";
import forjob from "../components/mainview/view/job/forjob";
import UserSalary from "../components/mainview/view/salary/UserSalary";
import PersonWorkAttendance from "../components/mainview/view/workAttendance/PersonWorkAttendance";
import PersonDepartment from "../components/mainview/view/department/PersonDepartment";
import person_management from "../components/mainview/view/train/person_management";
import SystemManagement from "../components/mainview/view/System/SystemManagement";

Vue.use(Router)

const router = new Router({
  routes: [
    // {
    //   path:'/test',
    //   name: 'testview',
    //   component: HelloWorld
    //
    // },
    {
      path: '/',
      name: 'Userlogin',
      component: Userlogin
    },
    {
      path: "/resetpassword",
      name: "resetpassword",
      component: resetpassword
    }
    ,{
      path: "/mainView",
      name: "mainView",
      component: mainView

    },
    {
      path:"/RequestFindPassword",
      name:"RequestFindPassword",
      component: RequestFindPassword

    },
    {
      path: "/searchpasswordView",
      name: "/RequestFindPassword",
      component: searchpasswordView
    },
    {
      path: "/forjob",
      name:"forjob",
      component: forjob
    },
    {
      path: "/home",
      name: "home",
      component: mainView,
      children:[
        {
          path:"/personal_information",
          name:"personal_information",
          component: personal_information,

        },
        {
          path:"/person_information_change",
          name:"person_information_change",
          component: person_information_change
        },
        {
          path:"/create_personal_information",
          name:"create_person",
          component: create_person
        },
        {
          path:"/changepassword",
          name:"changepassword",
          component: changepassword
        },
        {
          path:"/self_employee",
          name: "self_employee",
          component: self_employee
        },
        {
          path:"/firstview",
          name:"firstview",
          component: firstView
        },
        {
          path:"/employee_management",
          name:"employee_management",
          component:employee_management
        },
        {
          path:"/dep_employee",
          name:"dep_employee",
          component: dep_employee
        },
        {
          path:"/department_management",
          name:"department_management",
          component: department_management
        },
        {
          path:"/department_summary",
          name:"department_summary",
          component: department_summary
        },
        {
          path:"/user_department",
          name:"user_department",
          component: user_department
        },
        {
          path:"/PersonDepartment",
          name:"PersonDepartment",
          component: PersonDepartment
        },
        {
          path:"/AdminAttendanceManagement",
          name: "AdminAttendanceManagement",
          component: AdminAttendanceManagement
        },
        {
          path:"/departmentWorkAttendance",
          name: "departmentWorkAttendance",
          component: departmentWorkAttendance
        },
        {
          path:"/PersonWorkAttendance",
          name: "PersonWorkAttendance",
          component: PersonWorkAttendance
        },
        {
          path:"/home/train_management",
          name:"train_management",
          component: train_management
        },
        {
          path:"/person_management",
          name: "person_management",
          component: person_management
        },
        {
          path:"/department_train",
          name:"department_train",
          component: department_train
        },
        {
          path:"/AdminSalaryManagement",
          name:"AdminSalaryManagement",
          component: AdminSalaryManagement
        },
        {
          path:"/AdminSalaryYearManagement",
          name:"AdminSalaryYearManagement",
          component: AdminSalaryYearManagement
        },
        {
          path: "/DepYearManagement",
          name: "DepYearManagement",
          component: DepYearManagement
        },
        {
          path: "/DepMonthManagement",
          name:"DepMonthManagement",
          component: DepMonthManagement
        },
        {
          path: "/UserSalary",
          name:"UserSalary",
          component: UserSalary
        },

        {
          path: "/depjob",
          name:"depjob",
          component: depjob
        },
        {
          path: "/adminjob",
          name:"adminjob",
          component: adminjob
        },
        {
          path: "/seeforjob",
          name:"forjob",
          component: forjob
        },
        {
          path: "/DepMonthManagement",
          name:"DepMonthManagement",
          component: DepMonthManagement
        },
        {
          path: "/SystemManagement",
          name:"SystemManagement",
          component: SystemManagement
        }


      ]
    },



  ]
})
export default router
