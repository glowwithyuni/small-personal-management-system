// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
//使用router配置路由
import Router from 'vue-router'
Vue.use(Router)


/**
 * @description: 导入excel并进行数据提取
 * @return:
 */
Vue.prototype.$importExcel = function (file, header) {
  let _this = this;
  return new Promise(function (resolve, reject) {
    const types = file.name.split('.')[1]
    const fileType = ['xlsx', 'xlc', 'xlm', 'xls', 'xlt', 'xlw', 'csv'].some(item => item === types)
    if (!fileType) {
      _this.$message({
        type: "warning",
        message: "文件格式不正确，请重新选择!"
      });
      reject();
    }
    const reader = new FileReader();
    reader.onload = function (e) {
      const data = e.target.result;
      this.wb = XLSX.read(data, {
        type: "binary"
      });
      const wsname = this.wb.SheetNames[0];
      const ws = this.wb.Sheets[wsname];
      /* Convert array of arrays */
      const sheetJson = XLSX.utils.sheet_to_json(ws);
      let tableData = []; //转换为真正的table所需要的数据
      //excel转为json数据，记住header中的label一定要和excel中的表头汉字一致，不然无法匹配
      for (let item of sheetJson) {
        let obj = {};
        for (let key in item) {
          for (let childItem of header) {
            if (key === childItem.label) {
              obj[childItem.prop] = item[key];
              break;
            }
          }
        }
        tableData.push(obj);
      }
      resolve(tableData);
    };
    reader.readAsBinaryString(file.raw);
  });
}








import FileSaver from 'file-saver'
import XLSX from 'xlsx'
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
// //vuex数据交流
// import Vuex from 'vuex'
// Vue.use(Vuex)
// export default new Vuex.Store({})
import store from "./vuex/vuexuse"
import axios from 'axios'
import testuserdata from "./components/testuserdata";
import testpwd from "./components/testpwd";
import qs from 'qs'
import api from './router/api'
import router from './router/index'
//使用element-ui
import el from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import register from "./components/login/resetpassword";
Vue.use(el)
//使用axios或jquery
Vue.config.productionTip = false
Vue.prototype.$https = axios
Vue.prototype.$axios = axios

Vue.component(testuserdata)
Vue.component(testpwd)
Vue.component(register)
Vue.prototype.api = api;
axios.defaults.baseURL = api.baseURL;
//文字自适应页面
import 'lib-flexible'
/* eslint-disable no-new */
//外部js调用
import VueSession from 'vue-session'
Vue.use(VueSession)
import Validator from 'vue-validator'
Vue.use(Validator)
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
})
