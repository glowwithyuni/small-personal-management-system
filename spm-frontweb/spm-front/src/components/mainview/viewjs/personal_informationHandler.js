function reloadEmployee()
{
  this.$axios.post("/employee/getEmployee",{
    userId:this.$store.state.userId
  })
    .then((res)=>{

      this.employee.userId=JSON.stringify(res.data.userId).replace(/\"/g, "")
      this.employee.username=JSON.stringify(res.data.userName).replace(/\"/g, "")
      this.employee.IdCard=JSON.stringify(res.data.idcard).replace(/\"/g, "")
      this.employee.gender=JSON.stringify(res.data.gender).replace(/\"/g, "")
      this.employee.politicaltype=JSON.stringify(res.data.politicaltype).replace(/\"/g, "")
      this.employee.nationId=JSON.stringify(res.data.nationId).replace(/\"/g, "")
      this.employee.address=JSON.stringify(res.data.address).replace(/\"/g, "")
      this.employee.phone=JSON.stringify(res.data.phone).replace(/\"/g, "")
      this.employee.education=JSON.stringify(res.data.education).replace(/\"/g, "")
      this.employee.educationsubject=JSON.stringify(res.data.educationsubject).replace(/\"/g, "")
      this.employee.school=JSON.stringify(res.data.school).replace(/\"/g, "")
      this.employee.departmentId=JSON.stringify(res.data.departmentId).replace(/\"/g, "")
      this.employee.departmentName=JSON.stringify(res.data.departmentName).replace(/\"/g, "")
      this.employee.workId=JSON.stringify(res.data.workId).replace(/\"/g, "")
      this.employee.workName=JSON.stringify(res.data.workName).replace(/\"/g, "")
      this.employee.baseSalary=JSON.stringify(res.data.baseSalary).replace(/\"/g, "")
        this.employee.email =JSON.stringify(res.data.email).replace(/\"/g, "")
    })
}

function reloadEmployees()
{
    this.$axios.post("/employee/getEmployee",{
        userId:this.$store.state.userId
    })
        .then((res)=>{
            this.employee.email =JSON.stringify(res.data.email).replace(/\"/g, "")
            this.employee.userId=JSON.stringify(res.data.userId).replace(/\"/g, "")
            this.employee.username=JSON.stringify(res.data.userName).replace(/\"/g, "")
            this.employee.idcard=JSON.stringify(res.data.idcard).replace(/\"/g, "")
            this.employee.gender=JSON.stringify(res.data.gender).replace(/\"/g, "")
            this.employee.politicaltype=JSON.stringify(res.data.politicaltype).replace(/\"/g, "")
            this.employee.nationId=JSON.stringify(res.data.nationId).replace(/\"/g, "")
            this.employee.address=JSON.stringify(res.data.address).replace(/\"/g, "")
            this.employee.phone=JSON.stringify(res.data.phone).replace(/\"/g, "")
            this.employee.education=JSON.stringify(res.data.education).replace(/\"/g, "")
            this.employee.educationsubject=JSON.stringify(res.data.educationsubject).replace(/\"/g, "")
            this.employee.school=JSON.stringify(res.data.school).replace(/\"/g, "")
            this.employee.departmentId=JSON.stringify(res.data.departmentId).replace(/\"/g, "")
            this.employee.departmentName=JSON.stringify(res.data.departmentName).replace(/\"/g, "")
            this.employee.workId=JSON.stringify(res.data.workId).replace(/\"/g, "")
            this.employee.workName=JSON.stringify(res.data.workName).replace(/\"/g, "")
            this.employee.baseSalary=JSON.stringify(res.data.baseSalary).replace(/\"/g, "")
        })
}


function reloadEmployeeAndSaveAddressAndPhone()
{
  this.$axios.post("/employee/getEmployee",{
    userId:this.$store.state.userId
  })
    .then((res)=>{
        this.employee.email =JSON.stringify(res.data.email).replace(/\"/g, "")
      this.employee.userId=JSON.stringify(res.data.userId).replace(/\"/g, "")
      this.employee.username=JSON.stringify(res.data.userName).replace(/\"/g, "")
      this.employee.IdCard=JSON.stringify(res.data.idcard).replace(/\"/g, "")
      this.employee.gender=JSON.stringify(res.data.gender).replace(/\"/g, "")
      this.employee.politicaltype=JSON.stringify(res.data.politicaltype).replace(/\"/g, "")
      this.employee.nationId=JSON.stringify(res.data.nationId).replace(/\"/g, "")
      this.employee.address=JSON.stringify(res.data.address).replace(/\"/g, "")
      this.employee.phone=JSON.stringify(res.data.phone).replace(/\"/g, "")
      this.employee.education=JSON.stringify(res.data.education).replace(/\"/g, "")
      this.employee.educationsubject=JSON.stringify(res.data.educationsubject).replace(/\"/g, "")
      this.employee.school=JSON.stringify(res.data.school).replace(/\"/g, "")
      this.employee.departmentId=JSON.stringify(res.data.departmentId).replace(/\"/g, "")
      this.employee.departmentName=JSON.stringify(res.data.departmentName).replace(/\"/g, "")
      this.employee.workId=JSON.stringify(res.data.workId).replace(/\"/g, "")
      this.employee.workName=JSON.stringify(res.data.workName).replace(/\"/g, "")
      this.employee.baseSalary=JSON.stringify(res.data.baseSalary).replace(/\"/g, "")
      this.old_address = this.employee.address;
      this.old_phone = this.employee.phone;
      // alert(this.old_address+this.old_phone)
    })
}
function pushView(id) {
    alert(id)
    if(id == 11)
    {
     return   this.$router.push({path:'/create_personal_information'})
    }


}


function reloadEmployeeIsNull()
{
  this.$axios.post("/employee/getEmployee",{
    userId:this.$store.state.userId
  })
    .then((res)=>{
      //alert(res.status )
      this.employee.userId=JSON.stringify(res.data.userId).replace(/\"/g, "")
      this.employee.username=JSON.stringify(res.data.userName).replace(/\"/g, "")
      this.employee.IdCard=JSON.stringify(res.data.idcard).replace(/\"/g, "")
      this.employee.gender=JSON.stringify(res.data.gender).replace(/\"/g, "")
      this.employee.politicaltype=JSON.stringify(res.data.politicaltype).replace(/\"/g, "")
      this.employee.nationId=JSON.stringify(res.data.nationId).replace(/\"/g, "")
      this.employee.address=JSON.stringify(res.data.address).replace(/\"/g, "")
      this.employee.phone=JSON.stringify(res.data.phone).replace(/\"/g, "")
      this.employee.education=JSON.stringify(res.data.education).replace(/\"/g, "")
      this.employee.educationsubject=JSON.stringify(res.data.educationsubject).replace(/\"/g, "")
      this.employee.school=JSON.stringify(res.data.school).replace(/\"/g, "")
      this.employee.departmentId=JSON.stringify(res.data.departmentId).replace(/\"/g, "")
      this.employee.departmentName=JSON.stringify(res.data.departmentName).replace(/\"/g, "")
      this.employee.workId=JSON.stringify(res.data.workId).replace(/\"/g, "")
      this.employee.workName=JSON.stringify(res.data.workName).replace(/\"/g, "")
      this.employee.baseSalary=JSON.stringify(res.data.baseSalary).replace(/\"/g, "")
      this.employee.email =JSON.stringify(res.data.email).replace(/\"/g, "")
      if(this.employee.IdCard == "null"||this.employee.gender == "null"||this.employee.politicaltype== "null"
        ||this.employee.nationId == "null"|| this.employee.address == "null"|| this.employee.phone == "null"
        || this.employee.education == "null"|| this.employee.educationsubject == "null"||this.employee.school=="null"||this.employee.email =="null")
      {
        alert("你的信息还未填写或未填写完整!已自动跳转到信息填写界面重新填写信息！")
        this.employee.IdCard=""
        this.employee.gender=""
        this.employee.politicaltype=""
        this.employee.nationId=""
        this.employee.address=""
        this.employee.phone=""
        this.employee.education=""
        this.employee.educationsubject=""
        this.employee.school=""
        this.employee.email=""
        this.$router.push("/create_personal_information")
      }
    })
}
function reloadEmployeeOnIdAndName()
{
  this.$axios.post("/employee/getEmployee",{
    userId:this.$store.state.userId
  })
    .then((res)=>{

      this.employee.userId=JSON.stringify(res.data.userId).replace(/\"/g, "")
      this.employee.username=JSON.stringify(res.data.userName).replace(/\"/g, "")
        this.employee.email =JSON.stringify(res.data.email).replace(/\"/g, "")
      this.employee.departmentId=JSON.stringify(res.data.departmentId).replace(/\"/g, "")
      this.employee.departmentName=JSON.stringify(res.data.departmentName).replace(/\"/g, "")
      this.employee.workId=JSON.stringify(res.data.workId).replace(/\"/g, "")
      this.employee.workName=JSON.stringify(res.data.workName).replace(/\"/g, "")
      this.employee.baseSalary=JSON.stringify(res.data.baseSalary).replace(/\"/g, "")
    })
}

export {
  reloadEmployee,
    reloadEmployees,
  reloadEmployeeAndSaveAddressAndPhone,
  pushView,
  reloadEmployeeIsNull,
  reloadEmployeeOnIdAndName
}
