package org.pjx.spm.controller;

import io.swagger.annotations.ApiOperation;
import org.pjx.spm.pojo.Jobneed;
import org.pjx.spm.service.JobneedServiceImpl;
import org.pjx.spm.util.interfacespecfiaction.RespBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2022-04-14 22:23:20
 * @Description
 * @Version 1.0
 */
@RestController
public class JobneedController {
    @Autowired
    private JobneedServiceImpl jobneedService;
    @ApiOperation("展示全部招聘信息")
    @RequestMapping(path = "/Jobneed/SelectAllJobneed")
    private List<Jobneed>SelectAllJobneed()
    {
        return jobneedService.selectAllJobneed();
    }
    @ApiOperation("根据部门Id展示部门招聘信息")
    @RequestMapping(path = "/Jobneed/SelectAllJobneedBydepartmentId")
    private List<Jobneed>SelectAllJobneedBydepartmentId(@RequestBody Map<String,Integer>map)
    {
        return jobneedService.selectJobneedBydepartmentId(map.get("departmentId"));
    }
    @ApiOperation("根据部门Id展示部门招聘信息")
    @RequestMapping(path = "/Jobneed/UpdateJobneedByJobrage")
    private RespBean UpdateJobneedByJobrage(@RequestBody Jobneed jobneed)
    {
         jobneedService.updateJobneedByJobrage(jobneed);
        return RespBean.ok("修改成功");
    }
    @ApiOperation("初始化新建初始化招聘信息")
    @RequestMapping(path = "/Jobneed/SelectDepUserNameBeforeInsert")
    private Map<Object,Object> SelectDepUserNameBeforeInsert(@RequestBody Map<String,String>map)
    {
        System.out.println(map.get("userId"));
        Map<Object,Object> maps = jobneedService.SelectDepUserNameBeforeInsert(map.get("userId"));
        System.out.println(maps.get("username"));
            return maps;
    }
    @ApiOperation("新建招聘信息")
    @RequestMapping(path = "/Jobneed/InsertJobneed")
    private RespBean InsertJobneed(@RequestBody Jobneed jobneed)
    {
        jobneedService.InsertJobneed(jobneed);
        return RespBean.ok("添加成功！");
    }
    @ApiOperation("新建招聘信息")
    @RequestMapping(path = "/Jobneed/deleteJobneedByJobrage")
    private RespBean deleteJobneedByJobrage(@RequestBody Map<String,Integer>map)
    {
        jobneedService.deleteJobneedByJobrage(map.get("jobrage"));
        return RespBean.ok("添加成功！");
    }

    @ApiOperation("获取所有部门Id")
    @RequestMapping(path = "/Jobneed/selectDepartmentId")
    private List<Integer> selectDepartmentId()
    {
        return  jobneedService.selectDepartmentId();

    }

    @ApiOperation("根据部门ID获取所有部门详细信息")
    @RequestMapping(path = "/Jobneed/selectDepByDepartmentId")
    private Map<Object, Object> selectDepByDepartmentId(@RequestBody Map<String,Integer>map)
    {
        return  jobneedService.selectDepByDepartmentId(map.get("departmentId"));

    }
}
