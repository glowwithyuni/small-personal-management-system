package org.pjx.spm.controller;

import io.swagger.annotations.ApiOperation;
import org.pjx.spm.pojo.employee_request;
import org.pjx.spm.service.UserServiceImpl;
import org.pjx.spm.util.interfacespecfiaction.RespBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2022-04-16 03:52:28
 * @Description
 * @Version 1.0
 */
@RestController
public class UserController {
    @Autowired
    private UserServiceImpl userService;
    @RequestMapping(value = "/resetPasswordByEmail",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "重置密码By邮箱")
    public RespBean resetPasswordByEmail(@RequestBody Map<String,String> map)
    {
            System.out.println(map.get("userId")+map.get("email"));
           if( userService.resetPasswordByEmail(map)==1)
           {
               return RespBean.ok("重置成功，已发送邮箱查看");
           }else {
               return RespBean.error("邮箱不一致，请重试！");
           }

    }
    @RequestMapping(value = "/selectAllEmployee_request",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "查看所有请求")
    public List<employee_request> selectAllEmployee_request()
    {


        return userService.selectAllEmployee_request();
    }
    @RequestMapping(value = "/updateResetEmpRequestCode",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "查看所有请求")
    public RespBean updateResetEmpRequestCode(@RequestBody  employee_request employee_requests)
    {
        System.out.println(employee_requests.toString());

        userService.updateResetEmpRequestCodes(employee_requests);
        return RespBean.ok("修改成功");
    }
    @RequestMapping(value = "/deletepasswordrequestById",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value = "查看所有请求")
    public RespBean deletepasswordrequestById(@RequestBody  employee_request employee_requests)
    {

        userService.deletepasswordrequestById(employee_requests.getUserId());
        return RespBean.ok("删除成功");
    }
}
