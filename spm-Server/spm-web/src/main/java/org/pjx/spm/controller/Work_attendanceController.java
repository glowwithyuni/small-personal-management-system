package org.pjx.spm.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import org.pjx.spm.pojo.Work_attendance;
import org.pjx.spm.service.Work_attendanceServiceImpl;
import org.pjx.spm.util.interfacespecfiaction.RespBean;
import org.pjx.spm.util.timeboundCheck.TimeBoundCheck;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2022-03-16 15:59:36
 * @Description
 * @Version 1.0
 */
@Controller
@RequestMapping("Work_attendance")
public class Work_attendanceController {
    @Autowired
    private Work_attendanceServiceImpl work_attendanceService;
    private final static Logger logger = LoggerFactory.getLogger(Work_attendanceController.class);
    @RequestMapping("selectAllWorkAttendance")
    @ResponseBody
    private List<Work_attendance>selectAllWorkAttendance()
    {
//        Work_attendance work_attendance = work_attendanceService.selectAllWorkAttendanceByAttendance(1);
//        logger.info(work_attendance.toString());
//       boolean b = TimeBoundCheck.isEffectiveDate(work_attendance.getAttendcheckdate(),work_attendance.getCheckstartdate(),work_attendance.getCheckenddate());
//       System.out.println(b);
       return  work_attendanceService.selectAllWorkAttendance();

    }
    @RequestMapping("selectAllWorkAttendanceByDepartmentId")
    @ResponseBody
    private List<Work_attendance>selectAllWorkAttendanceByDepartmentId(@RequestBody Map<String,String> map)
    {

        return  work_attendanceService.selectAllWorkAttendanceByDepartmentId(map.get("departmentId"));

    }

    @RequestMapping("selectAllWorkAttendanceByDepartmentIdThisMonth")
    @ResponseBody
    private List<Work_attendance>selectAllWorkAttendanceByDepartmentIdThisMonth(@RequestBody Map<String,String> map)
    {
        String month = map.get("checkdate").toString().substring(0,map.get("checkdate").toString().length()-3)+"%";

        return  work_attendanceService.selectAllWorkAttendanceByDepartmentIdThisMonth(map.get("departmentId"),month);

    }
    @RequestMapping("selectAllWorkAttendanceAscBycheckstartdate")
    @ResponseBody
    private List<Work_attendance>selectAllWorkAttendanceAscBycheckstartdate()
    {
        return work_attendanceService.selectAllWorkAttendance();
    }
    @RequestMapping("selectAllWorkAttendanceAscBycheckstartdateByThisMonth")
    @ResponseBody
    private List<Work_attendance>selectAllWorkAttendanceAscBycheckstartdateByThisMonth(@RequestBody Map<Object,Object>map)
    {
        String month = map.get("checkdate").toString().substring(0,map.get("checkdate").toString().length()-3)+"%";
        return work_attendanceService.selectAllWorkAttendanceAscBycheckstartdateByThisMonth(month);
    }
    @RequestMapping("selectMonthAttendanceByuserId")
    @ResponseBody
    private List<Work_attendance>selectMonthAttendanceByuserId(@RequestBody Map<Object,Object>map)
    {
        String month = map.get("checkdate").toString().substring(0,map.get("checkdate").toString().length()-3)+"%";
        return work_attendanceService.selectMonthAttendanceByuserId(map.get("userId").toString(),month);
    }
    @RequestMapping("selectDayAttendanceByuserId")
    @ResponseBody
    private List<Work_attendance>selectDayAttendanceByuserId(@RequestBody Map<Object,Object>map)
    {
        String month = map.get("checkdate").toString();
        return work_attendanceService.selectDayAttendanceByuserId(map.get("userId").toString(),month);
    }
    @RequestMapping("selectDayAttendanceByuserIdAndDepId")
    @ResponseBody
    private List<Work_attendance>selectDayAttendanceByuserIdAndDepId(@RequestBody Map<Object,Object>map)
    {
        String month = map.get("checkdate").toString();
        return work_attendanceService.selectDayAttendanceByuserIdAndDepId(map.get("userId").toString(),month,map.get("departmentId").toString());
    }
    /**
     * 根据checkdate考勤日期查询考勤
     * @param map
     * @return
     */
    @RequestMapping("selectAttendByCheckdate")
    @ResponseBody
    private List<Work_attendance>selectAttendByCheckdate(@RequestBody Map<String,String>map)
    {
        return work_attendanceService.selectAttendByCheckdate(map.get("checkdate"));
    }
    @RequestMapping("selectAttendByCheckdateAndDepId")
    @ResponseBody
    private List<Work_attendance>selectAttendByCheckdateAndDepId(@RequestBody Map<String,String>map)
    {
        return work_attendanceService.selectAllAttendByCheckdateAndDepId(map.get("checkdate"),map.get("departmentId"));
    }
    @RequestMapping("selectAllEmpUserIdAndUsername")
    @ResponseBody
    private List<Map<String, String>> selectAllEmpUserIdAndUsername()
    {
        return work_attendanceService.selectAllEmpUserIdAndUsername();
    }
    @RequestMapping("selectAllDepEmpUserIdAndUsername")
    @ResponseBody
    private List<Map<String, String>> selectAllDepEmpUserIdAndUsername(@RequestBody Map<String,Integer>map)
    {
        return work_attendanceService.selectDepEmpUserIdAndUsername(map);
    }
    @RequestMapping("createNewAttendance")
    @ResponseBody
    private RespBean createNewAttendance(@RequestBody Map<Object,Object> map)
    {

       return work_attendanceService.createNewAttendance(map);
    }
    @RequestMapping("UpdateAttendcheckdate")
    @ResponseBody
    private RespBean UpdateAttendcheckdate(@RequestBody Work_attendance work_attendance) throws ParseException {

        return work_attendanceService.UpdateAttendcheckdate(work_attendance);
    }
    @RequestMapping("DeleteWorkAttendanceByattendance")
    @ResponseBody
    private RespBean DeleteWorkAttendanceByattendance(@RequestBody Map<Object,Object> map)  {

        return work_attendanceService.DeleteWorkAttendanceByattendances(map);
    }
}
