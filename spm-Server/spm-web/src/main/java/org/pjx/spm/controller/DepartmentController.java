package org.pjx.spm.controller;

import io.swagger.annotations.ApiOperation;
import org.pjx.spm.pojo.Department;
import org.pjx.spm.pojo.Department_emp;
import org.pjx.spm.service.Department_empServiceImpl;
import org.pjx.spm.util.interfacespecfiaction.RespBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2022-03-03 10:24:34
 * @Description 部门controller层
 * @Version 1.0
 */
@Controller
public class DepartmentController {
    private final static Logger logger = LoggerFactory.getLogger(DepartmentController.class);
    @Autowired
    private Department_empServiceImpl department_empService;
    @ApiOperation("查找所有部门员工信息")
    @RequestMapping("/department/getAllDepartmentEmp")
    @ResponseBody
    private List<Department_emp> getAllDepartmentEmp()
    {

        return department_empService.getAllDepartmentEmp();
    }
    @ApiOperation("查找所有部门员工信息")
    @RequestMapping("/department/selectDepartmentEmpByDepartmentId")
    @ResponseBody
    private List<Department_emp> selectDepartmentEmpByDepartmentId(@RequestBody Map<String,Integer> map)
    {

        return department_empService.selectDepartmentEmpByDepartmentId(map.get("departmentId"));
    }
    @ApiOperation("查找所有部门员工信息")
    @RequestMapping("/department/selectDepartmentByDepartmentId")
    @ResponseBody
    private Department selectDepartmentByDepartmentId(@RequestBody Map<String,Integer> map)
    {

        return department_empService.selectDepartmentByDepartmentId(map.get("departmentId"));
    }
    @ApiOperation("查找所有部门信息")
    @RequestMapping("/department/getDepartment")
    @ResponseBody
    private List<Department> getDepartment()
    {
        return department_empService.getDepartment();
    }
    @ApiOperation("根据部门Id查找部门信息")
    @RequestMapping("/department/getDepartmentByDepartmentId")
    @ResponseBody
    private List<Department> getDepartmentByDepartmentId(@RequestBody Map<String,Integer> map)
    {
        return department_empService.getDepartmentByDepartmentId(map.get("departmentId"));
    }
    @ApiOperation("查找不是部长的员工的id")
    @RequestMapping("/department/getUserIdByNoDepartment")
    @ResponseBody
    private List<Department_emp> getUserIdByNoDepartment()
    {

        return department_empService.getUserIdByNoDepartment();
    }
    @ApiOperation("查找不是部长的员工的name")
    @RequestMapping("/department/getUserNameByNoDepartment")
    @ResponseBody
    private Map<String,Object> getUserNameByNoDepartment(@RequestBody  Map<String,String> map)
    {
        logger.info("获取的不是department的userId："+map.get("userId"));
        return department_empService.getUserNameByNoDepartment(map.get("userId"));
    }
    @ApiOperation("创建新部门 ，选定部长的情况下")
    @RequestMapping("/department/InsertnewDepartment")
    @ResponseBody
    private RespBean InsertnewDepartment(@RequestBody Department department)
    {
        int res = department_empService.InsertnewDepartment(department);
        if(res == 1)
        {
            return RespBean.ok("插入部门成功！");
        }
        return RespBean.error("插入失败，部门Id不可重复！");
    }
    @ApiOperation("更改部门成员信息")
    @RequestMapping("/department/updatenewDepartmentEmpByuserId")
    @ResponseBody
    private RespBean updatenewDepartmentEmpByuserId(@RequestBody Map<String,String>map)
    {
        Department_emp department_emp = new Department_emp();
        department_emp.setDepartmentId(Integer.valueOf(map.get("departmentId")));
        department_emp.setDepartmentName(map.get("departmentName"));
        department_emp.setWorkId(Integer.valueOf(map.get("workId")));
        department_emp.setWorkName(map.get("workName"));
        department_emp.setUsername(map.get("username"));
        department_emp.setUserId(map.get("userId"));
        int res = department_empService.updatenewDepartmentEmpByuserId(department_emp);
        if( res == 1)
        {
            return RespBean.ok("插入部门成员表成功");
        }
        return RespBean.error("插入失败");
    }
    @ApiOperation("根据departmentId删除部门")
    @RequestMapping("/department/deleteDepartmentByDepartmentId")
    @ResponseBody
    private RespBean deleteDepartmentByDepartmentId(@RequestBody Map<String,String>map)
    {
        int res = department_empService.deleteDepartmentByDepartmentId(Integer.valueOf(map.get("departmentId")));
        if (res == 1)
        {
            return RespBean.ok("删除部门成功");
        }
        return RespBean.error("删除失败，该部门不存在或输入错误！");
    }
    @ApiOperation("根据userId更改权限")
    @RequestMapping("/department/updateRoleByuserId")
    @ResponseBody
    private RespBean updateRoleByuserId(@RequestBody Map<String,String> map)
    {
        String role =map.get("role");
        String userId = map.get("userId");
            int res1 = department_empService.updateUserRoleByuserId(role,userId);
            int res2 = department_empService.updateEmpRoleByuserId(role,userId);
            int res3 = department_empService.updateDepRoleByuserId(role,userId);
            if(res1 == 1 && res2 ==1 && res3 == 1)
            {
                return RespBean.ok("更新权限成功");
            }
        return RespBean.ok("更新权限失败");
    }
    @ApiOperation("更新部门人数")
    @RequestMapping("/department/UpdateDepartmentNum")
    @ResponseBody
    private RespBean UpdateDepartmentNum(@RequestBody Map<String,String> map)
    {
        Integer departmentId = Integer.valueOf(map.get("departmentId"));
        int res = department_empService.UpdateDepartmentNum(departmentId);
        if(res == 1)
        {
            return RespBean.ok("更新部门人数成功惹");
        }
        return RespBean.error("更新部门人数失败惹");
    }
    @ApiOperation("根据userId更改权限")
    @RequestMapping("/department/updatedepartmentNameBydepartmentId")
    @ResponseBody
    private RespBean updatedepartmentNameBydepartmentId(@RequestBody Map<String,String> map)
    {
        String departmentName =map.get("departmentName");
        Integer departmentId =Integer.valueOf(map.get("departmentId")) ;
        int res1 = department_empService.updateEmpDepartmentNameByDepartmentId(departmentName,departmentId);
        int res2 = department_empService.updateDepEmpDepartmentNameByDepartmentId(departmentName,departmentId);
        int res3 = department_empService.updateDepDepartmentNameByDepartmentId(departmentName,departmentId);
        if(res1 == 1 && res2 ==1 && res3 == 1)
        {
            return RespBean.ok("更新部门名成功");
        }
        return RespBean.ok("更新部门名失败");
    }
    @ApiOperation("删除部门")
    @RequestMapping("/department/deleteDepartment")
    @ResponseBody
    private RespBean deleteDepartment(@RequestBody Map<String,String> map)
    {
        String userId = map.get("userId");
        String role ="user";
        Integer departmentId = Integer.valueOf(map.get("departmentId"));
            List<String> list = department_empService.getDepartment_empuserIdBydepartmentId(departmentId);
        for (String userIds : list) {
            department_empService.ClearDepartment_emp(userIds);
            department_empService.ClearEmployeeDepartment(userIds);
        }
         department_empService.updateUserRoleByuserId(role,userId);
         department_empService.updateEmpRoleByuserId(role,userId);
         department_empService.updateDepRoleByuserId(role,userId);
        int res4 = department_empService.deleteDepartmentByDepartmentId(departmentId);
        if(res4 == 1)
        {
            return RespBean.ok("删除部门"+map.get("departmentName")+"成功");
        }
        return RespBean.error("删除部门失败");
    }
    @ApiOperation("根据部门Id查询部门所有成员")
    @RequestMapping("/department/selectDepByDepartmentId")
    @ResponseBody
     private List<Department_emp>selectDepByDepartmentId(@RequestBody  Map<String,Integer>map)
    {
        return department_empService.selectDepByDepartmentId(map.get("departmentId"));
    }
    @ApiOperation("根据userId查询部门成员信息")
    @RequestMapping("/department/selectDepByuserId")
    @ResponseBody
    private List<Department_emp>selectDepByuserId(@RequestBody  Map<String,String>map)
    {
        return department_empService.selectDepByuserId(map.get("userId"));
    }
    @ApiOperation("根据部门Id查询成员")
    @RequestMapping("/department/selectDepMsgByDepartmentId")
    @ResponseBody
    private Department selectDepMsgByDepartmentId(@RequestBody  Map<String,Integer>map)
    {
        logger.info("收到的deparmentId:"+map.get("departmentId"));
        Department department = department_empService.selectDepMsgByDepartmentId(map.get("departmentId"));
        logger.info("查询的部门详细信息:"+department.toString());
        return department;
    }
    @ApiOperation("根据userId修改部门成员姓名及其部门信息")
    @RequestMapping("/department/updateDepuserNameByuserId")
    @ResponseBody
    private RespBean updateDepuserNameByuserId(@RequestBody Map<String,String>map)
    {

            if("user".equals(map.get("role")))
            {
                department_empService.updateDepuserNameByuserId(map.get("username"),map.get("workName"),map.get("userId"));
               return RespBean.ok("修改成功");
            }
            if("department".equals(map.get("role")))
            {
//                修改部长的个人职位和姓名
                department_empService.updateMinisteruserNameByuserId
                        (map.get("username"),
                        map.get("workName"),
                        map.get("userId"),
                        map.get("departmentName"),
                        map.get("departmentId"));

                return RespBean.ok("修改成功");
            }
        return RespBean.error("修改失败");
    }
    @ApiOperation("接口測試")
    @RequestMapping("/department/test")
    @ResponseBody
    private List<Department_emp> test(@RequestBody Map<String,Integer>map){
        List<Department_emp> list = department_empService.getDepartmenterByDepartmentId(map.get("departmentId"));
        Iterator<Department_emp> iterator = list.iterator();
                    while (iterator.hasNext())
                    {
                        logger.info(String.valueOf(iterator.next().getUserId()+iterator.next().getUsername()));
                    }
        return list;

    }
    @ApiOperation("移除部门成员")
    @RequestMapping("/department/removeDepartmentMate")
    @ResponseBody
    private RespBean removeDepartmentMate(@RequestBody Map<String,String>map)
    {

        department_empService.removeDepartmentMate(map.get("userId"),map.get("departmentId"));
        return RespBean.ok("移除成功！");
    }
    @ApiOperation("增加部门成员")
    @RequestMapping("/department/addnewDepMate")
    @ResponseBody
    private RespBean addnewDepMate(@RequestBody Map<String,String>map)
    {
        logger.info("待添加的部门成员信息："+map.toString());
        department_empService.addnewDepMate(
                map.get("departmentId"),
                map.get("departmentName"),
                map.get("userId"),
                map.get("workName"),
                map.get("workId"));
        return RespBean.ok("增加成功！");
    }
    @ApiOperation("获取部门Id")
    @RequestMapping("/department/selectDepIdFromDepartment")
    @ResponseBody
    private List<String> selectDepIdFromDepartment()
    {
        return department_empService.selectDepIdFromDepartment();
    }
    @ApiOperation("根据DepId获取DepName")
    @RequestMapping("/department/selectDepNameFromDepartmentByDepId")
    @ResponseBody
    private String selectDepNameFromDepartmentByDepId(@RequestBody Map<String,String>map)
    {
        return department_empService.selectDepNameFromDepartmentByDepId(map.get("departmentId"));
    }
    @ApiOperation("根据DepId获取DepName")
    @RequestMapping("/department/changeDeper")
    @ResponseBody
    private RespBean changeDeper(@RequestBody Map<String,String>map)
    {
        department_empService.UpdateNewDep(
                map.get("departmentId"),
                map.get("departmentName"),
                map.get("workId"),
                map.get("workName"),
                map.get("userId"));
        department_empService.clearOldDep(map.get("olduserId"));
        Calendar cal = Calendar.getInstance();
        String year =  String.valueOf(cal.get(Calendar.YEAR));
        department_empService.updateSalaryPower("department",map.get("userId"),year);
        department_empService.updateSalaryPower("user",map.get("olduserId"),year);
        return RespBean.ok("更换部长成功");
    }
    @ApiOperation("获取无安排就职的员工")
    @RequestMapping("/department/selectNoWorkuserId")
    @ResponseBody
    private List<String> selectNoWorkuserId()
    {

        return department_empService.selectNoWorkuserId();
    }
    @ApiOperation("移除部门")
    @RequestMapping("/department/removeDepartment")
    @ResponseBody
    private RespBean removeDepartment(@RequestBody Map<String,String> map)
    {
        Calendar cal = Calendar.getInstance();
        department_empService.UpdateOutputEmpAndDep(map.get("departmentId"),map.get("userId"));
        department_empService.updateSalaryPower("user",map.get("userId"),String.valueOf(cal.get(Calendar.YEAR)));
        department_empService.deleteDepartmentByDepartmentId(Integer.valueOf(map.get("departmentId")));
        return RespBean.ok("解散部门成功");
    }
    @ApiOperation("根据role升序返回dep_emp")
    @RequestMapping("/department/selectdepartment_empByrole")
    @ResponseBody
    private List<Department_emp> selectdepartment_empByrole()
    {

      return department_empService.selectdepartment_empByrole();
    }
    @ApiOperation("根据role升序返回dep_emp")
    @RequestMapping("/department/updateDepNum")
    @ResponseBody
    private RespBean updateDepNum()
    {
        Calendar cal = Calendar.getInstance();
        System.out.println("calendar"+cal.get(Calendar.YEAR));
        System.out.println("calendar"+(cal.get(Calendar.MONTH)+1));
        department_empService.updateNum();
        return RespBean.ok("更新成功捏");
    }
}
