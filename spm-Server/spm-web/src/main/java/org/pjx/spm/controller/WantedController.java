package org.pjx.spm.controller;

import io.swagger.annotations.ApiOperation;
import org.pjx.spm.pojo.Employee;
import org.pjx.spm.pojo.Wanted;
import org.pjx.spm.service.WantedServiceImpl;
import org.pjx.spm.util.interfacespecfiaction.RespBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2022-04-15 00:27:18
 * @Description
 * @Version 1.0
 */
@RestController
public class WantedController {
    @Autowired
    private WantedServiceImpl wantedService;
    @ApiOperation("查询所有应聘信息")
    @RequestMapping("/wanted/selectAllWanted")
    private List<Wanted> selectAllWanted()
    {
        return wantedService.selectAllWanted();
    }
    @ApiOperation("根据应聘部门Id查询所有应聘信息")
    @RequestMapping("/wanted/selectWantedBydepartmentId")
    private List<Wanted> selectWantedBydepartmentId(@RequestBody Map<String,Integer>map)
    {
        return wantedService.selectWantedBydepartmentId(map.get("departmentId"));
    }

    @ApiOperation("新建求职信息")
    @RequestMapping("/wanted/InsertNewWanted")
    private RespBean InsertNewWanted(@RequestBody Wanted wanted)
    {
        wantedService.InsertNewWanted(wanted);
        return RespBean.ok("添加成功！");
    }
    @ApiOperation("根据队列更改应聘信息")
    @RequestMapping("/wanted/updateWantedByWantedRage")
    private RespBean updateWantedByWantedRage(@RequestBody Wanted wanted)
    {
        wantedService.updateWantedByWantedRage(wanted);
        return RespBean.ok("添加成功！");
    }
    @ApiOperation("根据队列删除应聘信息")
    @RequestMapping("/wanted/deleteWantedByWantedRage")
    private RespBean deleteWantedByWantedRage(@RequestBody Map<String,Integer>map)
    {
        wantedService.deleteWantedByWantedRage(map.get("wantedrage"));
        return RespBean.ok("删除成功！");
    }
    @ApiOperation("根据队列更改应聘信息")
    @RequestMapping("/wanted/selectWantedByDepartmentIdAndIsaccept")
    private List<Wanted> selectWantedByDepartmentIdAndIsaccept(@RequestBody Map<String,Integer>map)
    {
        return  wantedService.selectWantedByDepartmentIdAndIsaccept(map.get("departmentId"),map.get("isaccept"));
    }
    @ApiOperation("根据部门Id和职位查找应聘信息")
    @RequestMapping("/wanted/selectWantedByDepartmentIdAndworkName")
    private List<Wanted> selectWantedByDepartmentIdAndworkName(@RequestBody Map<String,String>map)
    {
        return  wantedService.selectWantedByDepartmentIdAndworkName(map.get("departmentId"),map.get("workName"));
    }
    @ApiOperation("根据队列更改应聘信息")
    @RequestMapping("/wanted/selectWantedByIsaccept")
    private List<Wanted> selectWantedByIsaccept(@RequestBody Map<String,Integer>map)
    {
        return  wantedService.selectWantedByIsaccept(map.get("isaccept"));
    }
    @ApiOperation("根据队列更改应聘信息")
    @RequestMapping("/wanted/updateWantedIsacceptBywantedrage")
    private RespBean updateWantedIsacceptBywantedrage(@RequestBody Map<String,Integer>map)
    {
        wantedService.updateWantedIsacceptBywantedrage(map.get("wantedrage"),map.get("isaccept"));
        return RespBean.ok("更新成功！");
    }
    @ApiOperation("检查userId是否存在")
    @RequestMapping("/wanted/userIdIsIllegal")
    private RespBean userIdIsIllegal(@RequestBody Map<String,String>map)
    {
       int res = wantedService.userIdIsIllegal(map.get("userId"));
        System.out.println(res);
       if(res == 0)
       {
           return RespBean.ok("合法！");
       }
       else
       {
           return RespBean.ok("不合法！");
       }
    }
    @ApiOperation("检查userId是否存在")
    @RequestMapping("/wanted/workIdIsIllegal")
    private RespBean workIdIsIllegal(@RequestBody Map<String,String>map)
    {
        int res = wantedService.workIdIsIllegal(map.get("workId"),map.get("departmentId"));
        System.out.println(res);
        if(res == 0)
        {
            return RespBean.ok("合法！");
        }
        else
        {
            return RespBean.ok("不合法！");
        }
    }
    @ApiOperation("初始化新建员工")
    @RequestMapping("/wanted/InsertNewEmp")
    private RespBean InsertNewEmp(@RequestBody Map<String,String>map)
    {
        wantedService.InsertNewEmps(map);
       return RespBean.ok("新增成功，已邮箱告知！");
    }
    @ApiOperation("初始化新建员工")
    @RequestMapping("/wanted/InsertNewWanteds")
    private RespBean InsertNewWanteds(@RequestBody Wanted wanted)
    {
        wantedService.InsertNewWanteds(wanted);
        return RespBean.ok("新增成功，请等待面试结果！");
    }
}

