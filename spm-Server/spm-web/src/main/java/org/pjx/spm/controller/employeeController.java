package org.pjx.spm.controller;

import io.swagger.annotations.ApiOperation;
import org.pjx.spm.pojo.Employee;
import org.pjx.spm.pojo.EmployeeTool;
import org.pjx.spm.pojo.User;
import org.pjx.spm.service.Department_empServiceImpl;
import org.pjx.spm.service.EmployeeService;
import org.pjx.spm.service.EmployeeServiceImpl;
import org.pjx.spm.util.interfacespecfiaction.RespBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @Author Glow
 * @Date 2022-02-16 16:02:00
 * @Description 雇员
 * @Version 1.0
 */
@Controller
public class employeeController {
    private final static Logger logger = LoggerFactory.getLogger(employeeController.class);
    @Autowired
    private RedisTemplate<String, Object> redisTemplates;
    @Autowired
    EmployeeServiceImpl employeeService;
    @Autowired
    Department_empServiceImpl department_empService;
    @ResponseBody
    @RequestMapping(value = "/employee/getEmployee",method = RequestMethod.POST)
    @ApiOperation("根据ID获取员工信息")
    public Employee getemployee(@RequestBody Map<String,String>map)
    {
        logger.info("访问员工接口成功");
        logger.info(map.get("userId"));
        Employee employee = new Employee();
        employee = employeeService.getEmployeeById(map.get("userId"));
        logger.info("员工信息查询为："+employee.toString());
            return employee;

    }
    @ResponseBody
    @RequestMapping(value = "/employee/getEmployeeById",method = RequestMethod.POST)
    @ApiOperation("根据ID获取员工信息")
    public List<Employee> getemployeeById(@RequestBody Map<String,String>map)
    {
        logger.info("访问员工接口成功");
        logger.info(map.get("userId"));
        Employee employee = new Employee();
        employee = employeeService.getEmployeeById(map.get("userId"));
        List<Employee> list = new LinkedList<>();
        list.add(employee);
        logger.info("员工信息查询为："+employee.toString());
        return list;

    }

    @ResponseBody
    @RequestMapping(value = "/employee/getEmployeeByRole",method = RequestMethod.POST)
    @ApiOperation("根据权限获取员工信息")
    public List<Employee> getEmployeeByRole(@RequestBody Map<String,String>map)
    {
        logger.info("访问员工接口成功");
        logger.info(map.get("role"));
        Employee employee = new Employee();
        List<Employee> list = null;
        list = employeeService.getEmployeeByRole(map.get("role"));
        logger.info("员工信息查询为："+employee.toString());
        return list;

    }
    @ResponseBody
    @RequestMapping(value = "/employee/getEmployeeByRoleAnduserId",method = RequestMethod.POST)
    @ApiOperation("根据权限和员工Id获取员工信息")
    public List<Employee> getEmployeeByRoleAnduserId(@RequestBody Map<String,String>map)
    {
        logger.info("访问员工接口成功/employee/getEmployeeByRoleAnduserId");
        logger.info(map.get("role")+":"+map.get("userId"));
        Employee employee = new Employee();
        List<Employee> list = null;
        list = employeeService.getEmployeeByRoleAnduserId(map.get("userId"),map.get("role"));
        return list;

    }
    @ResponseBody
    @RequestMapping(value = "/employee/updateAddressById",method = RequestMethod.POST)
    @ApiOperation("根据ID更新员工地址")
    public RespBean updateAddressById(@RequestBody Map<String,String>map)
    {
        logger.info("访问员工接口成功");
        logger.info(map.get("userId")+":"+map.get("Address"));
        String userId = map.get("userId");
        String Address = map.get("Address");
        int res = employeeService.updateAddressById(Address,userId);
        if( res == 1)
        {
            return RespBean.ok("更新地址成功");
        }
        return RespBean.error("更新地址失败请重试");
    }

    @ResponseBody
    @RequestMapping(value = "/employee/updatePhoneById",method = RequestMethod.POST)
    @ApiOperation("根据ID更新员工电话")
    public RespBean updatePhoneById(@RequestBody Map<String,String>map)
    {
        logger.info("访问员工接口成功");
        logger.info(map.get("userId")+":"+map.get("Phone"));
        String userId = map.get("userId");
        String Phone= map.get("Phone");
        int res = employeeService.updatePhoneById(Phone,userId);
        if( res == 1)
        {
            return RespBean.ok("更新联系方式成功");
        }
        return RespBean.error("更新联系方式失败请重试");
    }
    @ResponseBody
    @RequestMapping(value = "/updateAcceptDepartmentById",method = RequestMethod.POST)
    @ApiOperation("根据ID更新员工信息")
    public RespBean updateAcceptDepartmentById(@RequestBody Employee employee) {
        logger.info("雇员更新前信息:"+employee.toString());
        int res = employeeService.updateAcceptDepartmentById(employee);
        if (res == 0) {
            return RespBean.error("更新失败，请重试");
        }
//        redisTemplates.opsForValue().set("EMPLOYEER:"+employee.getUserId(),employee);
//        redisTemplates.expire("EMPLOYEER:"+employee.getUserId(),3600, TimeUnit.SECONDS);
        return RespBean.ok("更新成功！");

    }
//    @ResponseBody
//    @RequestMapping(value = "/updateAndIdcardAcceptDepartmentById",method = RequestMethod.POST)
//    @ApiOperation("根据ID更新员工信息")
//    public RespBean updateAndIdcardAcceptDepartmentById(@RequestBody Employee employee) {
//        logger.info("更改包括身份号码的信息："+employee.toString());
//        int res = employeeService.updateAndIdcardAcceptDepartmentById(employee);
//        if (res == 0) {
//            return RespBean.error("更新失败，请重试");
//        }
//        return RespBean.ok("更新成功！");
//
//    }

    @ResponseBody
    @RequestMapping(value = "/updateIdcardById",method = RequestMethod.POST)
    @ApiOperation("根据ID更新员工身份证")
    public RespBean updateIdcardById(@RequestBody Map<String,String>map) {
        logger.info(map.get("Idcard")+":"+map.get("userId"));
        int res = employeeService.updateIdcardById(map.get("Idcard"),map.get("userId"));
        if (res == 0) {
            return RespBean.error("更新失败，请重试");
        }

        return RespBean.ok("信息全部更新成功！");

    }

    @ResponseBody
    @RequestMapping(value = "/getAllEmployee",method = RequestMethod.POST)
    @ApiOperation("获取所有员工信息")
    public List<Employee> getAllEmployee(@RequestBody Map<String,String>map) {
        String userId = map.get("userId");
        logger.info(userId);
        Employee emps = new Employee();
        emps =(Employee) redisTemplates.opsForValue().get("EMPLOYEER:"+userId);
        if(emps == null)
        {
            emps = employeeService.getEmployeeById(userId);
            redisTemplates.opsForValue().set("EMPLOYEER:"+userId,emps);
        }
        List<Employee> list = null;
        logger.info("员工信息放redis后："+emps.toString());
        if (emps.getRole().equals("admin"))
        {

            list = employeeService.getAllEmployee();
            Iterator<Employee> iterator = list.iterator();//只能前往后输出;
            while(iterator.hasNext()){
                Employee tool = new Employee();
                tool = iterator.next();
                redisTemplates.opsForValue().set("EMPLOYEER:"+tool.getUserId(),tool);
                redisTemplates.expire("EMPLOYEER:"+tool.getUserId(),3600,TimeUnit.SECONDS);
                System.out.println(tool.toString());
            }
            return list;
        }
        return list;
    }
    @ResponseBody
    @RequestMapping(value = "/getNotInEmployeeUserId",method = RequestMethod.POST)
    @ApiOperation("获取未创建雇员userId信息")
    public List<EmployeeTool> getNotInEmployeeUserId()
    {
        List<EmployeeTool> employeeTools = employeeService.getNotInEmployeeUserId();
        Iterator<EmployeeTool> iterator = employeeTools.iterator();//只能前往后输出;
        while(iterator.hasNext()){
            EmployeeTool tool = new EmployeeTool();
            tool = iterator.next();
//            redisTemplates.opsForValue().set("EMPLOYEER:"+tool.getUserId(),tool);
//            redisTemplates.expire("EMPLOYEER:"+tool.getUserId(),3600,TimeUnit.SECONDS);
            System.out.println(tool.toString());
        }
        return employeeService.getNotInEmployeeUserId();
    }
    @ResponseBody
    @RequestMapping(value = "/InsertEmployeeAcceptDepartment",method = RequestMethod.POST)
    @ApiOperation("插入除雇员的部门信息的信息")
    public RespBean InsertEmployeeAcceptDepartment(@RequestBody Employee employee)
    {
        logger.info("插入除雇员的部门信息的信息前:"+employee.toString());
        int res = employeeService.InsertEmployeeAcceptDepartment(employee);
        if(res == 1)
        {
            if(employeeService.DepEmpIsExist(employee.getUserId()) != 1)
            {
                department_empService.InsertInitDepEmp(employee.getUserId(),employee.getUserName(),employee.getRole());
            }

            return RespBean.ok("插入成功");
        }
        return RespBean.error("插入失败，数据异常！");
    }
    @ResponseBody
    @RequestMapping(value = "/deleteEmployeeByuserId",method = RequestMethod.POST)
    @ApiOperation("根据雇员的id删除")
    public RespBean deleteEmployeeByuserId(@RequestBody Map<String,String>map)
    {
        System.out.println("访问删除员工接口");
        int res = employeeService.deleteEmployeeByuserId(map.get("userId"));
        if(res == 1)
        {
            return RespBean.ok("删除"+map.get("userId")+"成功");
        }
        return RespBean.error("删除数据失败");
    }
    @ResponseBody
    @RequestMapping(value = "/employee/UpdateDepartmentByuserId",method = RequestMethod.POST)
    @ApiOperation("更新雇员信息")
    public RespBean UpdateDepartmentByuserId(@RequestBody Map<String,Object>map)
    {
            logger.info("更新emp的雇员部门信息："+map.toString());
            int res = employeeService.UpdateDepartmentByuserId(map);
            if( res == 1)
            {
                return RespBean.ok("更新emp雇员成功！");
            }
        return RespBean.error("更新emp雇员失败！");
    }
    @ApiOperation("查找所有指定部门Id的员工")
    @ResponseBody
    @RequestMapping("/selectEmpByDepId")
    private List<Employee>selectEmpByDepId(@RequestBody Map<String,String> map)
    {
        String userId = map.get("userId");
        logger.info("访问员工的获取所有部门Id的指定员工："+userId);
        Employee employee = employeeService.getEmployeeById(userId);

        Integer depId = employee.getDepartmentId();
        return employeeService.selectEmpByDepId(depId);
    }
    @ApiOperation("记录部门员工Id")
    @ResponseBody
    @RequestMapping("/selectDepId")
    private  Map<String ,Integer> selectDepId(@RequestBody Map<String,String> map)
    {
        String userId = map.get("userId");
        logger.info("selectDepId："+userId);
        Employee employee = employeeService.getEmployeeById(userId);
        System.out.println(employee.toString());
        Map<String ,Integer> maps = new HashMap<String ,Integer>();
        maps.put("departmentId",employee.getDepartmentId());
        return maps;

    }

    @ApiOperation("selectEmpByuserIdAndDepartmentId")
    @ResponseBody
    @RequestMapping("/selectEmpByuserIdAndDepartmentId")
    private  Employee selectEmpByuserIdAndDepartmentId(@RequestBody Map<String,String> map)
    {
        String userId = map.get("userId");
        Integer departmentId =Integer.valueOf(map.get("departmentId")) ;
        Employee employee = new Employee();
        employee.setUserId(userId);
        employee.setDepartmentId(departmentId);
        System.out.println(employee.toString());

        return employeeService.selectEmpByuserIdAndDepartmentId(employee);

    }
}
