package org.pjx.spm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * @Author Glow
 * @Date 2022-01-27 13:01:10
 * @Description 主程序入口
 * @Version 1.0
 */
@EnableCaching
@SpringBootApplication
public class spmApplication {
    public static void main(String[] args)
    {
        SpringApplication.run(spmApplication.class,args);
    }
}
