package org.pjx.spm.config.Inteceptor;

import org.pjx.spm.controller.loginController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.Nullable;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @Author Glow
 * @Date 2022-02-12 17:21:49
 * @Description 登录拦截token，后续增加passwordEncoder加密密码
 * @Version 1.0
 */
public class AuthInterceptor implements HandlerInterceptor {
    private final static Logger logger = LoggerFactory.getLogger(AuthInterceptor.class);
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        response.setHeader("Access-Control-Allow-Origin","*");
        logger.info("测试session");
        HttpSession httpSession =request.getSession();
        Object user =httpSession.getAttribute("userId");//取出session
        logger.info((String)user+"啊啊啊啊");
        if(user == null)
        {
            logger.info((String)request.getContextPath());
            response.sendRedirect("/");
            return false;
        }

        return true;
    }
    //视图渲染前调用，（controller方法调用之后）
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable ModelAndView modelAndView) throws Exception
    {

    }
    //请求完成后使用，资源清理
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception
    {

    }
}
