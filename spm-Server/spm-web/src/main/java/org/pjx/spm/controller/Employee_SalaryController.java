package org.pjx.spm.controller;

import io.swagger.annotations.ApiOperation;
import org.pjx.spm.pojo.Employee_Salary;
import org.pjx.spm.service.EmployeeServiceImpl;
import org.pjx.spm.service.Employee_SalaryServiceImpl;
import org.pjx.spm.service.Employee_trainServiceImpl;
import org.pjx.spm.util.interfacespecfiaction.RespBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2022-03-23 20:34:16
 * @Description
 * @Version 1.0
 */
@Controller
@RequestMapping("Employee_Salary")
public class Employee_SalaryController {
    @Autowired
    private Employee_SalaryServiceImpl employee_salaryService;
    @Autowired
    private EmployeeServiceImpl employeeService;
    @RequestMapping("SelectAllEmployeeSalaryBySelectedSalaryMonth")
    @ResponseBody
    @ApiOperation("输出所有年份月份的员工薪资表")
    private List<Employee_Salary>SelectAllEmployeeSalaryBySelectedSalaryMonth()
    {
        return employee_salaryService.SelectAllEmployeeSalaryBySelectedSalaryMonth();
    }

    @RequestMapping("SelectAllEmployeeSalaryBySalaryMonthDescBysalaryMonth")
    @ResponseBody
    @ApiOperation("获取指定年份的员工薪资表:注意，模糊查询需要带上哦%")
    private List<Employee_Salary>SelectAllEmployeeSalaryBySalaryMonthDescBysalaryMonth(@RequestBody Map<String,String>map)
    {
        System.out.println(map.get("salaryMonth"));
        return employee_salaryService.UpdateThisMonthSalary(map.get("salaryMonth"));
    }
    @RequestMapping("SelectAllDepEmployeeSalaryBySalaryMonthDescBysalaryMonth")
    @ResponseBody
    @ApiOperation("获取指定年份的员工薪资表:注意，模糊查询需要带上哦%")
    private List<Employee_Salary>SelectAllDepEmployeeSalaryBySalaryMonthDescBysalaryMonth(@RequestBody Map<String,String>map)
    {
        System.out.println(map.get("salaryMonth"));
        return employee_salaryService.SelectAllDepEmployeeSalaryBySalaryMonthDescBysalaryMonth(map.get("salaryMonth"),map.get("departmentId"));
    }
    @RequestMapping("UpdateThisMonthDepSalary")
    @ResponseBody
    @ApiOperation("获取指定年份的员工薪资表:注意，模糊查询需要带上哦%")
    private List<Employee_Salary>UpdateThisMonthDepSalary(@RequestBody Map<String,String>map)
    {
        System.out.println(map.get("salaryMonth"));
        return employee_salaryService.UpdateThisMonthDepSalary(map.get("salaryMonth"),map.get("departmentId"));
    }
    @RequestMapping("UpdateThisMonthUserSalary")
    @ResponseBody
    @ApiOperation("获取指定年份的员工薪资表:注意，模糊查询需要带上哦%")
    private List<Employee_Salary>UpdateThisMonthUserSalary(@RequestBody Map<String,String>map)
    {
        System.out.println(map.get("salaryMonth"));
        return employee_salaryService.UpdateThisMonthUserSalary(map.get("salaryMonth"),map.get("userId"));
    }

    @RequestMapping("SelectAllEmployeeSalaryByuserIdAndSalaryMonthDescBysalaryMonth")
    @ResponseBody
    @ApiOperation("获取指定年份的员工薪资表:注意，模糊查询需要带上哦%")
    private List<Employee_Salary>SelectAllEmployeeSalaryByuserIdAndSalaryMonthDescBysalaryMonth(@RequestBody Map<String,String>map)
    {
        List<Employee_Salary>list =employee_salaryService.SelectAllEmployeeSalaryByuserIdAndSalaryMonthDescBysalaryMonth(map.get("userId"),map.get("salaryMonth"));

        System.out.println(list.toString());
        Date date = new Date();
        System.out.println(date);
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM");
        System.out.println(sf.format(date));

        return list;
    }
    @RequestMapping("SelectEmpUserIdAndUsername")
    @ResponseBody
    @ApiOperation("获取empuser的信息")
    private List<Map<Object,Object>>SelectEmpUserIdAndUsername(@RequestBody Map<String,String> map)
    {
        System.out.println("要更新薪资的指定薪资："+map.get("checkdate"));
        String month = map.get("checkdate");
        if(month.length()>7)
        {
            System.out.println(map.get("checkdate").substring(0,5)+map.get("checkdate").charAt(map.get("checkdate").length()-1));
            return employeeService.selectEmpUserIdAnduserName(map.get("checkdate").substring(0,6)+map.get("checkdate").charAt(map.get("checkdate").length()-1));
        }
        return employeeService.selectEmpUserIdAnduserName(map.get("checkdate"));
    }

    @RequestMapping("selectEmpUserIdAnduserNameByDepartmentId")
    @ResponseBody
    @ApiOperation("获取empuser的信息")
    private List<Map<Object,Object>>selectEmpUserIdAnduserNameByDepartmentId(@RequestBody Map<String,String> map)
    {
        System.out.println("要更新薪资的指定薪资："+map.get("checkdate"));
        String month = map.get("checkdate");
        if(month.length()>7)
        {
            System.out.println(map.get("checkdate").substring(0,6)+map.get("checkdate").charAt(map.get("checkdate").length()-1));
            return employeeService.selectEmpUserIdAnduserNameByDepartmentId
                    (map.get("checkdate").substring(0,6)+map.get("checkdate").charAt(map.get("checkdate").length()-1),
                            map.get("departmentId"));
        }
        return employeeService.selectEmpUserIdAnduserNameByDepartmentId(map.get("checkdate"),map.get("departmentId"));
    }

    @RequestMapping("UpdateEmpSalary")
    @ResponseBody
    @ApiOperation("根据userId和月份获取指定的月薪资表")
    private RespBean UpdateEmpSalary(@RequestBody Map<Object,Object>map)
    {
        System.out.println("userId:"+map.get("userId").toString()+"\n baseSalary:"+map.get("baseSalary").toString()+"\n salaryMonth:"+map.get("salaryMonth").toString());
        if(map.get("salaryMonth").toString().length()!=7)
        {
            System.out.println(map.get("salaryMonth").toString().substring(0,5)+"0"+map.get("salaryMonth").toString().charAt(map.get("salaryMonth").toString().length()-1));
        }
        return   employee_salaryService.changeEmpSalary(map);
//    return RespBean.ok("ok");
    }
    @RequestMapping("UpdateYearEmpSalary")
    @ApiOperation("初始化某年的员工薪资表")
    @ResponseBody
    private List<Employee_Salary> UpdateYearEmpSalary(@RequestBody Map<String,String> map)
    {
        String year = map.get("salaryMonth");
        for(int i = 1 ; i <= 9; i++)
        {
            employee_salaryService.UpdateThisMonthSalary(year+"-0"+i+"%");
        }
        for(int i = 10 ; i <= 12; i++)
        {
            employee_salaryService.UpdateThisMonthSalary(year+"-"+i+"%");
        }
        return employee_salaryService.UpdateYearEmpSalary(year);
    }

    @RequestMapping("UpdateYearDepEmpSalary")
    @ApiOperation("初始化某年的部门员工薪资表")
    @ResponseBody
    private List<Employee_Salary> UpdateYearDepEmpSalary(@RequestBody Map<String,String> map)
    {
        String year = map.get("salaryMonth");
        for(int i = 1 ; i <= 9; i++)
        {
            employee_salaryService.UpdateThisMonthDepSalary(year+"-0"+i+"%",map.get("departmentId"));
        }
        for(int i = 10 ; i <= 12; i++)
        {
            employee_salaryService.UpdateThisMonthDepSalary(year+"-"+i+"%",map.get("departmentId"));
        }

        return employee_salaryService.UpdateYearDepEmpSalary(year,map.get("departmentId"));
    }
    @RequestMapping("UpdateYearUserEmpSalary")
    @ApiOperation("初始化某年的员工薪资表")
    @ResponseBody
    private List<Employee_Salary> UpdateYearUserEmpSalary(@RequestBody Map<String,String> map)
    {
        String year = map.get("salaryMonth");
        for(int i = 1 ; i <= 9; i++)
        {
            employee_salaryService.UpdateThisMonthUserSalary(year+"-0"+i+"%",map.get("userId"));
        }
        for(int i = 10 ; i <= 12; i++)
        {
            employee_salaryService.UpdateThisMonthUserSalary(year+"-"+i+"%",map.get("userId"));
        }
        return employee_salaryService.UpdateYearUserEmpSalary(year,map.get("userId"));
    }
//    @RequestMapping("InitInsert")
//    @ResponseBody
//    @ApiOperation("更新本月的薪资表")
//    private RespBean UpdateThisMonthSalary(@RequestBody List<Employee_Salary> employee_salaries)
//    {
//        ListIterator<Employee_Salary>listIterator =employee_salaries.listIterator();
//        while(listIterator.hasNext())
//        {
//            System.out.println(listIterator.next());
//        }
//        return RespBean.ok("lalala");
//    }
        @RequestMapping("selectSalaryByUserIdAndSalary")
        @ResponseBody
        @ApiOperation("根据idsalary获取去指定年份。/月份的薪资信息")
        private Employee_Salary selectSalaryByUserIdAndSalary(@RequestBody Map<Object,Object>map)
        {
           return employee_salaryService.selectSalaryByUserIdAndSalary
                   (map.get("userId").toString(),map.get("salaryMonth").toString());
        }
    @RequestMapping("selectSalaryBySalary")
    @ResponseBody
    @ApiOperation("获取指定年份。/月份的薪资信息")
    private List<Employee_Salary> selectSalaryBySalary(@RequestBody Map<Object,Object>map)
    {
        return  employee_salaryService.selectSalaryBySalary(map.get("salaryMonth").toString());
    }

}
