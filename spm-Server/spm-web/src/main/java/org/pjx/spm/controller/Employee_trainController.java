package org.pjx.spm.controller;

import io.swagger.annotations.ApiOperation;
import org.pjx.spm.pojo.Employee_Salary;
import org.pjx.spm.pojo.Employee_train;
import org.pjx.spm.service.Employee_SalaryServiceImpl;
import org.pjx.spm.service.Employee_trainServiceImpl;
import org.pjx.spm.service.UserServiceImpl;
import org.pjx.spm.util.interfacespecfiaction.RespBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2022-03-05 01:57:20
 * @Description 员工培训Controller层
 * @Version 1.0
 */
@Controller
@RequestMapping("Employee_train")
public class Employee_trainController {
    private final static Logger logger = LoggerFactory.getLogger(Employee_trainController.class);
    @Autowired
    private Employee_trainServiceImpl employee_trainService;
    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private Employee_SalaryServiceImpl employee_salaryService;
    @ApiOperation("获取员工培训表信息")
    @ResponseBody
    @RequestMapping(value = "selectAlltrain",method = RequestMethod.POST)
    private List<Employee_train>selectAlltrain()
    {
        return employee_trainService.selectAlltrain();
    }
    @ApiOperation("凭借userId获取员工培训表信息")
    @ResponseBody
    @RequestMapping(value = "selectTrainByuserId",method = RequestMethod.POST)
    private List<Employee_train>selectTrainByuserId(@RequestBody Map<String,String>map)
    {
        return employee_trainService.selectTrainByuserId(map.get("userId"));
    }
    @ApiOperation("凭借userId获取员工培训表信息")
    @ResponseBody
    @RequestMapping(value = "selectTrainByuserIdAndDepartmentId",method = RequestMethod.POST)
    private List<Employee_train>selectTrainByuserIdAndDepartmentId(@RequestBody Map<String,String>map)
    {
        return employee_trainService.selectTrainByuserIdAndDepartmentId(map.get("userId"),map.get("departmentId"));
    }
    @ApiOperation("凭借userId获取员工培训表信息")
    @ResponseBody
    @RequestMapping(value = "selectTrainByDepId",method = RequestMethod.POST)
    private List<Employee_train>selectTrainByDepId(@RequestBody Map<String,Integer>map)
    {
        return employee_trainService.selectTrainByDepId(map.get("departmentId"));
    }
    @ApiOperation("获取员工审批/已审批培训表信息")
    @ResponseBody
    @RequestMapping(value = "selectTrainByIsCheckAndDepartmentId",method = RequestMethod.POST)
    private List<Employee_train>selectTrainByIsCheckAndDepartmentId(@RequestBody Map<String,String>map)
    {

        return employee_trainService.selectTrainByIsCheckAndDepartmentId(map.get("userId"),map.get("ischeck"),map.get("departmentId"));
    }
    @ApiOperation("获取员工完成/未完成培训表信息")
    @ResponseBody
    @RequestMapping(value = "selectTrainByIsFinishAndDepartmentId",method = RequestMethod.POST)
    private List<Employee_train>selectTrainByIsFinishAndDepartmentId(@RequestBody Map<String,String>map)
    {

        return employee_trainService.selectTrainByIsFinishAndDepartmentId(map.get("userId"),map.get("isfinish"),map.get("departmentId"));
    }
    @ApiOperation("获取员工审批/已审批培训表信息")
    @ResponseBody
    @RequestMapping(value = "selectTrainByIsCheck",method = RequestMethod.POST)
    private List<Employee_train>selectTrainByIsCheck(@RequestBody Map<String,String>map)
    {

        return employee_trainService.selectTrainByIsCheck(map.get("userId"),map.get("ischeck"));
    }
    @ApiOperation("获取员工完成/未完成培训表信息")
    @ResponseBody
    @RequestMapping(value = "selectTrainByIsFinish",method = RequestMethod.POST)
    private List<Employee_train>selectTrainByIsFinish(@RequestBody Map<String,String>map)
    {

        return employee_trainService.selectTrainByIsFinish(map.get("userId"),map.get("isfinish"));
    }
    @ApiOperation("获取员工完成/未完成培训表信息")
    @ResponseBody
    @RequestMapping(value = "selectTrainEmp",method = RequestMethod.POST)
    private List<Map<Object,Object>>selectTrainEmp()
    {

        return employee_trainService.selectTrainEmp();
    }
    @ApiOperation("获取员工完成/未完成培训表信息")
    @ResponseBody
    @RequestMapping(value = "selectTrainEmpByDepId",method = RequestMethod.POST)
    private List<Map<Object,Object>>selectTrainEmpByDepId(@RequestBody Map<String,Integer> map)
    {

        return employee_trainService.selectTrainEmpByDepId(map.get("departmentId"));
    }
    @ApiOperation("增加员工培训信息")
    @ResponseBody
    @RequestMapping(value = "InsertNewTrain",method = RequestMethod.POST)
    private RespBean InsertNewTrain(@RequestBody Employee_train employee_train)
    {
        employee_trainService.UpdateisTrain(employee_train.getUserId());
        employee_trainService.InsertNewTrain(employee_train);

        return RespBean.ok("新增员工培训成功");
    }
    @ApiOperation("取消员工培训")
    @ResponseBody
    @RequestMapping(value = "CancelNotCheckTrain",method = RequestMethod.POST)
    private RespBean CancelNotCheckTrain(@RequestBody Employee_train employee_train)
    {
        employee_trainService.DeleteNotCheckTrain(employee_train.getTrainrage());
        employee_trainService.UpdateCancelTrain(employee_train.getUserId());
        return RespBean.ok("删除员工成功");
    }

    @ApiOperation("开始员工培训")
    @ResponseBody
    @RequestMapping(value = "StartTrain",method = RequestMethod.POST)
    private RespBean StartTrain(@RequestBody Employee_train employee_train)
    {

        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM");
        String role =userService.selectroleByuserId(employee_train.getUserId());
        String checkdate = sf.format(employee_train.getTrainstartdate())+"%";
        String userId = employee_train.getUserId();
        String username = employee_train.getUsername();
        List<Employee_Salary> employee_salaryList =employee_salaryService.
                SelectAllEmployeeSalaryByuserIdAndSalaryMonthDescBysalaryMonth
                        (userId,checkdate);
        if(employee_salaryList.toString().equals("[]"))
        {

            System.out.println("role:"+role);
            System.out.println(employee_salaryList.toString());
            Employee_Salary employee_salary = new Employee_Salary();
            employee_salary.setUserId(userId);
            employee_salary.setUsername(username);
            employee_salary.setRole(role);
            employee_salary.setSalaryMonth(sf.format(employee_train.getTrainstartdate()));
            employee_salaryService.InsertEmployeeSalaryInIt(employee_salary);
        }
        employee_trainService.UpdateCheckTrainStart(employee_train);


        return RespBean.ok("员工已开始培训");
    }
    @ApiOperation("完成员工培训")
    @ResponseBody
    @RequestMapping(value = "FinishTrain",method = RequestMethod.POST)
    private RespBean FinishTrain(@RequestBody Employee_train employee_train)
    {
        employee_trainService.UpdateFinishTrain(employee_train);
        employee_trainService.UpdateCancelTrain(employee_train.getUserId());
        return RespBean.ok("员工已完成培训");
    }
}
