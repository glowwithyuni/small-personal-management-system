package org.pjx.spm.service;

import org.pjx.spm.mapper.Work_attendanceMapper;

/**
 * @Author Glow
 * @Date 2022-03-16 15:57:00
 * @Description
 * @Version 1.0
 */
public interface Work_attendanceService  extends Work_attendanceMapper {

}
