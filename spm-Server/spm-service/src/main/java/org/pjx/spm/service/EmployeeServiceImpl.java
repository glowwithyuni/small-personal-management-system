package org.pjx.spm.service;

import org.apache.ibatis.annotations.Param;
import org.pjx.spm.mapper.EmployeeMapper;
import org.pjx.spm.pojo.Employee;
import org.pjx.spm.pojo.EmployeeTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2022-02-16 16:24:54
 * @Description 员工信息业务层
 * @Version 1.0
 */
@Service
public class EmployeeServiceImpl implements EmployeeService{
    @Autowired
    EmployeeMapper employeeMapper;
    @Override
    public Employee getEmployeeById(String userId) {
        return  employeeMapper.getEmployeeById(userId);
    }

    @Override
    public int updateAddressById( String Address,String userId) {
        return employeeMapper.updateAddressById(Address, userId);
    }

    @Override
    public int updatePhoneById(String Phone,String userId ) {
        return employeeMapper.updatePhoneById(Phone,userId);
    }

    @Override
    public int updateAcceptDepartmentById(Employee employee) {
        return employeeMapper.updateAcceptDepartmentById(employee);
    }

    @Override
    public int updateAndIdcardAcceptDepartmentById(Employee employee) {
        return employeeMapper.updateAndIdcardAcceptDepartmentById(employee);
    }

    @Override
    public int updateIdcardById(String Idcard, String userId) {
        return employeeMapper.updateIdcardById(Idcard, userId);
    }

    @Override
    public List<Employee> getAllEmployee() {
        return employeeMapper.getAllEmployee();
    }
    @Override
    public List<Employee> getEmployeeByRole(String role) {
        return employeeMapper.getEmployeeByRole(role);
    }
    @Override
    public List<Employee> getEmployeeByRoleAnduserId(String userId, String role)
    {
        return employeeMapper.getEmployeeByRoleAnduserId(userId,role);
    }
    //获取未创建雇员的userId
    @Override
    public List<EmployeeTool> getNotInEmployeeUserId() {
        return employeeMapper.getNotInEmployeeUserId();
    }

    @Override
    public int InsertEmployeeAcceptDepartment(Employee employee) {
        return employeeMapper.InsertEmployeeAcceptDepartment(employee);
    }

    @Override
    public int deleteEmployeeByuserId(String userId) {
        return employeeMapper.deleteEmployeeByuserId(userId);
    }


    //部门管理的惹
    @Override
    public int UpdateDepartmentByuserId(Map<String, Object> map) {
        return employeeMapper.UpdateDepartmentByuserId(map);
    }

    @Override
    public int UpdateDepartmentClearByuserId(String userId) {
        return employeeMapper.UpdateDepartmentClearByuserId(userId);
    }

    @Override
    public int updatedepEmployeeName(Map<String,Object> map) {
        return employeeMapper.updatedepEmployeeName(map);
    }

    @Override
    public List<Map<Object, Object>> selectYearSalary(String salaryMonth) {
        return employeeMapper.selectYearSalary(salaryMonth);
    }

    @Override
    public List<Map<Object, Object>> selectDepYearSalary(String salaryMonth, String departmentId) {
        return employeeMapper.selectDepYearSalary(salaryMonth, departmentId);
    }

    @Override
    public Map<Object, Object> selectUserYearSalary(String salaryMonth, String userId) {
        return employeeMapper.selectUserYearSalary(salaryMonth, userId);
    }

    @Override
    public List<Map<Object, Object>> selectEmpUserIdAnduserName(String salaryMonth) {
        return employeeMapper.selectEmpUserIdAnduserName(salaryMonth);
    }

    @Override
    public List<Map<Object, Object>> selectEmpUserIdAnduserNameByDepartmentId(String salaryMonth,String departmentId) {
        return employeeMapper.selectEmpUserIdAnduserNameByDepartmentId(salaryMonth,departmentId);
    }

    @Override
    public String selectRoleByUserId(String userId) {
        return employeeMapper.selectRoleByUserId(userId);
    }

    @Override
    public int InsertInitEmp(String userId, String username, String role, String email) {
        return employeeMapper.InsertInitEmp(userId,username,role,email);
    }

    @Override
    public int DepEmpIsExist(String userId) {
        return employeeMapper.DepEmpIsExist(userId);
    }

    @Override
    public List<Employee> selectEmpByDepId(Integer departmentId) {
        return employeeMapper.selectEmpByDepId(departmentId);
    }

    @Override
    public Employee selectEmpByuserIdAndDepartmentId(Employee employee) {
        return employeeMapper.selectEmpByuserIdAndDepartmentId(employee);
    }
}
