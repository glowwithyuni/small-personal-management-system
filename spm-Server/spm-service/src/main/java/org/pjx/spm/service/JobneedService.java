package org.pjx.spm.service;

import org.pjx.spm.mapper.JobneedMapper;

import java.util.List;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2022-04-14 22:18:37
 * @Description
 * @Version 1.0
 */
public interface JobneedService extends JobneedMapper {
    @Override
    List<Integer> selectDepartmentId();

    @Override Map<Object, Object> selectDepByDepartmentId(Integer departmentId);
}
