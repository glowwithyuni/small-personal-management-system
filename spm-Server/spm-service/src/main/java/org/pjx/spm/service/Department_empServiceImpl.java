package org.pjx.spm.service;

import org.pjx.spm.mapper.Department_empMapper;
import org.pjx.spm.pojo.Department;
import org.pjx.spm.pojo.Department_emp;
import org.pjx.spm.util.interfacespecfiaction.RespBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2022-03-03 10:49:36
 * @Description department服务层
 * @Version 1.0
 */
@Service
public class Department_empServiceImpl implements Department_empService{
   @Autowired
   private Department_empMapper department_empMapper;


    @Override
    /**
     * 获取所有部门成员信息
     */
    public List<Department_emp> getAllDepartmentEmp() {
        return department_empMapper.getAllDepartmentEmp();
    }

    /**
     * 获取指定部门的员工信息
     * @param departmentId
     * @return
     */
    @Override
    public List<Department_emp> getDepartmentEmpBydepartmentId(Integer departmentId) {
        return department_empMapper.getDepartmentEmpBydepartmentId(departmentId);
    }

    /**
     * 根据员工Id获取员工所在部门的个人职位信息
     * @param userId
     * @return
     */
    @Override
    public List<Department_emp> getDepartmentEmpByuserId(String userId) {
        return department_empMapper.getDepartmentEmpByuserId(userId);
    }

    /**
     * 根据部门Id获取部门的部长ID和部长名
     * @param departmentId
     * @return
     */
    @Override
    public List<Department_emp> getDepartmenterByDepartmentId(Integer departmentId) {
        return department_empMapper.getDepartmenterByDepartmentId(departmentId);
    }

    /**
     * 根据userId设置进入员工加入部门的信息
     * @param department_emp
     * @return
     */
    @Override
    public int updateEmpByuserId(Department_emp department_emp) {
        return department_empMapper.updateEmpByuserId(department_emp);
    }

    /**
     * 统计部门人数
     * @param departmentId
     * @return
     */
    @Override
    public int getDepartmentNum(Integer departmentId) {
        return department_empMapper.getDepartmentNum(departmentId);
    }

    /**
     * 更新部门人数
     * @param departmentId
     * @return
     */
    @Override
    public int UpdateDepartmentNum(Integer departmentId) {
        return department_empMapper.UpdateDepartmentNum(departmentId);
    }

    /**
     * 移除指定userId的部门员工置于闲置
     * @param userId
     * @return
     */
    @Override
    public int UpdateRemoveEmpInDepartmentByUserId(String userId) {
        return department_empMapper.UpdateRemoveEmpInDepartmentByUserId(userId);
    }

    /**
     * 创建新部门 ，无设定部长的情况下
     * @param department
     * @return
     */
    @Override
    public int InsertnewDepartmentWithoutEmp(Department department) {
        return department_empMapper.InsertnewDepartmentWithoutEmp(department);
    }

    /**
     * 创建新部门 ，选定部长的情况下
     * @param department
     * @return
     */
    @Override
    public int InsertnewDepartment(Department department) {
        return department_empMapper.InsertnewDepartment(department);
    }

    /**
     * 更换部长1-1
     * @param departmentId
     * @return
     */
    @Override
    public List<Department_emp> getOriginDepartmenter(Integer departmentId) {
        return department_empMapper.getOriginDepartmenter(departmentId);
    }

    /**
     * 更换部长1-2
     * @param department
     * @return
     */
    @Override
    public int updateChangeDepartmenter(Department department) {
        return department_empMapper.updateChangeDepartmenter(department);
    }

    /**
     * 更换部长1-3
     * @param department_emp
     * @return
     */
    @Override
    public int updatenewDepartmentEmpByuserId(Department_emp department_emp) {
        return department_empMapper.updatenewDepartmentEmpByuserId(department_emp);
    }

    /**
     * 更换部长1-4
     * @param userId
     * @return
     */
    @Override
    public int updateoldDepartmentEmpByuserId(String userId) {
        return department_empMapper.updateoldDepartmentEmpByuserId(userId);
    }

    /**
     * 查看未安排的部门成员
     * @return
     */
    @Override
    public List<Department_emp> getnoworkEmp() {
        return department_empMapper.getnoworkEmp();
    }

    /**
     * 查看安排的部门成员
     * @return
     */
    @Override
    public List<Department_emp> getworkEmp() {
        return department_empMapper.getworkEmp();
    }

    /**
     * 修改部门名1-1
     * @param departmentName
     * @param departmentId
     * @return
     */
    @Override
    public int updatedepartmentName(String departmentName, Integer departmentId) {
        return department_empMapper.updatedepartmentName(departmentName,departmentId);
    }

    /**
     * 修改部门名1-2
     * @param departmentName
     * @param departmentId
     * @return
     */
    @Override
    public int updatedepartmentEmpName(String departmentName, Integer departmentId) {
        return department_empMapper.updatedepartmentEmpName(departmentName,departmentId);
    }

    /**
     * 查看所有部门信息
     * @return
     */
    @Override
    public List<Department> getDepartment() {
        return department_empMapper.getDepartment();
    }

    @Override
    public List<Department> getDepartmentByDepartmentId(Integer departmentId) {
        return department_empMapper.getDepartmentByDepartmentId(departmentId);
    }

    @Override
    public List<Department_emp> getUserIdByNoDepartment() {
        return department_empMapper.getUserIdByNoDepartment();
    }

    @Override
    public Map<String,Object> getUserNameByNoDepartment(String userId) {
        return department_empMapper.getUserNameByNoDepartment(userId);
    }

    @Override
    public int deleteDepartmentByDepartmentId(Integer departmentId) {
        return department_empMapper.deleteDepartmentByDepartmentId(departmentId);
    }
    //    根据depId查询部门成员信息
    @Override
    public List<Department_emp> selectDepByDepartmentId(Integer departmentId) {
        return department_empMapper.selectDepByDepartmentId(departmentId);
    }
    //    根据userId查询部门成员信息
    @Override
    public List<Department_emp> selectDepByuserId(String userId) {
        return department_empMapper.selectDepByuserId(userId);
    }
//    根据depId查询部门详细信息
    @Override
    public Department selectDepMsgByDepartmentId(Integer departmentId) {
        return department_empMapper.selectDepMsgByDepartmentId(departmentId);
    }
//      根据userId更改普通成员姓名信息
    @Override
    public int updateDepuserNameByuserId(String username, String workName,String userId) {
        return department_empMapper.updateDepuserNameByuserId(username,workName,userId);
    }
//        根据userId更改部门部长姓名信息
    @Override
    public int updateMinisteruserNameByuserId(String username, String workName,String userId,String departmentName,String departmentId) {
        return department_empMapper.updateMinisteruserNameByuserId(username, workName,userId,departmentName,departmentId);
    }

    /**
     * 移除部门成员
     * @param userId
     * @param departmentId
     * @return
     */
    @Override
    public int removeDepartmentMate(String userId, String departmentId) {
        return department_empMapper.removeDepartmentMate(userId,departmentId);
    }

    /**
     * 添加部门成员
     * @param departmentId
     * @param departmentName
     * @param userId
     * @param workName
     * @return
     */
    @Override
    public int addnewDepMate(String departmentId, String departmentName, String userId, String workName,String workId) {
        return department_empMapper.addnewDepMate(departmentId, departmentName, userId, workName,workId);
    }

    /**
     * 获取素有DepId
     * @return
     */
    @Override
    public List<String> selectDepIdFromDepartment() {
        return department_empMapper.selectDepIdFromDepartment();
    }

    @Override
    public String selectDepNameFromDepartmentByDepId(String departmentId) {
        return department_empMapper.selectDepNameFromDepartmentByDepId(departmentId);
    }

    /**
     * 获取无安排就职的员工
     * @return
     */
    @Override
    public List<String> selectNoWorkuserId() {
        return department_empMapper.selectNoWorkuserId();
    }

    /**
     * 更新部长
     * @param departmentId
     * @param departmentName
     * @param workId
     * @param workName
     * @param userId
     * @return
     */
    @Override
    public int UpdateNewDep(String departmentId, String departmentName, String workId, String workName, String userId) {
        return department_empMapper.UpdateNewDep(departmentId, departmentName, workId, workName, userId);
    }

    /**
     * 更新旧部长信息
     * @param userId
     * @return
     */
    @Override
    public int clearOldDep(String userId) {
        return department_empMapper.clearOldDep(userId);
    }

    /**
     * 清空部门
     * @param departmentId
     * @param userId
     * @return
     */
    @Override
    public int UpdateOutputEmpAndDep(String departmentId, String userId) {
        return department_empMapper.UpdateOutputEmpAndDep(departmentId, userId);
    }

    @Override
    public List<Department_emp> selectdepartment_empByrole() {
        return department_empMapper.selectdepartment_empByrole();
    }

    @Override
    public int InsertInitDepEmp(String userId, String username, String role) {
        return department_empMapper.InsertInitDepEmp(userId, username, role);
    }

    @Override
    public List<Department_emp> selectDepartmentEmpByDepartmentId(Integer departmentId) {
        return department_empMapper.selectDepartmentEmpByDepartmentId(departmentId);
    }

    @Override
    public Department selectDepartmentByDepartmentId(Integer departmentId) {
        return department_empMapper.selectDepartmentByDepartmentId(departmentId);
    }

    @Override
    public List<Integer> selectAlldepartmentId() {
        return department_empMapper.selectAlldepartmentId();
    }

    @Override
    public int updateDepNum(Integer departmentId) {
        return department_empMapper.updateDepNum(departmentId);
    }

    @Override
    public int updateSalaryPower( String role,String userId, String salaryMonth) {
        return department_empMapper.updateSalaryPower(role, userId, salaryMonth);
    }

    public void updateNum()
    {
        List<Integer> list = department_empMapper.selectAlldepartmentId();
        ListIterator listIterator = list.listIterator();
        while (listIterator.hasNext())
        {
            Integer depId = (Integer) listIterator.next();
            department_empMapper.updateDepNum(depId);
        }

    }
    //根据userId修改权限
    @Override
    public int updateUserRoleByuserId(String role, String userId) {
        return department_empMapper.updateUserRoleByuserId(role, userId);
    }

    @Override
    public int updateEmpRoleByuserId(String role, String userId) {
        return department_empMapper.updateEmpRoleByuserId(role, userId);
    }

    @Override
    public int updateDepRoleByuserId(String role, String userId) {
        return department_empMapper.updateDepRoleByuserId(role, userId);
    }
//根据departmentId修改部门名
    @Override
    public int updateEmpDepartmentNameByDepartmentId(String departmentName, Integer departmentId) {
        return department_empMapper.updateEmpDepartmentNameByDepartmentId(departmentName, departmentId);
    }

    @Override
    public int updateDepEmpDepartmentNameByDepartmentId(String departmentName, Integer departmentId) {
        return department_empMapper.updateDepEmpDepartmentNameByDepartmentId(departmentName, departmentId);
    }

    @Override
    public int updateDepDepartmentNameByDepartmentId(String departmentName, Integer departmentId) {
        return department_empMapper.updateDepDepartmentNameByDepartmentId(departmentName,departmentId);
    }

    @Override
    public List<String> getDepartment_empuserIdBydepartmentId(Integer departmentId) {
        return department_empMapper.getDepartment_empuserIdBydepartmentId(departmentId);
    }

    @Override
    public int ClearDepartment_emp(String userId) {
        return department_empMapper.ClearDepartment_emp(userId);
    }

    @Override
    public int ClearEmployeeDepartment(String userId) {
        return department_empMapper.ClearEmployeeDepartment(userId);
    }
}
