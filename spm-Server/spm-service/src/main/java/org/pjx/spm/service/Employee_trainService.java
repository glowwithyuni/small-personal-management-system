package org.pjx.spm.service;

import org.pjx.spm.mapper.Employee_trainMapper;

/**
 * @Author Glow
 * @Date 2022-03-05 01:51:46
 * @Description 员工培训service层
 * @Version 1.0
 */
public interface Employee_trainService extends Employee_trainMapper {
}
