package org.pjx.spm.service;

import org.pjx.spm.mapper.JobneedMapper;
import org.pjx.spm.pojo.Jobneed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2022-04-14 22:19:39
 * @Description 招聘信息服务层
 * @Version 1.0
 */
@Service
public class JobneedServiceImpl  implements JobneedService{
    @Autowired
    private JobneedMapper jobneedService;

    @Override
    public List<Jobneed> selectAllJobneed() {
        return jobneedService.selectAllJobneed();
    }

    @Override
    public List<Jobneed> selectJobneedBydepartmentId(Integer departmentId) {
        return jobneedService.selectJobneedBydepartmentId(departmentId);
    }

    @Override
    public int InsertJobneed(Jobneed jobneed) {
        return jobneedService.InsertJobneed(jobneed);
    }

    @Override
    public int updateJobneedByJobrage(Jobneed jobneed) {
        return jobneedService.updateJobneedByJobrage(jobneed);
    }

    @Override
    public int deleteJobneedByJobrage(Integer jobrage) {
        return jobneedService.deleteJobneedByJobrage(jobrage);
    }

    @Override
    public Map<Object, Object> SelectDepUserNameBeforeInsert(String userId) {
        return jobneedService.SelectDepUserNameBeforeInsert(userId);
    }

    @Override
    public List<Integer> selectDepartmentId() {
        return jobneedService.selectDepartmentId();
    }

    @Override
    public Map<Object, Object> selectDepByDepartmentId(Integer departmentId) {
        return jobneedService.selectDepByDepartmentId(departmentId);
    }
}
