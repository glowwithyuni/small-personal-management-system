package org.pjx.spm.service;

import org.pjx.spm.mapper.UserMapper;

/**
 * @Author Glow
 * @Date 2022-01-27 13:14:59
 * @Description
 * @Version 1.0
 */
public interface UserService extends UserMapper {
}
