package org.pjx.spm.service;

import org.pjx.spm.mapper.EmployeeMapper;

/**
 * @Author Glow
 * @Date 2022-02-16 16:24:28
 * @Description 员工管理服务层
 * @Version 1.0
 */
public interface EmployeeService extends EmployeeMapper {
}
