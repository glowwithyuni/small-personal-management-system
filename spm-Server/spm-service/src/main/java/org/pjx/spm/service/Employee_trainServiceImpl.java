package org.pjx.spm.service;

import org.pjx.spm.mapper.Employee_trainMapper;
import org.pjx.spm.pojo.Employee_train;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2022-03-05 01:52:29
 * @Description 员工培训service层
 * @Version 1.0
 */
@Service
public class Employee_trainServiceImpl implements Employee_trainService{
    @Autowired
    private Employee_trainMapper employee_trainMapper;

    /**
     * 查询所有培训信息
     * @return
     */
    @Override
    public List<Employee_train> selectAlltrain() {
        return employee_trainMapper.selectAlltrain();
    }

    /**
     * 根据userId查询所有培训信息
     * @param userId
     * @return
     */
    @Override
    public List<Employee_train> selectTrainByuserId(String userId) {
        return employee_trainMapper.selectTrainByuserId(userId);
    }

    @Override
    public List<Employee_train> selectTrainByuserIdAndDepartmentId(String userId, String departmentId) {
        return employee_trainMapper.selectTrainByuserIdAndDepartmentId(userId, departmentId);
    }

    @Override
    public List<Employee_train> selectTrainByDepId(Integer departmentId) {
        return employee_trainMapper.selectTrainByDepId(departmentId);
    }

    /**
     * 查询是否被审批
     * @param ischeck
     * @return
     */
    @Override
    public List<Employee_train> selectTrainByIsCheck(String userId,String ischeck) {
        return employee_trainMapper.selectTrainByIsCheck(userId,ischeck);
    }

    /**
     * 查询是否完成培训
     * @param isfinish
     * @return
     */
    @Override
    public List<Employee_train> selectTrainByIsFinish(String userId,String isfinish) {
        return employee_trainMapper.selectTrainByIsFinish(userId,isfinish);
    }

    @Override
    public List<Employee_train> selectTrainByIsCheckAndDepartmentId(String userId, String ischeck, String departmentId) {
        return employee_trainMapper.selectTrainByIsCheckAndDepartmentId(userId, ischeck, departmentId);
    }

    @Override
    public List<Employee_train> selectTrainByIsFinishAndDepartmentId(String userId, String isfinish, String departmentId) {
        return employee_trainMapper.selectTrainByIsFinishAndDepartmentId(userId, isfinish, departmentId);
    }

    @Override
    public List<Map<Object,Object>> selectTrainEmp() {
        return employee_trainMapper.selectTrainEmp();
    }

    @Override
    public List<Map<Object, Object>> selectTrainEmpByDepId(Integer departmentId) {
        return employee_trainMapper.selectTrainEmpByDepId(departmentId);
    }

    @Override
    public int InsertNewTrain(Employee_train employee_train) {
        return employee_trainMapper.InsertNewTrain(employee_train);
    }

    @Override
    public int UpdateisTrain(String userId) {
        return employee_trainMapper.UpdateisTrain(userId);
    }

    @Override
    public int UpdateCancelTrain(String userId) {
        return employee_trainMapper.UpdateCancelTrain(userId);
    }

    @Override
    public int DeleteNotCheckTrain(Integer trainrage) {
        return employee_trainMapper.DeleteNotCheckTrain(trainrage);
    }

    @Override
    public int UpdateCheckTrainStart(Employee_train employee_train) {
        System.out.println(employee_train.toString());
        return employee_trainMapper.UpdateCheckTrainStart(employee_train);
    }

    /**
     * 结束培训上缴报表~
     * @param employee_train
     * @return
     */
    @Override
    public int UpdateFinishTrain(Employee_train employee_train) {
        return employee_trainMapper.UpdateFinishTrain(employee_train);
    }

    @Override
    public List<Employee_train> selectTrainByIdAndStartDate(String userId, String trainstartdate) {
        return employee_trainMapper.selectTrainByIdAndStartDate(userId, trainstartdate);
    }

    @Override
    public int updatesetaddBytrainrage(Integer trainrage, Integer isadd) {
        return employee_trainMapper.updatesetaddBytrainrage(trainrage, isadd);
    }

}
