package org.pjx.spm.service;

import org.pjx.spm.mapper.UserMapper;
import org.pjx.spm.pojo.Employee;
import org.pjx.spm.pojo.User;
import org.pjx.spm.pojo.employee_request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * @Author Glow
 * @Date 2022-01-27 13:16:22
 * @Description
 * @Version 1.0
 */
@Service
public class UserServiceImpl implements UserService{
    @Autowired
    UserMapper userMapper;
    //    邮件
    @Autowired
    JavaMailSenderImpl mailSender;
    @Override
    public int accountIsTrue(String userId, String password) {
        return userMapper.accountIsTrue(userId,password);
    }

    @Override
    public User selectUserById(String userId) {
        return userMapper.selectUserById(userId);
    }

    @Override
    public int changePassword(User user) {
        return userMapper.changePassword(user);
    }

    @Override
    public int userIsExistById(String userId) {
        return userMapper.userIsExistById(userId);
    }

    @Override
    public int requestsetpassword(employee_request employeeRequest) {
        return userMapper.requestsetpassword(employeeRequest);
    }
    @Override
    public int userrequestIsExistById(String userId) {
        return userMapper.userrequestIsExistById(userId);
    }

    @Override
    public int deletepasswordrequestById(String userId) {
        return userMapper.deletepasswordrequestById(userId);
    }

    @Override
    public int userrequestAndCheckcodeIsExistById(String userId,String checkcode) {
        return userMapper.userrequestAndCheckcodeIsExistById(userId,checkcode);
    }

    @Override
    public int getnewpassword(User user) {
        return userMapper.getnewpassword(user);
    }

    @Override
    public String getoriginpasswordById(String userId) {
        return userMapper.getoriginpasswordById(userId);
    }

    @Override
    public int updateusernameAndemailById(User user) {
        return userMapper.updateusernameAndemailById(user);
    }

    @Override
    public Map<String,Object> getUsernameAndEmailAndroleById(String userId) {
        return userMapper.getUsernameAndEmailAndroleById(userId);
    }

    @Override
    public int updateusernameAndemailAndroleById(User user) {
        return userMapper.updateusernameAndemailAndroleById(user);
    }
    public int resetPasswordByEmail(Map<String,String>map)
    {
        String str="abcdefghijklmnopqrstuvwxyz1234567890";
        String userId = map.get("userId");
        String email = map.get("email");
        if(userMapper.UserIdAndEmailIsExist(userId,email)==1)
        {
            Random random = new Random();
            StringBuffer sb = new StringBuffer();
            for(int i=0;i<10;i++) {
                int number = random.nextInt(36);            //可随机产生字符的数量
                sb.append(str.charAt(number));
            }
            String password = sb.toString();
            userMapper.updatePasswordByUserId(password,userId);
            try{

                int count=1;//默认发送一次
                MimeMessage mimeMessage = mailSender.createMimeMessage();
                MimeMessageHelper helper = new MimeMessageHelper(mimeMessage,true);
                while(count--!=0){
//                String codeNum = "";
//                int [] code = new int[3];
//                Random random = new Random();
//                //自动生成验证码
//                for (int i = 0; i < 6; i++) {
//                    int num = random.nextInt(10) + 48;
//                    int uppercase = random.nextInt(26) + 65;
//                    int lowercase = random.nextInt(26) + 97;
//                    code[0] = num;
//                    code[1] = uppercase;
//                    code[2] = lowercase;
//                    codeNum+=(char)code[random.nextInt(3)];
//                }
                    //标题
                    helper.setSubject("重置密码通知：");
                    //内容
                    helper.setText("您好！，您的新密码已更新。您的账号为："+"<h2>"+userId+".新密码为"+password+"</h2>"+"千万不能告诉别人哦！",true);
                    //邮件接收者
                    helper.setTo(email);
                    //邮件发送者，必须和配置文件里的一样，不然授权码匹配不上
                    helper.setFrom("654763560@qq.com");
                    mailSender.send(mimeMessage);
                    System.out.println("邮件发送成功！");
                }


            }catch (MessagingException e)
            {
                e.printStackTrace();
            }



            return 1;
        }
        return  0 ;
    }

    @Override
    public int InsertInitDepartmentMate(Map<String, Object> map) {
        return userMapper.InsertInitDepartmentMate(map);
    }

    @Override
    public String selectroleByuserId(String userId) {
        return userMapper.selectroleByuserId(userId);
    }

    @Override
    public int UserIdAndEmailIsExist(String userId, String email) {
        return userMapper.UserIdAndEmailIsExist(userId, email);
    }

    @Override
    public int updatePasswordByUserId(String password, String userId) {
        return userMapper.updatePasswordByUserId(password, userId);
    }

    @Override
    public List<employee_request> selectAllEmployee_request() {
        return userMapper.selectAllEmployee_request();
    }

    @Override
    public int updateResetEmpRequestCode(employee_request employee_requests) {
        return userMapper.updateResetEmpRequestCode(employee_requests);
    }
    public int updateResetEmpRequestCodes(employee_request employee_requests) {
        String str="abcdefghijklmnopqrstuvwxyz1234567890";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for(int i=0;i<10;i++) {
            int number = random.nextInt(36);            //可随机产生字符的数量
            sb.append(str.charAt(number));
        }
        String password = sb.toString();
        employee_requests.setRequestcode(password);
        employee_requests.setIsavailable(0);
        userMapper.updateResetEmpRequestCode(employee_requests);
        String email =userMapper.selectEmailByuserId(employee_requests.getUserId());

        try{

            int count=1;//默认发送一次
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage,true);
            while(count--!=0){
//                String codeNum = "";
//                int [] code = new int[3];
//                Random random = new Random();
//                //自动生成验证码
//                for (int i = 0; i < 6; i++) {
//                    int num = random.nextInt(10) + 48;
//                    int uppercase = random.nextInt(26) + 65;
//                    int lowercase = random.nextInt(26) + 97;
//                    code[0] = num;
//                    code[1] = uppercase;
//                    code[2] = lowercase;
//                    codeNum+=(char)code[random.nextInt(3)];
//                }
                //标题
                helper.setSubject("重置密码通知：");
                //内容
                helper.setText("您好！，您的新密码已更新。您的账号为："+"<h2>"+employee_requests.getUserId()+".新密码为"+password+"</h2>"+
                        "<br>如果不方便请在提取密码界面输入checkcode"+employee_requests.getCheckcode()+".千万不能告诉别人哦！",true);
                //邮件接收者
                helper.setTo(email);
                //邮件发送者，必须和配置文件里的一样，不然授权码匹配不上
                helper.setFrom("654763560@qq.com");
                mailSender.send(mimeMessage);
                System.out.println("邮件发送成功！");
            }


        }catch (MessagingException e)
        {
            e.printStackTrace();
        }




        return 1;
    }
    @Override
    public String selectEmailByuserId(String userId) {
        return userMapper.selectEmailByuserId(userId);
    }

    @Override
    public int updatetokenByuserId(String token, String userId) {
        return userMapper.updatetokenByuserId(token, userId);
    }

    @Override
    public String selecttokenByuserId(String userId) {
        return userMapper.selecttokenByuserId(userId);
    }
}
