package org.pjx.spm.service;

import org.pjx.spm.mapper.WantedMapper;
import org.pjx.spm.pojo.Department_emp;
import org.pjx.spm.pojo.Employee;
import org.pjx.spm.pojo.User;
import org.pjx.spm.pojo.Wanted;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * @Author Glow
 * @Date 2022-04-15 00:24:08
 * @Description
 * @Version 1.0
 */
@Service
public class WantedServiceImpl  implements WantedService{
    @Autowired
    WantedMapper wantedMapper;
//    邮件
@Autowired
JavaMailSenderImpl mailSender;
    @Override
    public int deleteWantedByWantedRage(Integer wantedrage) {
        return wantedMapper.deleteWantedByWantedRage(wantedrage);
    }

    @Override
    public int InsertNewWanted(Wanted wanted) {
        return wantedMapper.InsertNewWanted(wanted);
    }

    @Override
    public int updateWantedByWantedRage(Wanted wanted) {
        return wantedMapper.updateWantedByWantedRage(wanted);
    }

    @Override
    public List<Wanted> selectWantedBydepartmentId(Integer departmentId) {
        return wantedMapper.selectWantedBydepartmentId(departmentId);
    }

    @Override
    public List<Wanted> selectAllWanted() {
        return wantedMapper.selectAllWanted();
    }

    @Override
    public List<Wanted> selectWantedByDepartmentIdAndIsaccept(Integer departmentId, Integer isaccept) {
        return wantedMapper.selectWantedByDepartmentIdAndIsaccept(departmentId,isaccept);
    }

    @Override
    public List<Wanted> selectWantedByIsaccept(Integer isaccept) {
        return wantedMapper.selectWantedByIsaccept(isaccept);
    }

    @Override
    public int updateWantedIsacceptBywantedrage(Integer wantedrage, Integer isaccept) {
        return wantedMapper.updateWantedIsacceptBywantedrage(wantedrage, isaccept);
    }

    @Override
    public List<Wanted> selectWantedByDepartmentIdAndworkName(String departmentId, String workName) {
        return wantedMapper.selectWantedByDepartmentIdAndworkName(departmentId, workName);
    }

    @Override
    public int userIdIsIllegal(String userId) {
        return wantedMapper.userIdIsIllegal(userId);
    }

    @Override
    public int workIdIsIllegal(String workId, String departmentId) {
        return wantedMapper.workIdIsIllegal(workId, departmentId);
    }

    @Override
    public int InsertNewDep(Department_emp department_emp) {
        return wantedMapper.InsertNewDep(department_emp);
    }

    @Override
    public int InsertNewEmp(Employee employee) {
        return wantedMapper.InsertNewEmp(employee);
    }

    @Override
    public int InsertNewUser(User user) {
        return wantedMapper.InsertNewUser(user);
    }

    @Override
    public int updateWantedDatrByWantedRage(String getDate, String wantedrage) {
        return wantedMapper.updateWantedDatrByWantedRage(getDate, wantedrage);
    }

    @Override
    public int InsertNewWanteds(Wanted wanted) {

        wanted.setWantedDate(new Date());
        return wantedMapper.InsertNewWanteds(wanted);
    }

    public int InsertNewEmps(Map<String,String>map)  {
        String str="abcdefghijklmnopqrstuvwxyz1234567890";    //可随机产生的字符
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for(int i=0;i<10;i++) {
            int number = random.nextInt(36);            //可随机产生字符的数量
            sb.append(str.charAt(number));
        }
            String userId = map.get("userId");
            String username =map.get("username");
            String departmentId = map.get("departmentId");
            String departmentName =map.get("departmentName");
            String workId = map.get("workId");
            String workName =map.get("workName");
            String idcard = map.get("idcard");
            String gender = map.get("gender");
            String politicaltype = map.get("politicaltype");
            String nationId = map.get("nationId");
            String address = map.get("address");
            String email = map.get("email");
            String phone =map.get("phone");
            String education =map.get("education");
            String educationsubject =map.get("educationsubject");
            String school = map.get("school");
            String baseSalary =map.get("baseSalary");
            String isaccept = map.get("isaccept");
            System.out.println(map.toString());
            String password = sb.toString();
        System.out.println(password);
        User user = new User();
        user.setUserId(map.get("userId"));
        user.setUsername(map.get("username"));
        user.setRole("user");
        user.setEmail(email);
        user.setPassword(password);
        System.out.println(user.toString());
        Employee employee = new Employee();

        employee.setUserId(userId);
        employee.setUserName(username);
        employee.setGender(gender);
        employee.setIdcard(idcard);
        employee.setSchool(school);
        employee.setEmail(email);
        employee.setEducation(education);
        employee.setEducationsubject(educationsubject);
        employee.setRole("user");
        employee.setPhone(phone);
        employee.setPoliticaltype(politicaltype);
        employee.setAddress(address);
        employee.setNationId(nationId);
        employee.setDepartmentId(Integer.valueOf(departmentId));
        employee.setDepartmentName(departmentName);
        employee.setWorkId(Integer.valueOf(workId));
        employee.setWorkName(workName);
        employee.setBaseSalary(Double.valueOf(baseSalary));
        System.out.println(employee.toString());
        Department_emp department_emp = new Department_emp();
        department_emp.setDepartmentId(Integer.valueOf(departmentId));
        department_emp.setDepartmentName(departmentName);
        department_emp.setUserId(userId);
        department_emp.setUsername(username);
        department_emp.setWorkId(Integer.valueOf(workId));
        department_emp.setWorkName(workName);
        department_emp.setRole("user");
        department_emp.setIstrain(0);
        department_emp.setIswork(1);
        System.out.println(department_emp.toString());
        String timeStr1= LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        //开始添加
        //首先先把状态改为录取
        wantedMapper.updateWantedDatrByWantedRage(timeStr1,map.get("wantedrage"));
        wantedMapper.updateWantedIsacceptBywantedrage(Integer.valueOf(map.get("wantedrage")),Integer.valueOf(isaccept));
        //
        wantedMapper.InsertNewUser(user);
        wantedMapper.InsertNewEmp(employee);
        wantedMapper.InsertNewDep(department_emp);















        try{

            int count=1;//默认发送一次
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage,true);
            while(count--!=0){
//                String codeNum = "";
//                int [] code = new int[3];
//                Random random = new Random();
//                //自动生成验证码
//                for (int i = 0; i < 6; i++) {
//                    int num = random.nextInt(10) + 48;
//                    int uppercase = random.nextInt(26) + 65;
//                    int lowercase = random.nextInt(26) + 97;
//                    code[0] = num;
//                    code[1] = uppercase;
//                    code[2] = lowercase;
//                    codeNum+=(char)code[random.nextInt(3)];
//                }
                //标题
                helper.setSubject("录取通知：");
                //内容
                helper.setText("您好！，欢迎来到贵公司。您的账号为："+"<h2>"+userId+".密码为"+password+"</h2>"+"千万不能告诉别人哦！",true);
                //邮件接收者
                helper.setTo(email);
                //邮件发送者，必须和配置文件里的一样，不然授权码匹配不上
                helper.setFrom("654763560@qq.com");
                mailSender.send(mimeMessage);
                System.out.println("邮件发送成功！");
            }


        }catch (MessagingException e)
        {
            e.printStackTrace();
        }

        return 1;
    }
}
