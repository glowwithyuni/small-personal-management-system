package org.pjx.spm.service;

import org.pjx.spm.mapper.Employee_SalaryMapper;
import org.pjx.spm.pojo.Employee;
import org.pjx.spm.pojo.Employee_Salary;
import org.pjx.spm.pojo.Employee_train;
import org.pjx.spm.pojo.Work_attendance;
import org.pjx.spm.util.interfacespecfiaction.RespBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.NumberFormat;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2022-03-23 20:32:49
 * @Description 员工薪资表
 * @Version 1.0
 */
@Service
public class Employee_SalaryServiceImpl implements  Employee_SalaryService{
    @Autowired
    private Employee_SalaryMapper employee_salaryMapper;
    @Autowired
    private Employee_trainServiceImpl employee_trainService;
    @Autowired
    private Work_attendanceServiceImpl work_attendanceService;
    @Autowired
    private EmployeeServiceImpl employeeService;
    @Override
    public List<Employee_Salary> SelectAllEmployeeSalaryBySelectedSalaryMonth() {
        return employee_salaryMapper.SelectAllEmployeeSalaryBySelectedSalaryMonth();
    }

    @Override
    public List<Employee_Salary> SelectAllEmployeeSalaryBySalaryMonthDescBysalaryMonth(String salaryMonth) {
        return employee_salaryMapper.SelectAllEmployeeSalaryBySalaryMonthDescBysalaryMonth(salaryMonth);
    }

    @Override
    public List<Employee_Salary> SelectAllDepEmployeeSalaryBySalaryMonthDescBysalaryMonth(String salaryMonth, String departmentId) {
        return employee_salaryMapper.SelectAllDepEmployeeSalaryBySalaryMonthDescBysalaryMonth(salaryMonth, departmentId);
    }

    @Override
    public List<Employee_Salary> SelectAllUserEmployeeSalaryBySalaryMonthDescBysalaryMonth(String salaryMonth, String userId) {
        return employee_salaryMapper.SelectAllUserEmployeeSalaryBySalaryMonthDescBysalaryMonth(salaryMonth, userId);
    }

    public List<Employee_Salary> UpdateThisMonthSalary(String salaryMonth)
    {
        int init = 0 ;
        String yearMonth = salaryMonth.substring(0,salaryMonth.length()-1);
        System.out.println(yearMonth);
        List<Employee_Salary> list =employee_salaryMapper.SelectAllEmployeeSalaryBySalaryMonthDescBysalaryMonth(salaryMonth);
        ListIterator<Employee_Salary> listIterator = list.listIterator();
        while (listIterator.hasNext())
        {
            init = 0;
            Employee_Salary employee_salary = listIterator.next();
            //万一月份更新，需要减去原先的部分再添加到年份
            Employee_Salary years = employee_salaryMapper.selectSalaryByUserIdAndSalary
                    (employee_salary.getUserId(),salaryMonth.substring(0,salaryMonth.length()-4));
            System.out.println("这几年是:"+years);
            if(years == null)
            {
                years = new Employee_Salary();
                    years.setUserId(employee_salary.getUserId());
                    years.setUsername(employee_salary.getUsername());
                    years.setRole(employee_salary.getRole());
                    years.setSalaryMonth(salaryMonth.substring(0,salaryMonth.length()-4));
                    employee_salaryMapper.InsertEmployeeSalaryInIt(years);
                    years.setIssearch(1);
                    employee_salaryMapper.updateIssearch(years);
                years = employee_salaryMapper.selectSalaryByUserIdAndSalary
                        (employee_salary.getUserId(),salaryMonth.substring(0,salaryMonth.length()-4));
                    System.out.println("新建的年份："+years.toString());
                    init = 1;
            }
            List<Employee_train> listTrain = employee_trainService.selectTrainByIdAndStartDate
                                    (employee_salary.getUserId(),employee_salary.getSalaryMonth()+"%");
            ListIterator<Employee_train>listIteratorTrain = listTrain.listIterator();
            System.out.println("要加载检查的月份："+employee_salary.toString());
            while(listIteratorTrain.hasNext())
            {

               Employee_train employee_train = listIteratorTrain.next();
               if(employee_train.getIsadd()==1)
               {
                   System.out.println("该培训记录已被读取");
                   continue;
               }
               //因为培训的更新，要把年度月份调整
                if(employee_salary.getIssearch()==1&&init == 0)
                {
                    System.out.println("continue拦不住？");
                    years.setBaseSalary(years.getBaseSalary()-employee_salary.getBaseSalary());
                    years.setTransportSalary(years.getTransportSalary()-employee_salary.getTransportSalary());
                    years.setRunAttendanceTimes(years.getRunAttendanceTimes()-employee_salary.getRunAttendanceTimes());
                    years.setLateAttendanceTimes(years.getLateAttendanceTimes()-employee_salary.getLateAttendanceTimes());
                    years.setAbsentAttendanceTimes(years.getAbsentAttendanceTimes()-employee_salary.getAbsentAttendanceTimes());
                    years.setRestAttendanceTimes(years.getRestAttendanceTimes()-employee_salary.getRestAttendanceTimes());
                    years.setRunAttendanceMoney(years.getRunAttendanceMoney()-employee_salary.getRunAttendanceMoney());
                    years.setLateAttendanceMoney(years.getLateAttendanceMoney()-employee_salary.getLateAttendanceMoney());
                    years.setAbsentAttendanceMoney(years.getAbsentAttendanceMoney()-employee_salary.getAbsentAttendanceMoney());
                    years.setRestAttendanceMoney(years.getRestAttendanceMoney()-employee_salary.getRestAttendanceMoney());
                    years.setWorkTimes(years.getWorkTimes()-employee_salary.getWorkTimes());
                    years.setWorkMoney(years.getWorkMoney()-employee_salary.getWorkMoney());
                    years.setOnTimes(years.getOnTimes()-employee_salary.getOnTimes());
                    years.setAllAttendMoney(years.getAllAttendMoney()-employee_salary.getAllAttendMoney());
                    years.setTaxMoney(years.getTaxMoney()-employee_salary.getTaxMoney());
                    years.setTotalMoney(years.getTotalMoney()-employee_salary.getTotalMoney());
                    employee_salary.setIssearch(0);
                }



               //正常添加
               System.out.println(employee_train.toString());
               employee_salary.setTransportSalary(employee_salary.getTransportSalary()+employee_train.getTransportSalary());
                employee_trainService.updatesetaddBytrainrage(employee_train.getTrainrage(),1);
            }
            List<Work_attendance>workAttendances =work_attendanceService.
                    selectAttendanceByIdAndCheckDate(employee_salary.getUserId(),employee_salary.getSalaryMonth()+"%");
            ListIterator<Work_attendance> listIteratorWorkAttendance = workAttendances.listIterator();
            while(listIteratorWorkAttendance.hasNext())
            {
                Work_attendance work_attendance = listIteratorWorkAttendance.next();
                if(work_attendance.getIssearch().equals(1))
                {

                    System.out.println("该考勤记录已被读取:"+work_attendance.toString());
                    continue;
                }
                //如果考勤记录状态为0，则跳过该考勤
                if(work_attendance.getChecktype().equals("0"))
                {
                    continue;
                }
                //考勤更新，需要更改月份薪资，影响年份，需删去该年份的月份的薪资并更新
                if(employee_salary.getIssearch()==1&&init == 0)
                {
                    System.out.println("continue拦不住？");
                    years.setBaseSalary(years.getBaseSalary()-employee_salary.getBaseSalary());
                    years.setTransportSalary(years.getTransportSalary()-employee_salary.getTransportSalary());
                    years.setRunAttendanceTimes(years.getRunAttendanceTimes()-employee_salary.getRunAttendanceTimes());
                    years.setLateAttendanceTimes(years.getLateAttendanceTimes()-employee_salary.getLateAttendanceTimes());
                    years.setAbsentAttendanceTimes(years.getAbsentAttendanceTimes()-employee_salary.getAbsentAttendanceTimes());
                    years.setRestAttendanceTimes(years.getRestAttendanceTimes()-employee_salary.getRestAttendanceTimes());
                    years.setRunAttendanceMoney(years.getRunAttendanceMoney()-employee_salary.getRunAttendanceMoney());
                    years.setLateAttendanceMoney(years.getLateAttendanceMoney()-employee_salary.getLateAttendanceMoney());
                    years.setAbsentAttendanceMoney(years.getAbsentAttendanceMoney()-employee_salary.getAbsentAttendanceMoney());
                    years.setRestAttendanceMoney(years.getRestAttendanceMoney()-employee_salary.getRestAttendanceMoney());
                    years.setWorkTimes(years.getWorkTimes()-employee_salary.getWorkTimes());
                    years.setWorkMoney(years.getWorkMoney()-employee_salary.getWorkMoney());
                    years.setOnTimes(years.getOnTimes()-employee_salary.getOnTimes());
                    years.setAllAttendMoney(years.getAllAttendMoney()-employee_salary.getAllAttendMoney());
                    years.setTaxMoney(years.getTaxMoney()-employee_salary.getTaxMoney());
                    years.setTotalMoney(years.getTotalMoney()-employee_salary.getTotalMoney());
                    employee_salary.setIssearch(0);
                }





                System.out.println("需要更新的考勤为："+work_attendance.toString());
                System.out.println("要初始化的emp："+employee_salary.toString());

                if(work_attendance.getChecktype().equals("1"))
                {
                    employee_salary.setOnTimes(employee_salary.getOnTimes()+1);
                }
                if(work_attendance.getChecktype().equals("2"))
                {
                    employee_salary.setRestAttendanceTimes(employee_salary.getRestAttendanceTimes()+1);
                }
                if(work_attendance.getChecktype().equals("3"))
                {
                    employee_salary.setLateAttendanceTimes(employee_salary.getLateAttendanceTimes()+1);
                }
                if(work_attendance.getChecktype().equals("4"))
                {
                    employee_salary.setAbsentAttendanceTimes(employee_salary.getAbsentAttendanceTimes()+1);
                }
                if(work_attendance.getChecktype().equals("5"))
                {
                    employee_salary.setRunAttendanceTimes(employee_salary.getRunAttendanceTimes()+1);
                }
                if(work_attendance.getChecktype().equals("6"))
                {
                    employee_salary.setOnTimes(employee_salary.getOnTimes()+1);
                    employee_salary.setWorkTimes(employee_salary.getWorkTimes()+1);
                }
                work_attendanceService.updateIssearchByAttendance(work_attendance.getAttendance(),1);
            }
            int wholeday = employee_salary.getAbsentAttendanceTimes()+employee_salary.getLateAttendanceTimes()
                    +employee_salary.getRestAttendanceTimes()+employee_salary.getRunAttendanceTimes()+
                    employee_salary.getOnTimes();
            //统计完目前的考勤和培训信息后，开始统计税收等
            //如果全勤，则分权限补给薪资~
            if(employee_salary.getRunAttendanceTimes()==0&&employee_salary.getAbsentAttendanceTimes()==0
               &&employee_salary.getLateAttendanceTimes()==0&&employee_salary.getRestAttendanceTimes()==0)
            {
                System.out.print("你全勤了");
                if(employee_salary.getRole().equals("user"))
                {
                    employee_salary.setAllAttendMoney(employee_salary.getBaseSalary()*0.03);
                }
                else if(employee_salary.getRole().equals("department"))
                {
                    employee_salary.setAllAttendMoney(employee_salary.getBaseSalary()*0.06);
                }
                System.out.println("全勤奖金："+employee_salary.getAllAttendMoney());
            }
            //考勤规则，如果是权限越高，奖惩越严重~
                //缺勤次数<3 从轻处罚
                if(employee_salary.getAbsentAttendanceTimes()<=3)
                {
                    if(employee_salary.getRole().equals("user"))
                    {
                        employee_salary.setAbsentAttendanceMoney
                                (employee_salary.getBaseSalary()*0.01*employee_salary.getAbsentAttendanceTimes());
                    }else if(employee_salary.getRole().equals("department"))
                    {
                        employee_salary.setAbsentAttendanceMoney
                                (employee_salary.getBaseSalary()*0.02*employee_salary.getAbsentAttendanceTimes());
                    }
                }
                //缺勤次数>3 所有累积从重处罚
                else {

                    if(employee_salary.getRole().equals("user"))
                    {
                        employee_salary.setAbsentAttendanceMoney
                                (employee_salary.getBaseSalary()*0.02*employee_salary.getAbsentAttendanceTimes());
                    }else if(employee_salary.getRole().equals("department"))
                    {
                        employee_salary.setAbsentAttendanceMoney
                                (employee_salary.getBaseSalary()*0.04*employee_salary.getAbsentAttendanceTimes());
                    }
                }

            //迟到次数<3 从轻处罚
            if(employee_salary.getLateAttendanceTimes()<=3)
            {
                if(employee_salary.getRole().equals("user"))
                {
                    employee_salary.setLateAttendanceMoney
                            (employee_salary.getBaseSalary()*0.005*employee_salary.getLateAttendanceTimes());
                }else if(employee_salary.getRole().equals("department"))
                {
                    employee_salary.setLateAttendanceMoney
                            (employee_salary.getBaseSalary()*0.01*employee_salary.getLateAttendanceTimes());
                }
            }
            //迟到次数>3 所有累积从重处罚
            else {
                if(employee_salary.getRole().equals("user"))
                {
                    employee_salary.setLateAttendanceMoney
                            (employee_salary.getBaseSalary()*0.01*employee_salary.getLateAttendanceTimes());
                }else if(employee_salary.getRole().equals("department"))
                {
                    employee_salary.setLateAttendanceMoney
                            (employee_salary.getBaseSalary()*0.02*employee_salary.getLateAttendanceTimes());
                }
            }
            //请假扣薪一视同仁~
            if(employee_salary.getRestAttendanceTimes()>0)
            {
                if(employee_salary.getRole().equals("user"))
                {
                    employee_salary.setRestAttendanceMoney
                            ((employee_salary.getBaseSalary()/wholeday)*employee_salary.getRestAttendanceTimes());
                }else if(employee_salary.getRole().equals("department"))
                {
                    employee_salary.setRestAttendanceMoney
                            ((employee_salary.getBaseSalary()/wholeday)*employee_salary.getRestAttendanceTimes());
                }
            }
            //早退，如果小于3次，则从轻处罚
            if(employee_salary.getRunAttendanceTimes()<3)
            {
                if(employee_salary.getRole().equals("user"))
                {
                    employee_salary.setRunAttendanceMoney(employee_salary.getBaseSalary()*0.005*employee_salary.getRunAttendanceTimes());
                }
                else if(employee_salary.getRole().equals("department"))
                {
                    employee_salary.setRunAttendanceMoney(employee_salary.getBaseSalary()*0.01*employee_salary.getRunAttendanceTimes());
                }
            }
            else{
                if(employee_salary.getRole().equals("user"))
                {
                    employee_salary.setRunAttendanceMoney(employee_salary.getBaseSalary()*0.01*employee_salary.getRunAttendanceTimes());

                }else if(employee_salary.getRole().equals("department"))
                {
                    employee_salary.setRunAttendanceMoney(employee_salary.getBaseSalary()*0.02*employee_salary.getRunAttendanceTimes());
                }

            }
            //根据权限分配加班费~
            if(employee_salary.getRole().equals("user"))
            {
                    employee_salary.setWorkMoney(employee_salary.getBaseSalary()*0.001*employee_salary.getWorkTimes());
            }else if(employee_salary.getRole().equals("department"))
            {
                employee_salary.setWorkMoney(employee_salary.getBaseSalary()*0.002*employee_salary.getWorkTimes());
            }

            //统计薪资税收~············
            Double noTaxMoney =
                    employee_salary.getBaseSalary()+ employee_salary.getAllAttendMoney()-
                    employee_salary.getRunAttendanceMoney()-employee_salary.getAbsentAttendanceMoney()
                    -employee_salary.getRestAttendanceMoney()-employee_salary.getLateAttendanceMoney()
                    +employee_salary.getWorkMoney();
            Double TaxMoney = 0.00;
            if(noTaxMoney<=5000.0)
            {
                employee_salary.setTaxMoney(0.0);
                TaxMoney =noTaxMoney;
            }
            else if(noTaxMoney>5000.00&&noTaxMoney<=8000.00){
                employee_salary.setTaxMoney(noTaxMoney*0.03);
                TaxMoney = (noTaxMoney*(1-0.03));
            }
            else if(noTaxMoney>8000.00&&noTaxMoney<=12000.00){
                employee_salary.setTaxMoney(noTaxMoney*0.05);
                TaxMoney = (noTaxMoney*(1-0.05));
            }
            else if(noTaxMoney>12000.00&&noTaxMoney<=18000.00){
                employee_salary.setTaxMoney(noTaxMoney*0.09);
                TaxMoney = (noTaxMoney*(1-0.09));
            }
            else if(noTaxMoney>18000.00&&noTaxMoney<=24000.00){
                employee_salary.setTaxMoney(noTaxMoney*0.15);
                TaxMoney = (noTaxMoney*(1-0.15));
            }
            else if(noTaxMoney>24000.00){
                employee_salary.setTaxMoney(noTaxMoney*0.20);
                TaxMoney = (noTaxMoney*(1-0.2));
            }

            employee_salary.setTotalMoney(TaxMoney);
            System.out.println(employee_salary.toString());

            employee_salaryMapper.UpdateEmpSalaryBysalaryOrder(employee_salary);
            employee_salaryMapper.updateyearsalary(years);
        }
        list =employee_salaryMapper.SelectAllEmployeeSalaryBySalaryMonthDescBysalaryMonth(salaryMonth);
        return list;
    }
    public List<Employee_Salary> UpdateYearEmpSalary(String SalaryYear)
    {
        System.out.println(SalaryYear);
        List<Map<Object,Object>> list = employeeService.selectYearSalary(SalaryYear+"%");
        ListIterator<Map<Object,Object>>listIterator = list.listIterator();
        Employee_Salary oldempsal = new Employee_Salary();
        String userId;
        String username;
        String role;
        while(listIterator.hasNext())
        {

            Map<Object,Object>map = listIterator.next();
            userId = map.get("userId").toString();
            username = map.get("username").toString();
            role = map.get("role").toString();
            oldempsal.setUserId(userId);
            oldempsal.setUsername(username);
            oldempsal.setRole(role);
            oldempsal.setSalaryMonth(SalaryYear);
            System.out.println(oldempsal.toString());
            if(employee_salaryMapper.selectIsExistYearSalary(userId,SalaryYear)==0)
            {
                employee_salaryMapper.InsertEmployeeSalaryInIt(oldempsal);
                oldempsal.setIssearch(1);
                employee_salaryMapper.updateIssearch(oldempsal);
            }
            oldempsal.setSalaryMonth(SalaryYear+"-%");
            List<Employee_Salary> employee_salaryList = employee_salaryMapper.selectAllMonthByuserIdAndSalaryMonth(oldempsal);
            ListIterator<Employee_Salary> employee_salaryListIterator = employee_salaryList.listIterator();
            System.out.println(employee_salaryList.size());
            oldempsal = employee_salaryMapper.selectSalaryByUserIdAndSalary(userId,SalaryYear);
            System.out.println("oldempsal:"+oldempsal.toString());
            while(employee_salaryListIterator.hasNext())
            {

                Employee_Salary newempsal = employee_salaryListIterator.next();
                if(newempsal.getIssearch()==0)
                {
                    System.out.println(userId+":"+newempsal.toString());
                    oldempsal.setBaseSalary(oldempsal.getBaseSalary()+newempsal.getBaseSalary());
                    oldempsal.setRunAttendanceTimes(oldempsal.getRunAttendanceTimes()+newempsal.getRunAttendanceTimes());
                    oldempsal.setLateAttendanceTimes(oldempsal.getLateAttendanceTimes()+newempsal.getLateAttendanceTimes());
                    oldempsal.setAbsentAttendanceTimes(oldempsal.getAbsentAttendanceTimes()+newempsal.getAbsentAttendanceTimes());
                    oldempsal.setRestAttendanceTimes(oldempsal.getRestAttendanceTimes()+newempsal.getRestAttendanceTimes());
                    oldempsal.setRunAttendanceMoney(oldempsal.getRunAttendanceMoney()+newempsal.getRunAttendanceMoney());
                    oldempsal.setLateAttendanceMoney(oldempsal.getLateAttendanceMoney()+newempsal.getLateAttendanceMoney());
                    oldempsal.setAbsentAttendanceMoney(oldempsal.getAbsentAttendanceMoney()+newempsal.getAbsentAttendanceMoney());
                    oldempsal.setRestAttendanceMoney(oldempsal.getRestAttendanceMoney()+newempsal.getRestAttendanceMoney());
                    oldempsal.setWorkTimes(oldempsal.getWorkTimes()+newempsal.getWorkTimes());
                    oldempsal.setWorkMoney(oldempsal.getWorkMoney()+newempsal.getWorkMoney());
                    oldempsal.setOnTimes(oldempsal.getOnTimes()+newempsal.getOnTimes());
                    oldempsal.setAllAttendMoney(oldempsal.getAllAttendMoney()+newempsal.getAllAttendMoney());
                    oldempsal.setTaxMoney(oldempsal.getTaxMoney()+newempsal.getTaxMoney());
                    oldempsal.setTransportSalary(oldempsal.getTransportSalary()+newempsal.getTransportSalary());
                    oldempsal.setTotalMoney(oldempsal.getTotalMoney()+newempsal.getTotalMoney());
                    //只有上够12个月的才能加年终奖~
                    if(employee_salaryList.size() == 12&&
                            (oldempsal.getRunAttendanceTimes()==0&&oldempsal.getAbsentAttendanceTimes()==0
                                    &&oldempsal.getLateAttendanceTimes()==0&&oldempsal.getRestAttendanceTimes()==0))
                    {
                        //如果全勤，则分权限补给薪资~
                        System.out.print("你全勤了");
                        if(oldempsal.getRole().equals("user"))
                        {
                            oldempsal.setAllAttendMoney(oldempsal.getAllAttendMoney()*1.2);
                        }
                        else if(oldempsal.getRole().equals("department"))
                        {
                            oldempsal.setAllAttendMoney(oldempsal.getAllAttendMoney()*2.4);
                        }
                        System.out.print("全勤奖金："+oldempsal.getAllAttendMoney());
                    }
                    System.out.println("更新后的年度薪资"+oldempsal.toString());


                    newempsal.setIssearch(1);
                    employee_salaryMapper.updateyearsalary(oldempsal);
                    employee_salaryMapper.updateIssearch(newempsal);

                }


            }

        }
        return employee_salaryMapper.selectSalaryBySalary(SalaryYear);
    }


    public List<Employee_Salary> UpdateThisMonthDepSalary(String salaryMonth,String departmentId)
    {
        int init = 0;
        String yearMonth = salaryMonth.substring(0,salaryMonth.length()-1);
        System.out.println(yearMonth);
        List<Employee_Salary> list =employee_salaryMapper.SelectAllDepEmployeeSalaryBySalaryMonthDescBysalaryMonth(salaryMonth,departmentId);
        ListIterator<Employee_Salary> listIterator = list.listIterator();
        while (listIterator.hasNext())
        {
            init = 0;
            Employee_Salary employee_salary = listIterator.next();
            //万一月份更新，需要减去原先的部分再添加到年份
            Employee_Salary years = employee_salaryMapper.selectSalaryByUserIdAndSalary
                    (employee_salary.getUserId(),salaryMonth.substring(0,salaryMonth.length()-4));

            if(years == null)
            {
                years = new Employee_Salary();
                years.setUserId(employee_salary.getUserId());
                years.setUsername(employee_salary.getUsername());
                years.setRole(employee_salary.getRole());
                years.setSalaryMonth(salaryMonth.substring(0,salaryMonth.length()-4));
                employee_salaryMapper.InsertEmployeeSalaryInIt(years);
                years.setIssearch(1);
                employee_salaryMapper.updateIssearch(years);
                years = employee_salaryMapper.selectSalaryByUserIdAndSalary
                        (employee_salary.getUserId(),salaryMonth.substring(0,salaryMonth.length()-4));
                System.out.println("新建的年份："+years.toString());
                init = 1;
            }
            List<Employee_train> listTrain = employee_trainService.selectTrainByIdAndStartDate
                    (employee_salary.getUserId(),employee_salary.getSalaryMonth()+"%");
            ListIterator<Employee_train>listIteratorTrain = listTrain.listIterator();
            System.out.println("要加载检查的月份："+employee_salary.toString());
            while(listIteratorTrain.hasNext())
            {

                Employee_train employee_train = listIteratorTrain.next();
                if(employee_train.getIsadd()==1)
                {
                    System.out.println("该培训记录已被读取");
                    continue;
                }
                //因为培训的更新，要把年度月份调整
                if(employee_salary.getIssearch()==1&&init == 0)
                {
                    System.out.println("continue拦不住？");
                    years.setBaseSalary(years.getBaseSalary()-employee_salary.getBaseSalary());
                    years.setTransportSalary(years.getTransportSalary()-employee_salary.getTransportSalary());
                    years.setRunAttendanceTimes(years.getRunAttendanceTimes()-employee_salary.getRunAttendanceTimes());
                    years.setLateAttendanceTimes(years.getLateAttendanceTimes()-employee_salary.getLateAttendanceTimes());
                    years.setAbsentAttendanceTimes(years.getAbsentAttendanceTimes()-employee_salary.getAbsentAttendanceTimes());
                    years.setRestAttendanceTimes(years.getRestAttendanceTimes()-employee_salary.getRestAttendanceTimes());
                    years.setRunAttendanceMoney(years.getRunAttendanceMoney()-employee_salary.getRunAttendanceMoney());
                    years.setLateAttendanceMoney(years.getLateAttendanceMoney()-employee_salary.getLateAttendanceMoney());
                    years.setAbsentAttendanceMoney(years.getAbsentAttendanceMoney()-employee_salary.getAbsentAttendanceMoney());
                    years.setRestAttendanceMoney(years.getRestAttendanceMoney()-employee_salary.getRestAttendanceMoney());
                    years.setWorkTimes(years.getWorkTimes()-employee_salary.getWorkTimes());
                    years.setWorkMoney(years.getWorkMoney()-employee_salary.getWorkMoney());
                    years.setOnTimes(years.getOnTimes()-employee_salary.getOnTimes());
                    years.setAllAttendMoney(years.getAllAttendMoney()-employee_salary.getAllAttendMoney());
                    years.setTaxMoney(years.getTaxMoney()-employee_salary.getTaxMoney());
                    years.setTotalMoney(years.getTotalMoney()-employee_salary.getTotalMoney());
                    employee_salary.setIssearch(0);
                }



                //正常添加
                System.out.println(employee_train.toString());
                employee_salary.setTransportSalary(employee_salary.getTransportSalary()+employee_train.getTransportSalary());
                employee_trainService.updatesetaddBytrainrage(employee_train.getTrainrage(),1);
            }
            List<Work_attendance>workAttendances =work_attendanceService.
                    selectAttendanceByIdAndCheckDate(employee_salary.getUserId(),employee_salary.getSalaryMonth()+"%");
            ListIterator<Work_attendance> listIteratorWorkAttendance = workAttendances.listIterator();
            while(listIteratorWorkAttendance.hasNext())
            {
                Work_attendance work_attendance = listIteratorWorkAttendance.next();
                if(work_attendance.getIssearch().equals(1))
                {

                    System.out.println("该考勤记录已被读取:"+work_attendance.toString());
                    continue;
                }
                //如果考勤记录状态为0，则跳过该考勤
                if(work_attendance.getChecktype().equals("0"))
                {
                    continue;
                }
                //考勤更新，需要更改月份薪资，影响年份，需删去该年份的月份的薪资并更新
                if(employee_salary.getIssearch()==1&&init == 0)
                {
                    System.out.println("continue拦不住？");
                    years.setBaseSalary(years.getBaseSalary()-employee_salary.getBaseSalary());
                    years.setTransportSalary(years.getTransportSalary()-employee_salary.getTransportSalary());
                    years.setRunAttendanceTimes(years.getRunAttendanceTimes()-employee_salary.getRunAttendanceTimes());
                    years.setLateAttendanceTimes(years.getLateAttendanceTimes()-employee_salary.getLateAttendanceTimes());
                    years.setAbsentAttendanceTimes(years.getAbsentAttendanceTimes()-employee_salary.getAbsentAttendanceTimes());
                    years.setRestAttendanceTimes(years.getRestAttendanceTimes()-employee_salary.getRestAttendanceTimes());
                    years.setRunAttendanceMoney(years.getRunAttendanceMoney()-employee_salary.getRunAttendanceMoney());
                    years.setLateAttendanceMoney(years.getLateAttendanceMoney()-employee_salary.getLateAttendanceMoney());
                    years.setAbsentAttendanceMoney(years.getAbsentAttendanceMoney()-employee_salary.getAbsentAttendanceMoney());
                    years.setRestAttendanceMoney(years.getRestAttendanceMoney()-employee_salary.getRestAttendanceMoney());
                    years.setWorkTimes(years.getWorkTimes()-employee_salary.getWorkTimes());
                    years.setWorkMoney(years.getWorkMoney()-employee_salary.getWorkMoney());
                    years.setOnTimes(years.getOnTimes()-employee_salary.getOnTimes());
                    years.setAllAttendMoney(years.getAllAttendMoney()-employee_salary.getAllAttendMoney());
                    years.setTaxMoney(years.getTaxMoney()-employee_salary.getTaxMoney());
                    years.setTotalMoney(years.getTotalMoney()-employee_salary.getTotalMoney());
                    employee_salary.setIssearch(0);
                }





                System.out.println(work_attendance.toString());
                if(work_attendance.getChecktype().equals("1"))
                {
                    employee_salary.setOnTimes(employee_salary.getOnTimes()+1);
                }
                if(work_attendance.getChecktype().equals("2"))
                {
                    employee_salary.setRestAttendanceTimes(employee_salary.getRestAttendanceTimes()+1);
                }
                if(work_attendance.getChecktype().equals("3"))
                {
                    employee_salary.setLateAttendanceTimes(employee_salary.getLateAttendanceTimes()+1);
                }
                if(work_attendance.getChecktype().equals("4"))
                {
                    employee_salary.setAbsentAttendanceTimes(employee_salary.getAbsentAttendanceTimes()+1);
                }
                if(work_attendance.getChecktype().equals("5"))
                {
                    employee_salary.setRunAttendanceTimes(employee_salary.getRunAttendanceTimes()+1);
                }
                if(work_attendance.getChecktype().equals("6"))
                {
                    employee_salary.setOnTimes(employee_salary.getOnTimes()+1);
                    employee_salary.setWorkTimes(employee_salary.getWorkTimes()+1);
                }
                work_attendanceService.updateIssearchByAttendance(work_attendance.getAttendance(),1);
            }
            int wholeday = employee_salary.getAbsentAttendanceTimes()+employee_salary.getLateAttendanceTimes()
                    +employee_salary.getRestAttendanceTimes()+employee_salary.getRunAttendanceTimes()+
                    employee_salary.getOnTimes();
            //统计完目前的考勤和培训信息后，开始统计税收等
            //如果全勤，则分权限补给薪资~
            if(employee_salary.getRunAttendanceTimes()==0&&employee_salary.getAbsentAttendanceTimes()==0
                    &&employee_salary.getLateAttendanceTimes()==0&&employee_salary.getRestAttendanceTimes()==0)
            {
                System.out.print("你全勤了");
                if(employee_salary.getRole().equals("user"))
                {
                    employee_salary.setAllAttendMoney(employee_salary.getBaseSalary()*0.03);
                }
                else if(employee_salary.getRole().equals("department"))
                {
                    employee_salary.setAllAttendMoney(employee_salary.getBaseSalary()*0.06);
                }
                System.out.println("全勤奖金："+employee_salary.getAllAttendMoney());
            }
            //考勤规则，如果是权限越高，奖惩越严重~
            //缺勤次数<3 从轻处罚
            if(employee_salary.getAbsentAttendanceTimes()<=3)
            {
                if(employee_salary.getRole().equals("user"))
                {
                    employee_salary.setAbsentAttendanceMoney
                            (employee_salary.getBaseSalary()*0.01*employee_salary.getAbsentAttendanceTimes());
                }else if(employee_salary.getRole().equals("department"))
                {
                    employee_salary.setAbsentAttendanceMoney
                            (employee_salary.getBaseSalary()*0.02*employee_salary.getAbsentAttendanceTimes());
                }
            }
            //缺勤次数>3 所有累积从重处罚
            else {

                if(employee_salary.getRole().equals("user"))
                {
                    employee_salary.setAbsentAttendanceMoney
                            (employee_salary.getBaseSalary()*0.02*employee_salary.getAbsentAttendanceTimes());
                }else if(employee_salary.getRole().equals("department"))
                {
                    employee_salary.setAbsentAttendanceMoney
                            (employee_salary.getBaseSalary()*0.04*employee_salary.getAbsentAttendanceTimes());
                }
            }

            //迟到次数<3 从轻处罚
            if(employee_salary.getLateAttendanceTimes()<=3)
            {
                if(employee_salary.getRole().equals("user"))
                {
                    employee_salary.setLateAttendanceMoney
                            (employee_salary.getBaseSalary()*0.005*employee_salary.getLateAttendanceTimes());
                }else if(employee_salary.getRole().equals("department"))
                {
                    employee_salary.setLateAttendanceMoney
                            (employee_salary.getBaseSalary()*0.01*employee_salary.getLateAttendanceTimes());
                }
            }
            //迟到次数>3 所有累积从重处罚
            else {
                if(employee_salary.getRole().equals("user"))
                {
                    employee_salary.setLateAttendanceMoney
                            (employee_salary.getBaseSalary()*0.01*employee_salary.getLateAttendanceTimes());
                }else if(employee_salary.getRole().equals("department"))
                {
                    employee_salary.setLateAttendanceMoney
                            (employee_salary.getBaseSalary()*0.02*employee_salary.getLateAttendanceTimes());
                }
            }
            //请假扣薪一视同仁~
            if(employee_salary.getRestAttendanceTimes()>0)
            {
                if(employee_salary.getRole().equals("user"))
                {
                    employee_salary.setRestAttendanceMoney
                            ((employee_salary.getBaseSalary()/wholeday)*employee_salary.getRestAttendanceTimes());
                }else if(employee_salary.getRole().equals("department"))
                {
                    employee_salary.setRestAttendanceMoney
                            ((employee_salary.getBaseSalary()/wholeday)*employee_salary.getRestAttendanceTimes());
                }
            }
            //早退，如果小于3次，则从轻处罚
            if(employee_salary.getRunAttendanceTimes()<3)
            {
                if(employee_salary.getRole().equals("user"))
                {
                    employee_salary.setRunAttendanceMoney(employee_salary.getBaseSalary()*0.005*employee_salary.getRunAttendanceTimes());
                }
                else if(employee_salary.getRole().equals("department"))
                {
                    employee_salary.setRunAttendanceMoney(employee_salary.getBaseSalary()*0.01*employee_salary.getRunAttendanceTimes());
                }
            }
            else{
                if(employee_salary.getRole().equals("user"))
                {
                    employee_salary.setRunAttendanceMoney(employee_salary.getBaseSalary()*0.01*employee_salary.getRunAttendanceTimes());

                }else if(employee_salary.getRole().equals("department"))
                {
                    employee_salary.setRunAttendanceMoney(employee_salary.getBaseSalary()*0.02*employee_salary.getRunAttendanceTimes());
                }

            }
            //根据权限分配加班费~
            if(employee_salary.getRole().equals("user"))
            {
                employee_salary.setWorkMoney(employee_salary.getBaseSalary()*0.001*employee_salary.getWorkTimes());
            }else if(employee_salary.getRole().equals("department"))
            {
                employee_salary.setWorkMoney(employee_salary.getBaseSalary()*0.002*employee_salary.getWorkTimes());
            }

            //统计薪资税收~············
            Double noTaxMoney =
                    employee_salary.getBaseSalary()+ employee_salary.getAllAttendMoney()-
                            employee_salary.getRunAttendanceMoney()-employee_salary.getAbsentAttendanceMoney()
                            -employee_salary.getRestAttendanceMoney()-employee_salary.getLateAttendanceMoney()
                            +employee_salary.getWorkMoney();
            Double TaxMoney = 0.00;
            if(noTaxMoney<=5000.0)
            {
                employee_salary.setTaxMoney(0.0);
                TaxMoney =noTaxMoney;
            }
            else if(noTaxMoney>5000.00&&noTaxMoney<=8000.00){
                employee_salary.setTaxMoney(noTaxMoney*0.03);
                TaxMoney = (noTaxMoney*(1-0.03));
            }
            else if(noTaxMoney>8000.00&&noTaxMoney<=12000.00){
                employee_salary.setTaxMoney(noTaxMoney*0.05);
                TaxMoney = (noTaxMoney*(1-0.05));
            }
            else if(noTaxMoney>12000.00&&noTaxMoney<=18000.00){
                employee_salary.setTaxMoney(noTaxMoney*0.09);
                TaxMoney = (noTaxMoney*(1-0.09));
            }
            else if(noTaxMoney>18000.00&&noTaxMoney<=24000.00){
                employee_salary.setTaxMoney(noTaxMoney*0.15);
                TaxMoney = (noTaxMoney*(1-0.15));
            }
            else if(noTaxMoney>24000.00){
                employee_salary.setTaxMoney(noTaxMoney*0.20);
                TaxMoney = (noTaxMoney*(1-0.2));
            }

            employee_salary.setTotalMoney(TaxMoney);
            System.out.println(employee_salary.toString());

            employee_salaryMapper.UpdateEmpSalaryBysalaryOrder(employee_salary);
            employee_salaryMapper.updateyearsalary(years);
        }
        list =employee_salaryMapper.SelectAllDepEmployeeSalaryBySalaryMonthDescBysalaryMonth(salaryMonth,departmentId);
        return list;
    }
    public List<Employee_Salary> UpdateThisMonthUserSalary(String salaryMonth,String userIds)
    {
        int init = 0;
        String yearMonth = salaryMonth.substring(0,salaryMonth.length()-1);
        System.out.println(yearMonth);
        List<Employee_Salary> list =employee_salaryMapper.SelectAllUserEmployeeSalaryBySalaryMonthDescBysalaryMonth(salaryMonth,userIds);
        ListIterator<Employee_Salary> listIterator = list.listIterator();
        while (listIterator.hasNext())
        {
            init = 0;
            Employee_Salary employee_salary = listIterator.next();
            //万一月份更新，需要减去原先的部分再添加到年份
            Employee_Salary years = employee_salaryMapper.selectSalaryByUserIdAndSalary
                    (employee_salary.getUserId(),salaryMonth.substring(0,salaryMonth.length()-4));

            if(years == null)
            {
                years = new Employee_Salary();
                years.setUserId(employee_salary.getUserId());
                years.setUsername(employee_salary.getUsername());
                years.setRole(employee_salary.getRole());
                years.setSalaryMonth(salaryMonth.substring(0,salaryMonth.length()-4));
                employee_salaryMapper.InsertEmployeeSalaryInIt(years);
                years.setIssearch(1);
                employee_salaryMapper.updateIssearch(years);
                years = employee_salaryMapper.selectSalaryByUserIdAndSalary
                        (employee_salary.getUserId(),salaryMonth.substring(0,salaryMonth.length()-4));
                System.out.println("新建的年份："+years.toString());
                init = 1;
            }
            List<Employee_train> listTrain = employee_trainService.selectTrainByIdAndStartDate
                    (employee_salary.getUserId(),employee_salary.getSalaryMonth()+"%");
            ListIterator<Employee_train>listIteratorTrain = listTrain.listIterator();
            System.out.println("要加载检查的月份："+employee_salary.toString());
            while(listIteratorTrain.hasNext())
            {

                Employee_train employee_train = listIteratorTrain.next();
                if(employee_train.getIsadd()==1)
                {
                    System.out.println("该培训记录已被读取");
                    continue;
                }
                //因为培训的更新，要把年度月份调整
                if(employee_salary.getIssearch()==1&&init == 0)
                {
                    System.out.println("continue拦不住？");
                    years.setBaseSalary(years.getBaseSalary()-employee_salary.getBaseSalary());
                    years.setTransportSalary(years.getTransportSalary()-employee_salary.getTransportSalary());
                    years.setRunAttendanceTimes(years.getRunAttendanceTimes()-employee_salary.getRunAttendanceTimes());
                    years.setLateAttendanceTimes(years.getLateAttendanceTimes()-employee_salary.getLateAttendanceTimes());
                    years.setAbsentAttendanceTimes(years.getAbsentAttendanceTimes()-employee_salary.getAbsentAttendanceTimes());
                    years.setRestAttendanceTimes(years.getRestAttendanceTimes()-employee_salary.getRestAttendanceTimes());
                    years.setRunAttendanceMoney(years.getRunAttendanceMoney()-employee_salary.getRunAttendanceMoney());
                    years.setLateAttendanceMoney(years.getLateAttendanceMoney()-employee_salary.getLateAttendanceMoney());
                    years.setAbsentAttendanceMoney(years.getAbsentAttendanceMoney()-employee_salary.getAbsentAttendanceMoney());
                    years.setRestAttendanceMoney(years.getRestAttendanceMoney()-employee_salary.getRestAttendanceMoney());
                    years.setWorkTimes(years.getWorkTimes()-employee_salary.getWorkTimes());
                    years.setWorkMoney(years.getWorkMoney()-employee_salary.getWorkMoney());
                    years.setOnTimes(years.getOnTimes()-employee_salary.getOnTimes());
                    years.setAllAttendMoney(years.getAllAttendMoney()-employee_salary.getAllAttendMoney());
                    years.setTaxMoney(years.getTaxMoney()-employee_salary.getTaxMoney());
                    years.setTotalMoney(years.getTotalMoney()-employee_salary.getTotalMoney());
                    employee_salary.setIssearch(0);
                }



                //正常添加
                System.out.println(employee_train.toString());
                employee_salary.setTransportSalary(employee_salary.getTransportSalary()+employee_train.getTransportSalary());
                employee_trainService.updatesetaddBytrainrage(employee_train.getTrainrage(),1);
            }
            List<Work_attendance>workAttendances =work_attendanceService.
                    selectAttendanceByIdAndCheckDate(employee_salary.getUserId(),employee_salary.getSalaryMonth()+"%");
            ListIterator<Work_attendance> listIteratorWorkAttendance = workAttendances.listIterator();
            while(listIteratorWorkAttendance.hasNext())
            {
                Work_attendance work_attendance = listIteratorWorkAttendance.next();
                if(work_attendance.getIssearch().equals(1))
                {

                    System.out.println("该考勤记录已被读取:"+work_attendance.toString());
                    continue;
                }
                //如果考勤记录状态为0，则跳过该考勤
                if(work_attendance.getChecktype().equals("0"))
                {
                    continue;
                }
                //考勤更新，需要更改月份薪资，影响年份，需删去该年份的月份的薪资并更新
                if(employee_salary.getIssearch()==1&&init == 0)
                {
                    System.out.println("continue拦不住？");
                    years.setBaseSalary(years.getBaseSalary()-employee_salary.getBaseSalary());
                    years.setTransportSalary(years.getTransportSalary()-employee_salary.getTransportSalary());
                    years.setRunAttendanceTimes(years.getRunAttendanceTimes()-employee_salary.getRunAttendanceTimes());
                    years.setLateAttendanceTimes(years.getLateAttendanceTimes()-employee_salary.getLateAttendanceTimes());
                    years.setAbsentAttendanceTimes(years.getAbsentAttendanceTimes()-employee_salary.getAbsentAttendanceTimes());
                    years.setRestAttendanceTimes(years.getRestAttendanceTimes()-employee_salary.getRestAttendanceTimes());
                    years.setRunAttendanceMoney(years.getRunAttendanceMoney()-employee_salary.getRunAttendanceMoney());
                    years.setLateAttendanceMoney(years.getLateAttendanceMoney()-employee_salary.getLateAttendanceMoney());
                    years.setAbsentAttendanceMoney(years.getAbsentAttendanceMoney()-employee_salary.getAbsentAttendanceMoney());
                    years.setRestAttendanceMoney(years.getRestAttendanceMoney()-employee_salary.getRestAttendanceMoney());
                    years.setWorkTimes(years.getWorkTimes()-employee_salary.getWorkTimes());
                    years.setWorkMoney(years.getWorkMoney()-employee_salary.getWorkMoney());
                    years.setOnTimes(years.getOnTimes()-employee_salary.getOnTimes());
                    years.setAllAttendMoney(years.getAllAttendMoney()-employee_salary.getAllAttendMoney());
                    years.setTaxMoney(years.getTaxMoney()-employee_salary.getTaxMoney());
                    years.setTotalMoney(years.getTotalMoney()-employee_salary.getTotalMoney());
                    employee_salary.setIssearch(0);
                }





                System.out.println(work_attendance.toString());
                if(work_attendance.getChecktype().equals("1"))
                {
                    employee_salary.setOnTimes(employee_salary.getOnTimes()+1);
                }
                if(work_attendance.getChecktype().equals("2"))
                {
                    employee_salary.setRestAttendanceTimes(employee_salary.getRestAttendanceTimes()+1);
                }
                if(work_attendance.getChecktype().equals("3"))
                {
                    employee_salary.setLateAttendanceTimes(employee_salary.getLateAttendanceTimes()+1);
                }
                if(work_attendance.getChecktype().equals("4"))
                {
                    employee_salary.setAbsentAttendanceTimes(employee_salary.getAbsentAttendanceTimes()+1);
                }
                if(work_attendance.getChecktype().equals("5"))
                {
                    employee_salary.setRunAttendanceTimes(employee_salary.getRunAttendanceTimes()+1);
                }
                if(work_attendance.getChecktype().equals("6"))
                {
                    employee_salary.setOnTimes(employee_salary.getOnTimes()+1);
                    employee_salary.setWorkTimes(employee_salary.getWorkTimes()+1);
                }
                work_attendanceService.updateIssearchByAttendance(work_attendance.getAttendance(),1);
            }
            int wholeday = employee_salary.getAbsentAttendanceTimes()+employee_salary.getLateAttendanceTimes()
                    +employee_salary.getRestAttendanceTimes()+employee_salary.getRunAttendanceTimes()+
                    employee_salary.getOnTimes();
            //统计完目前的考勤和培训信息后，开始统计税收等
            //如果全勤，则分权限补给薪资~
            if(employee_salary.getRunAttendanceTimes()==0&&employee_salary.getAbsentAttendanceTimes()==0
                    &&employee_salary.getLateAttendanceTimes()==0&&employee_salary.getRestAttendanceTimes()==0)
            {
                System.out.print("你全勤了");
                if(employee_salary.getRole().equals("user"))
                {
                    employee_salary.setAllAttendMoney(employee_salary.getBaseSalary()*0.03);
                }
                else if(employee_salary.getRole().equals("department"))
                {
                    employee_salary.setAllAttendMoney(employee_salary.getBaseSalary()*0.06);
                }
                System.out.println("全勤奖金："+employee_salary.getAllAttendMoney());
            }
            //考勤规则，如果是权限越高，奖惩越严重~
            //缺勤次数<3 从轻处罚
            if(employee_salary.getAbsentAttendanceTimes()<=3)
            {
                if(employee_salary.getRole().equals("user"))
                {
                    employee_salary.setAbsentAttendanceMoney
                            (employee_salary.getBaseSalary()*0.01*employee_salary.getAbsentAttendanceTimes());
                }else if(employee_salary.getRole().equals("department"))
                {
                    employee_salary.setAbsentAttendanceMoney
                            (employee_salary.getBaseSalary()*0.02*employee_salary.getAbsentAttendanceTimes());
                }
            }
            //缺勤次数>3 所有累积从重处罚
            else {

                if(employee_salary.getRole().equals("user"))
                {
                    employee_salary.setAbsentAttendanceMoney
                            (employee_salary.getBaseSalary()*0.02*employee_salary.getAbsentAttendanceTimes());
                }else if(employee_salary.getRole().equals("department"))
                {
                    employee_salary.setAbsentAttendanceMoney
                            (employee_salary.getBaseSalary()*0.04*employee_salary.getAbsentAttendanceTimes());
                }
            }

            //迟到次数<3 从轻处罚
            if(employee_salary.getLateAttendanceTimes()<=3)
            {
                if(employee_salary.getRole().equals("user"))
                {
                    employee_salary.setLateAttendanceMoney
                            (employee_salary.getBaseSalary()*0.005*employee_salary.getLateAttendanceTimes());
                }else if(employee_salary.getRole().equals("department"))
                {
                    employee_salary.setLateAttendanceMoney
                            (employee_salary.getBaseSalary()*0.01*employee_salary.getLateAttendanceTimes());
                }
            }
            //迟到次数>3 所有累积从重处罚
            else {
                if(employee_salary.getRole().equals("user"))
                {
                    employee_salary.setLateAttendanceMoney
                            (employee_salary.getBaseSalary()*0.01*employee_salary.getLateAttendanceTimes());
                }else if(employee_salary.getRole().equals("department"))
                {
                    employee_salary.setLateAttendanceMoney
                            (employee_salary.getBaseSalary()*0.02*employee_salary.getLateAttendanceTimes());
                }
            }
            //请假扣薪一视同仁~
            if(employee_salary.getRestAttendanceTimes()>0)
            {
                if(employee_salary.getRole().equals("user"))
                {
                    employee_salary.setRestAttendanceMoney
                            ((employee_salary.getBaseSalary()/wholeday)*employee_salary.getRestAttendanceTimes());
                }else if(employee_salary.getRole().equals("department"))
                {
                    employee_salary.setRestAttendanceMoney
                            ((employee_salary.getBaseSalary()/wholeday)*employee_salary.getRestAttendanceTimes());
                }
            }
            //早退，如果小于3次，则从轻处罚
            if(employee_salary.getRunAttendanceTimes()<3)
            {
                if(employee_salary.getRole().equals("user"))
                {
                    employee_salary.setRunAttendanceMoney(employee_salary.getBaseSalary()*0.005*employee_salary.getRunAttendanceTimes());
                }
                else if(employee_salary.getRole().equals("department"))
                {
                    employee_salary.setRunAttendanceMoney(employee_salary.getBaseSalary()*0.01*employee_salary.getRunAttendanceTimes());
                }
            }
            else{
                if(employee_salary.getRole().equals("user"))
                {
                    employee_salary.setRunAttendanceMoney(employee_salary.getBaseSalary()*0.01*employee_salary.getRunAttendanceTimes());

                }else if(employee_salary.getRole().equals("department"))
                {
                    employee_salary.setRunAttendanceMoney(employee_salary.getBaseSalary()*0.02*employee_salary.getRunAttendanceTimes());
                }

            }
            //根据权限分配加班费~
            if(employee_salary.getRole().equals("user"))
            {
                employee_salary.setWorkMoney(employee_salary.getBaseSalary()*0.001*employee_salary.getWorkTimes());
            }else if(employee_salary.getRole().equals("department"))
            {
                employee_salary.setWorkMoney(employee_salary.getBaseSalary()*0.002*employee_salary.getWorkTimes());
            }

            //统计薪资税收~············
            Double noTaxMoney =
                    employee_salary.getBaseSalary()+ employee_salary.getAllAttendMoney()-
                            employee_salary.getRunAttendanceMoney()-employee_salary.getAbsentAttendanceMoney()
                            -employee_salary.getRestAttendanceMoney()-employee_salary.getLateAttendanceMoney()
                            +employee_salary.getWorkMoney();
            Double TaxMoney = 0.00;
            if(noTaxMoney<=5000.0)
            {
                employee_salary.setTaxMoney(0.0);
                TaxMoney =noTaxMoney;
            }
            else if(noTaxMoney>5000.00&&noTaxMoney<=8000.00){
                employee_salary.setTaxMoney(noTaxMoney*0.03);
                TaxMoney = (noTaxMoney*(1-0.03));
            }
            else if(noTaxMoney>8000.00&&noTaxMoney<=12000.00){
                employee_salary.setTaxMoney(noTaxMoney*0.05);
                TaxMoney = (noTaxMoney*(1-0.05));
            }
            else if(noTaxMoney>12000.00&&noTaxMoney<=18000.00){
                employee_salary.setTaxMoney(noTaxMoney*0.09);
                TaxMoney = (noTaxMoney*(1-0.09));
            }
            else if(noTaxMoney>18000.00&&noTaxMoney<=24000.00){
                employee_salary.setTaxMoney(noTaxMoney*0.15);
                TaxMoney = (noTaxMoney*(1-0.15));
            }
            else if(noTaxMoney>24000.00){
                employee_salary.setTaxMoney(noTaxMoney*0.20);
                TaxMoney = (noTaxMoney*(1-0.2));
            }

            employee_salary.setTotalMoney(TaxMoney);
            System.out.println(employee_salary.toString());

            employee_salaryMapper.UpdateEmpSalaryBysalaryOrder(employee_salary);
            employee_salaryMapper.updateyearsalary(years);
        }
        list =employee_salaryMapper.SelectAllUserEmployeeSalaryBySalaryMonthDescBysalaryMonth(salaryMonth,userIds);
        return list;
    }
    public List<Employee_Salary> UpdateYearDepEmpSalary(String SalaryYear,String departmentId)
    {
        System.out.println(SalaryYear);
        List<Map<Object,Object>> list = employeeService.selectDepYearSalary(SalaryYear+"%",departmentId);
        ListIterator<Map<Object,Object>>listIterator = list.listIterator();
        Employee_Salary oldempsal = new Employee_Salary();
        String userId;
        String username;
        String role;
        while(listIterator.hasNext())
        {

            Map<Object,Object>map = listIterator.next();
            userId = map.get("userId").toString();
            username = map.get("username").toString();
            role = map.get("role").toString();
            oldempsal.setUserId(userId);
            oldempsal.setUsername(username);
            oldempsal.setRole(role);
            oldempsal.setSalaryMonth(SalaryYear);
            System.out.println(oldempsal.toString());
            if(employee_salaryMapper.selectIsExistYearSalary(userId,SalaryYear)==0)
            {
                employee_salaryMapper.InsertEmployeeSalaryInIt(oldempsal);
                oldempsal.setIssearch(1);
                employee_salaryMapper.updateIssearch(oldempsal);
            }
            oldempsal.setSalaryMonth(SalaryYear+"-%");
            List<Employee_Salary> employee_salaryList = employee_salaryMapper.selectAllMonthByuserIdAndSalaryMonth(oldempsal);
            ListIterator<Employee_Salary> employee_salaryListIterator = employee_salaryList.listIterator();
            System.out.println(employee_salaryList.size());
            oldempsal = employee_salaryMapper.selectSalaryByUserIdAndSalary(userId,SalaryYear);
            System.out.println("oldempsal:"+oldempsal.toString());
            while(employee_salaryListIterator.hasNext())
            {

                Employee_Salary newempsal = employee_salaryListIterator.next();
                if(newempsal.getIssearch()==0)
                {
                    System.out.println(userId+":"+newempsal.toString());
                    oldempsal.setBaseSalary(oldempsal.getBaseSalary()+newempsal.getBaseSalary());
                    oldempsal.setRunAttendanceTimes(oldempsal.getRunAttendanceTimes()+newempsal.getRunAttendanceTimes());
                    oldempsal.setLateAttendanceTimes(oldempsal.getLateAttendanceTimes()+newempsal.getLateAttendanceTimes());
                    oldempsal.setAbsentAttendanceTimes(oldempsal.getAbsentAttendanceTimes()+newempsal.getAbsentAttendanceTimes());
                    oldempsal.setRestAttendanceTimes(oldempsal.getRestAttendanceTimes()+newempsal.getRestAttendanceTimes());
                    oldempsal.setRunAttendanceMoney(oldempsal.getRunAttendanceMoney()+newempsal.getRunAttendanceMoney());
                    oldempsal.setLateAttendanceMoney(oldempsal.getLateAttendanceMoney()+newempsal.getLateAttendanceMoney());
                    oldempsal.setAbsentAttendanceMoney(oldempsal.getAbsentAttendanceMoney()+newempsal.getAbsentAttendanceMoney());
                    oldempsal.setRestAttendanceMoney(oldempsal.getRestAttendanceMoney()+newempsal.getRestAttendanceMoney());
                    oldempsal.setWorkTimes(oldempsal.getWorkTimes()+newempsal.getWorkTimes());
                    oldempsal.setWorkMoney(oldempsal.getWorkMoney()+newempsal.getWorkMoney());
                    oldempsal.setOnTimes(oldempsal.getOnTimes()+newempsal.getOnTimes());
                    oldempsal.setAllAttendMoney(oldempsal.getAllAttendMoney()+newempsal.getAllAttendMoney());
                    oldempsal.setTaxMoney(oldempsal.getTaxMoney()+newempsal.getTaxMoney());
                    oldempsal.setTransportSalary(oldempsal.getTransportSalary()+newempsal.getTransportSalary());
                    oldempsal.setTotalMoney(oldempsal.getTotalMoney()+newempsal.getTotalMoney());
                    //只有上够12个月的才能加年终奖~
                    if(employee_salaryList.size() == 12&&
                            (oldempsal.getRunAttendanceTimes()==0&&oldempsal.getAbsentAttendanceTimes()==0
                                    &&oldempsal.getLateAttendanceTimes()==0&&oldempsal.getRestAttendanceTimes()==0))
                    {
                        //如果全勤，则分权限补给薪资~
                        System.out.print("你全勤了");
                        if(oldempsal.getRole().equals("user"))
                        {
                            oldempsal.setAllAttendMoney(oldempsal.getAllAttendMoney()*1.2);
                        }
                        else if(oldempsal.getRole().equals("department"))
                        {
                            oldempsal.setAllAttendMoney(oldempsal.getAllAttendMoney()*2.4);
                        }
                        System.out.print("全勤奖金："+oldempsal.getAllAttendMoney());
                    }
                    System.out.println("更新后的年度薪资"+oldempsal.toString());


                    newempsal.setIssearch(1);
                    employee_salaryMapper.updateyearsalary(oldempsal);
                    employee_salaryMapper.updateIssearch(newempsal);

                }


            }

        }
        return employee_salaryMapper.selectSalaryBySalaryAndDepartmentId(SalaryYear,departmentId);
    }

    public List<Employee_Salary> UpdateYearUserEmpSalary(String SalaryYear,String userIds)
    {
        System.out.println(SalaryYear);
        Map<Object,Object> map = employeeService.selectUserYearSalary(SalaryYear+"%",userIds);
        Employee_Salary oldempsal = new Employee_Salary();
        String userId;
        String username;
        String role;
            userId = map.get("userId").toString();
            username = map.get("username").toString();
            role = map.get("role").toString();
            oldempsal.setUserId(userId);
            oldempsal.setUsername(username);
            oldempsal.setRole(role);
            oldempsal.setSalaryMonth(SalaryYear);
            System.out.println(oldempsal.toString());
            if(employee_salaryMapper.selectIsExistYearSalary(userId,SalaryYear)==0)
            {
                employee_salaryMapper.InsertEmployeeSalaryInIt(oldempsal);
                oldempsal.setIssearch(1);
                employee_salaryMapper.updateIssearch(oldempsal);
            }
            oldempsal.setSalaryMonth(SalaryYear+"-%");
            List<Employee_Salary> employee_salaryList = employee_salaryMapper.selectAllMonthByuserIdAndSalaryMonth(oldempsal);
            ListIterator<Employee_Salary> employee_salaryListIterator = employee_salaryList.listIterator();
            System.out.println(employee_salaryList.size());
            oldempsal = employee_salaryMapper.selectSalaryByUserIdAndSalary(userId,SalaryYear);
            System.out.println("oldempsal:"+oldempsal.toString());
            while(employee_salaryListIterator.hasNext())
            {

                Employee_Salary newempsal = employee_salaryListIterator.next();
                if(newempsal.getIssearch()==0)
                {
                    System.out.println(userId+":"+newempsal.toString());
                    oldempsal.setBaseSalary(oldempsal.getBaseSalary()+newempsal.getBaseSalary());
                    oldempsal.setRunAttendanceTimes(oldempsal.getRunAttendanceTimes()+newempsal.getRunAttendanceTimes());
                    oldempsal.setLateAttendanceTimes(oldempsal.getLateAttendanceTimes()+newempsal.getLateAttendanceTimes());
                    oldempsal.setAbsentAttendanceTimes(oldempsal.getAbsentAttendanceTimes()+newempsal.getAbsentAttendanceTimes());
                    oldempsal.setRestAttendanceTimes(oldempsal.getRestAttendanceTimes()+newempsal.getRestAttendanceTimes());
                    oldempsal.setRunAttendanceMoney(oldempsal.getRunAttendanceMoney()+newempsal.getRunAttendanceMoney());
                    oldempsal.setLateAttendanceMoney(oldempsal.getLateAttendanceMoney()+newempsal.getLateAttendanceMoney());
                    oldempsal.setAbsentAttendanceMoney(oldempsal.getAbsentAttendanceMoney()+newempsal.getAbsentAttendanceMoney());
                    oldempsal.setRestAttendanceMoney(oldempsal.getRestAttendanceMoney()+newempsal.getRestAttendanceMoney());
                    oldempsal.setWorkTimes(oldempsal.getWorkTimes()+newempsal.getWorkTimes());
                    oldempsal.setWorkMoney(oldempsal.getWorkMoney()+newempsal.getWorkMoney());
                    oldempsal.setOnTimes(oldempsal.getOnTimes()+newempsal.getOnTimes());
                    oldempsal.setAllAttendMoney(oldempsal.getAllAttendMoney()+newempsal.getAllAttendMoney());
                    oldempsal.setTaxMoney(oldempsal.getTaxMoney()+newempsal.getTaxMoney());
                    oldempsal.setTransportSalary(oldempsal.getTransportSalary()+newempsal.getTransportSalary());
                    oldempsal.setTotalMoney(oldempsal.getTotalMoney()+newempsal.getTotalMoney());
                    //只有上够12个月的才能加年终奖~
                    if(employee_salaryList.size() == 12&&
                            (oldempsal.getRunAttendanceTimes()==0&&oldempsal.getAbsentAttendanceTimes()==0
                                    &&oldempsal.getLateAttendanceTimes()==0&&oldempsal.getRestAttendanceTimes()==0))
                    {
                        //如果全勤，则分权限补给薪资~
                        System.out.print("你全勤了");
                        if(oldempsal.getRole().equals("user"))
                        {
                            oldempsal.setAllAttendMoney(oldempsal.getAllAttendMoney()*1.2);
                        }
                        else if(oldempsal.getRole().equals("department"))
                        {
                            oldempsal.setAllAttendMoney(oldempsal.getAllAttendMoney()*2.4);
                        }
                        System.out.print("全勤奖金："+oldempsal.getAllAttendMoney());
                    }
                    System.out.println("更新后的年度薪资"+oldempsal.toString());


                    newempsal.setIssearch(1);
                    employee_salaryMapper.updateyearsalary(oldempsal);
                    employee_salaryMapper.updateIssearch(newempsal);

                }


            }


        return employee_salaryMapper.selectAllUserSalaryByUserIdAndSalary(userId,SalaryYear+"%");
    }

    @Override
    public List<Employee_Salary> SelectAllEmployeeSalaryByuserIdAndSalaryMonthDescBysalaryMonth(String userId, String salaryMonth) {
        return employee_salaryMapper.SelectAllEmployeeSalaryByuserIdAndSalaryMonthDescBysalaryMonth(userId, salaryMonth);
    }

    @Override
    public int InsertEmployeeSalaryInIt(Employee_Salary employee_salary) {
        return employee_salaryMapper.InsertEmployeeSalaryInIt(employee_salary);
    }

    @Override
    public int UpdateEmpSalary(String baseSalary, String userId,String salaryMonth) {
        return employee_salaryMapper.UpdateEmpSalary(baseSalary, userId,salaryMonth);
    }

    @Override
    public Employee_Salary selectThisMonthBySalaryMonth(String userId, String salaryMonth) {
        return employee_salaryMapper.selectThisMonthBySalaryMonth(userId, salaryMonth);
    }

    public RespBean changeEmpSalary(Map<Object,Object>map)
    {
        System.out.println("要更新薪资的月份:"+map.get("salaryMonth").toString());
        Employee_Salary employeeSalary = new Employee_Salary();
        String deMonth = map.get("salaryMonth").toString();

        if(map.get("salaryMonth").toString().length()!=7)
        {
            System.out.println(map.get("salaryMonth").toString().substring(0,5)+"0"+map.get("salaryMonth").toString().charAt(map.get("salaryMonth").toString().length()-1));
            deMonth = map.get("salaryMonth").toString().substring(0,5)+"0"+map.get("salaryMonth").toString().charAt(map.get("salaryMonth").toString().length()-1);
        }

        employeeSalary = selectThisMonthBySalaryMonth(map.get("userId").toString(),deMonth);
        System.out.println(employeeSalary);
        if(employeeSalary.getIssearch()==1)
        {
            System.out.println("年份："+deMonth.toString().substring(0,4));
            Employee_Salary yearSalary = new Employee_Salary();
            yearSalary = selectThisMonthBySalaryMonth(map.get("userId").toString(),deMonth.substring(0,4));
            System.out.println(yearSalary);
            yearSalary.setBaseSalary(yearSalary.getBaseSalary()-employeeSalary.getBaseSalary());
            yearSalary.setTransportSalary(yearSalary.getTransportSalary()-employeeSalary.getTransportSalary());
            yearSalary.setRunAttendanceTimes(yearSalary.getRunAttendanceTimes()-employeeSalary.getRunAttendanceTimes());
            yearSalary.setLateAttendanceTimes(yearSalary.getLateAttendanceTimes()-employeeSalary.getLateAttendanceTimes());
            yearSalary.setAbsentAttendanceTimes(yearSalary.getAbsentAttendanceTimes()-employeeSalary.getAbsentAttendanceTimes());
            yearSalary.setRestAttendanceTimes(yearSalary.getRestAttendanceTimes()-employeeSalary.getRestAttendanceTimes());
            yearSalary.setRunAttendanceMoney(yearSalary.getRunAttendanceMoney()-employeeSalary.getRunAttendanceMoney());
            yearSalary.setLateAttendanceMoney(yearSalary.getLateAttendanceMoney()-employeeSalary.getLateAttendanceMoney());
            yearSalary.setAbsentAttendanceMoney(yearSalary.getAbsentAttendanceMoney()-employeeSalary.getAbsentAttendanceMoney());
            yearSalary.setRestAttendanceMoney(yearSalary.getRestAttendanceMoney()-employeeSalary.getRestAttendanceMoney());
            yearSalary.setWorkTimes(yearSalary.getWorkTimes()-employeeSalary.getWorkTimes());
            yearSalary.setWorkMoney(yearSalary.getWorkMoney()-employeeSalary.getWorkMoney());
            yearSalary.setOnTimes(yearSalary.getOnTimes()-employeeSalary.getOnTimes());
            yearSalary.setAllAttendMoney(yearSalary.getAllAttendMoney()-employeeSalary.getAllAttendMoney());
            yearSalary.setTaxMoney(yearSalary.getTaxMoney()-employeeSalary.getTaxMoney());
            yearSalary.setTotalMoney(yearSalary.getTotalMoney()-employeeSalary.getTotalMoney());
            System.out.println("年度的钱是:"+yearSalary.getTotalMoney());
            employee_salaryMapper.updateyearsalary(yearSalary);
        }

        System.out.println("钱钱是:"+employeeSalary.getTotalMoney());
       employee_salaryMapper.UpdateEmpSalary(map.get("baseSalary").toString(),map.get("userId").toString(),deMonth);
       return RespBean.ok("updateEMpMoneySuccessful!");
    }

    @Override
    public int UpdateEmpSalaryBysalaryOrder(Employee_Salary employee_salary) {
        return employee_salaryMapper.UpdateEmpSalaryBysalaryOrder(employee_salary);
    }

    @Override
    public List<Employee_Salary> selectAllMonthByuserIdAndSalaryMonth(Employee_Salary employee_salary) {
        return employee_salaryMapper.selectAllMonthByuserIdAndSalaryMonth(employee_salary);
    }

    @Override
    public int selectIsExistYearSalary(String userId, String salaryMonth) {
        return employee_salaryMapper.selectIsExistYearSalary(userId, salaryMonth);
    }

    @Override
    public Employee_Salary selectSalaryByUserIdAndSalary(String userId, String salaryMonth) {
        return employee_salaryMapper.selectSalaryByUserIdAndSalary(userId, salaryMonth);
    }

    @Override
    public List<Employee_Salary> selectAllUserSalaryByUserIdAndSalary(String userId, String salaryMonth) {
        return employee_salaryMapper.selectAllUserSalaryByUserIdAndSalary(userId, salaryMonth);
    }

    @Override
    public List<Employee_Salary> selectSalaryBySalary(String salaryMonth) {
        return employee_salaryMapper.selectSalaryBySalary(salaryMonth);
    }

    @Override
    public List<Employee_Salary> selectSalaryBySalaryAndDepartmentId(String salaryMonth, String departmentId) {
        return employee_salaryMapper.selectSalaryBySalaryAndDepartmentId(salaryMonth, departmentId);
    }

    @Override
    public int updateIssearch(Employee_Salary employee_salary) {
        return employee_salaryMapper.updateIssearch(employee_salary);
    }

    @Override
    public int updateyearsalary(Employee_Salary employee_salary) {
        return employee_salaryMapper.updateyearsalary(employee_salary);
    }



    public RespBean InitEmpSal(String salaryMonth)
    {
        //因为创建整个表很浪费时间，我选择通过创建培训、创建考勤的起始日期判断创建，ApiPost此方法10.00MS,这个花了9.02S，仅仅6个员工，如果255个员工岂不是炸裂
//            List<Map<Object, Object>> Emp = employeeService.selectEmpUserIdAnduserName();
//            ListIterator<Map<Object,Object>> listIterator = Emp.listIterator();
//        String userId;
//        String username;
//        String role;
//        String salaryMonths;
//        Employee_Salary employee_salary = new Employee_Salary();
//            while(listIterator.hasNext())
//            {
//                Map<Object,Object> map = listIterator.next();
//                userId = map.get("userId").toString();
//                username = map.get("username").toString();
//                role = map.get("role").toString();
//                System.out.println(userId+username+role);
//                employee_salary.setUserId(userId);
//                employee_salary.setUsername(username);
//                employee_salary.setRole(role);
//                for(int i = 1 ; i < 10; i++) {
//                    salaryMonths = salaryMonth + "-0" + i;
//                    employee_salary.setSalaryMonth(salaryMonths);
//                    employee_salaryMapper.InsertEmployeeSalaryInIt(employee_salary);
//                }
//                for(int i = 10 ; i <= 12 ; i++)
//                {
//                    salaryMonths = salaryMonth + "-" + i;
//                    employee_salary.setSalaryMonth(salaryMonths);
//                    employee_salaryMapper.InsertEmployeeSalaryInIt(employee_salary);
//                }
//
//            }
        return RespBean.ok("ok");
    }
}
