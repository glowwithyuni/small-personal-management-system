package org.pjx.spm.service;

import org.pjx.spm.mapper.Employee_SalaryMapper;

/**
 * @Author Glow
 * @Date 2022-03-23 20:32:22
 * @Description
 * @Version 1.0
 */
public interface Employee_SalaryService extends Employee_SalaryMapper {
}
