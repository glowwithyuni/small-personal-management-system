package org.pjx.spm.service;

import org.pjx.spm.mapper.Department_empMapper;

/**
 * @Author Glow
 * @Date 2022-03-03 10:49:09
 * @Description
 * @Version 1.0
 */
public interface Department_empService  extends Department_empMapper {
}
