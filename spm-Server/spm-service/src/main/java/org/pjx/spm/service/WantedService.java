package org.pjx.spm.service;

import org.pjx.spm.mapper.WantedMapper;
import org.pjx.spm.pojo.Department_emp;
import org.pjx.spm.pojo.Employee;
import org.pjx.spm.pojo.User;
import org.pjx.spm.pojo.Wanted;

import java.util.List;

/**
 * @Author Glow
 * @Date 2022-04-15 00:21:49
 * @Description
 * @Version 1.0
 */
public interface WantedService extends WantedMapper {
    @Override
    int deleteWantedByWantedRage(Integer wantedrage);

    @Override
    int InsertNewWanted(Wanted wanted);

    @Override
    int updateWantedByWantedRage(Wanted wanted);

    @Override
    List<Wanted> selectWantedBydepartmentId(Integer departmentId);

    @Override
    List<Wanted> selectAllWanted();

    @Override
    List<Wanted> selectWantedByDepartmentIdAndIsaccept(Integer departmentId, Integer isaccept);
    @Override
    List<Wanted> selectWantedByIsaccept(Integer isaccept);

    @Override
    int updateWantedIsacceptBywantedrage(Integer wantedrage, Integer isaccept);

    @Override
    List<Wanted> selectWantedByDepartmentIdAndworkName(String departmentId, String workName);

    @Override
    int userIdIsIllegal(String userId);

    @Override
    int workIdIsIllegal(String workId, String departmentId);

    @Override
    int InsertNewDep(Department_emp department_emp);

    @Override
    int InsertNewEmp(Employee employee);

    @Override
    int InsertNewUser(User user);

    @Override
    int updateWantedDatrByWantedRage(String getDate, String wantedrage);

    @Override
    int InsertNewWanteds(Wanted wanted);
}
