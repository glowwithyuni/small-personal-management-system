package org.pjx.spm.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.pjx.spm.pojo.Employee_Salary;

import java.util.List;

/**
 * @Author Glow
 * @Date 2022-03-23 17:20:17
 * @Description 薪资管理Mapper
 * @Version 1.0
 */
@Mapper
public interface Employee_SalaryMapper {
    List<Employee_Salary> SelectAllEmployeeSalaryBySelectedSalaryMonth();
    List<Employee_Salary> SelectAllEmployeeSalaryBySalaryMonthDescBysalaryMonth(@Param("salaryMonth")String salaryMonth);
    List<Employee_Salary> SelectAllDepEmployeeSalaryBySalaryMonthDescBysalaryMonth(@Param("salaryMonth")String salaryMonth,
                                                                                   @Param("departmentId")String departmentId);
    List<Employee_Salary> SelectAllUserEmployeeSalaryBySalaryMonthDescBysalaryMonth(@Param("salaryMonth")String salaryMonth,
                                                                                   @Param("userId")String userId);

    List<Employee_Salary> SelectAllEmployeeSalaryByuserIdAndSalaryMonthDescBysalaryMonth(@Param("userId")String userId,@Param("salaryMonth")String salaryMonth);
    int InsertEmployeeSalaryInIt(Employee_Salary employee_salary);
    int UpdateEmpSalary(@Param("baseSalary")String baseSalary,
                        @Param("userId")String userId,
                        @Param("salaryMonth")String salaryMonth);
    Employee_Salary selectThisMonthBySalaryMonth(@Param("userId")String userId,@Param("salaryMonth")String salaryMonth);
    int UpdateEmpSalaryBysalaryOrder(Employee_Salary employee_salary);
    List<Employee_Salary> selectAllMonthByuserIdAndSalaryMonth(Employee_Salary employee_salary);
    int selectIsExistYearSalary(@Param("userId")String userId,@Param("salaryMonth")String salaryMonth);
    Employee_Salary selectSalaryByUserIdAndSalary(@Param("userId")String userId,@Param("salaryMonth")String salaryMonth);
    List<Employee_Salary> selectAllUserSalaryByUserIdAndSalary(@Param("userId")String userId,@Param("salaryMonth")String salaryMonth);
    List<Employee_Salary> selectSalaryBySalary(@Param("salaryMonth")String salaryMonth);

    List<Employee_Salary> selectSalaryBySalaryAndDepartmentId(@Param("salaryMonth")String salaryMonth,@Param("departmentId")String departmentId);
    int updateIssearch(Employee_Salary employee_salary);
    int updateyearsalary(Employee_Salary employee_salary);
}
