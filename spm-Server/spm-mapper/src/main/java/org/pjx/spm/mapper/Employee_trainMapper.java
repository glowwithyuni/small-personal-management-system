package org.pjx.spm.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.pjx.spm.pojo.Employee_train;

import java.util.List;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2022-03-05 01:47:20
 * @Description 员工培训mapper层
 * @Version 1.0
 */
@Mapper
public interface Employee_trainMapper {
    List<Employee_train>selectAlltrain();
    List<Employee_train>selectTrainByuserId(@Param("userId")String userId);
    List<Employee_train>selectTrainByuserIdAndDepartmentId(@Param("userId")String userId,@Param("departmentId")String departmentId);
    List<Employee_train>selectTrainByDepId(@Param("departmentId")Integer departmentId);
    List<Employee_train>selectTrainByIsCheck(@Param("userId")String userId,@Param("ischeck")String ischeck);
    List<Employee_train>selectTrainByIsFinish(@Param("userId")String userId,@Param("isfinish")String isfinish);
    List<Employee_train>selectTrainByIsCheckAndDepartmentId(@Param("userId")String userId,@Param("ischeck")String ischeck,@Param("departmentId")String departmentId);
    List<Employee_train>selectTrainByIsFinishAndDepartmentId(@Param("userId")String userId,@Param("isfinish")String isfinish,@Param("departmentId")String departmentId);
//    获取新建培训员工视图
    List<Map<Object,Object>>selectTrainEmp();
    List<Map<Object,Object>>selectTrainEmpByDepId(@Param("departmentId") Integer departmentId);
    int InsertNewTrain(Employee_train employee_train);
    int UpdateisTrain(@Param("userId")String userId);
    int UpdateCancelTrain(@Param("userId")String userId);
    int DeleteNotCheckTrain(@Param("trainrage")Integer trainrage );
    int UpdateCheckTrainStart(Employee_train employee_train);
    int UpdateFinishTrain(Employee_train employee_train);
    List<Employee_train> selectTrainByIdAndStartDate(@Param("userId")String userId,@Param("trainstartdate")String trainstartdate);
    int updatesetaddBytrainrage(@Param("trainrage")Integer trainrage,@Param("isadd")Integer isadd);
}
