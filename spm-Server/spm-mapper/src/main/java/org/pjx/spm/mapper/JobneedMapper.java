package org.pjx.spm.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.pjx.spm.pojo.Jobneed;

import java.util.List;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2022-04-14 13:38:22
 * @Description
 * @Version 1.0
 */
@Mapper
public interface JobneedMapper {
    List<Jobneed>  selectAllJobneed();
    List<Jobneed> selectJobneedBydepartmentId(@Param("departmentId")Integer departmentId);
    int InsertJobneed(Jobneed jobneed);
    int updateJobneedByJobrage(Jobneed jobneed);
    int deleteJobneedByJobrage(@Param("jobrage")Integer jobrage);
    Map<Object,Object> SelectDepUserNameBeforeInsert(@Param("userId")String userId);
    List<Integer>selectDepartmentId();
    Map<Object,Object>selectDepByDepartmentId(@Param("departmentId")Integer departmentId);

}
