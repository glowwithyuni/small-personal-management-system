package org.pjx.spm.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.pjx.spm.pojo.Employee;
import org.pjx.spm.pojo.EmployeeTool;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2022-02-16 16:11:38
 * @Description 员工信息
 * @Version 1.0
 */
@Mapper
public interface EmployeeMapper {
    //获取指定员工ID的员工信息
    Employee getEmployeeById(@Param("userId")String userId);
    //更新指定员工ID的地址
    int updateAddressById(@Param("Address")String Address, @Param("userId")String userId);
    //更新指定员工ID的电话
    int updatePhoneById(@Param("Phone")String Phone,@Param("userId")String userId);
    //更新指定员工ID的除部门的信息
    int updateAcceptDepartmentById(Employee employee);
    int updateAndIdcardAcceptDepartmentById(Employee employee);
    //更新指定员工ID的身份证号码
    int updateIdcardById(@Param("Idcard")String Idcard,@Param("userId")String userId);
   //查看全部雇员信息
    List<Employee> getAllEmployee();
    //根据权限展示成员
    List<Employee> getEmployeeByRole(@Param("role")String role);
    //根据权限和ID模糊查询
    List<Employee> getEmployeeByRoleAnduserId(@Param("userId")String userId,@Param("role")String role);
   //获取未设置员工信息的员工ID
    List<EmployeeTool>  getNotInEmployeeUserId();
    //插入员工数据
    int InsertEmployeeAcceptDepartment(Employee employee);
    //根据userId删除员工信息~
    int deleteEmployeeByuserId(@Param("userId")String userId);



    //部门管理惹
    //根据userId随Department_emp表的员工加入部门操作而接着更新~
    int UpdateDepartmentByuserId(Map<String,Object> map);
    //根据userId随department_emp退出部门、更换部长操作、删除employee而清空员工信息
    int UpdateDepartmentClearByuserId(@Param("userId")String userId);
    //根据departmentId随着department_emp更新部门名而更新部门名@Param("departmentName")String departmentName,@Param("departmentId")Integer departmentId
    int updatedepEmployeeName(Map<String,Object>map);
    List<Map<Object,Object>> selectYearSalary(@Param("salaryMonth")String salaryMonth);
    List<Map<Object,Object>> selectDepYearSalary(@Param("salaryMonth")String salaryMonth,@Param("departmentId")String departmentId);
    Map<Object,Object>selectUserYearSalary(@Param("salaryMonth")String salaryMonth,@Param("userId")String userId);
    List<Map<Object,Object>> selectEmpUserIdAnduserName(@Param("salaryMonth")String salaryMonth);
    List<Map<Object,Object>> selectEmpUserIdAnduserNameByDepartmentId(@Param("salaryMonth")String salaryMonth,@Param("departmentId")String departmentId);
    String selectRoleByUserId(@Param("userId")String userId);
    int InsertInitEmp(@Param("userId")String userId,@Param("username")String username,
                      @Param("role")String role,@Param("email")String email
                      );
    int DepEmpIsExist(@Param("userId")String userId);
    List<Employee> selectEmpByDepId(@Param("departmentId")Integer departmentId);
    Employee selectEmpByuserIdAndDepartmentId(Employee employee);
}
