package org.pjx.spm.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.pjx.spm.pojo.Work_attendance;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2022-03-16 15:54:49
 * @Description
 * @Version 1.0
 */
@Mapper
public interface Work_attendanceMapper {
List<Work_attendance> selectAllWorkAttendance();
    List<Work_attendance> selectAllWorkAttendanceByDepartmentId(@Param("departmentId")String departmentId);
    List<Work_attendance> selectAllWorkAttendanceByDepartmentIdThisMonth(@Param("departmentId")String departmentId,@Param("checkdate")String checkdate);
Work_attendance selectAllWorkAttendanceByAttendance(@Param("attendance")Integer attendance);
List<Work_attendance>selectAllWorkAttendanceAscBycheckstartdate();
List<Work_attendance>selectAttendByCheckdate(@Param("checkdate") String checkdate);
    List<Work_attendance>selectAttendByCheckdateAndDepId(@Param("checkdate") String checkdate,@Param("departmentId")String departmentId);


int UpdateSetChecktype(Work_attendance work_attendance);
List<Map<String, String>>selectAllEmpUserIdAndUsername();
    List<Map<String, String>>selectAllDepEmpUserIdAndUsername(@Param("departmentId")Integer departmentId);
int InsertNewAttendance(Work_attendance work_attendance);
int UpdateAttendanceByattendance(Work_attendance work_attendance);
int DeleteWorkAttendanceByattendance(@Param("attendance")Integer attendance);
List<Work_attendance>selectAttendanceByIdAndCheckDate(@Param("userId")String userId,@Param("checkdate") String checkdate);
int updateIssearchByAttendance(@Param("attendance")Integer attendance,@Param("issearch")Integer issearch);
int selectissearchByAttendance(@Param("attendance")Integer attendance);
List<Work_attendance>selectAllWorkAttendanceAscBycheckstartdateByThisMonth(@Param("checkdate") String checkdate);
    List<Work_attendance>selectMonthAttendanceByuserId(@Param("userId")String userId,@Param("checkdate") String checkdate);
    List<Work_attendance>selectDayAttendanceByuserId(@Param("userId")String userId,@Param("checkdate") String checkdate);
    List<Work_attendance>selectDayAttendanceByuserIdAndDepId(@Param("userId")String userId,@Param("checkdate") String checkdate,@Param("departmentId")String departmentId);
Double selectBaseSalaryFromEmp(@Param("userId")String userId);
}
