package org.pjx.spm.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.pjx.spm.pojo.Employee;
import org.pjx.spm.pojo.User;
import org.pjx.spm.pojo.employee_request;

import java.util.List;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2022-01-27 13:10:22
 * @Description
 * @Version 1.0
 */
@Mapper
public interface UserMapper {
    //检验账号密码是否正确，后续中期后会加上passwordEncoder和token
    int accountIsTrue(@Param("userId")String userId,@Param("password")String password);
   //根据userId获取user信息
    User selectUserById(@Param("userId")String userId);
    //根据user更改密码
    int changePassword(User user);
    //查询用户是否存在
    int userIsExistById(@Param("userId")String userId);
    //请求管理员重设密码（做邮箱认证就会清除无用mapper）
    int requestsetpassword(employee_request employeeRequest);
    //查询用户请求是否存在（做邮箱认证就会清除无用mapper）
    int userrequestIsExistById(@Param("userId")String userId);
    //根据用户Id删除请求（做邮箱认证就会清除无用mapper）
    int deletepasswordrequestById(@Param("userId")String userId);

    //查询用户重设密码请求和验证码是否存中（做邮箱认证就会清除无用mapper）
    int userrequestAndCheckcodeIsExistById(@Param("userId")String userId,@Param("checkcode")String checkcode);
    //获取管理员设置的重设密码（做邮箱认证就会清除无用mapper）
    int getnewpassword(User user);
    //根据userId获取旧密码
    String getoriginpasswordById(@Param("userId")String userId);
    //根据userId更新username和email
    int updateusernameAndemailById(User user);
    //根据userId获取username和email
    Map<String,Object> getUsernameAndEmailAndroleById(@Param("userId")String userId);
    //根据userId更新username、email、role（新建雇员时用）
    int updateusernameAndemailAndroleById(User user);
    //新建用户时加入部门管理中
   int InsertInitDepartmentMate(Map<String,Object> map);
   String selectroleByuserId(@Param("userId")String userId);
   int UserIdAndEmailIsExist(@Param("userId")String userId,@Param("email")String email);
   int updatePasswordByUserId(@Param("password")String password,@Param("userId")String userId);
   List<employee_request>selectAllEmployee_request();
   int updateResetEmpRequestCode(employee_request employee_requests);
   String selectEmailByuserId(@Param("userId")String userId);
   int updatetokenByuserId(@Param("token")String token,@Param("userId")String userId);
   String selecttokenByuserId(@Param("userId")String userId);
}
