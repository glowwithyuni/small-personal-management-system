package org.pjx.spm.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.pjx.spm.pojo.Department_emp;
import org.pjx.spm.pojo.Employee;
import org.pjx.spm.pojo.User;
import org.pjx.spm.pojo.Wanted;

import java.util.List;

/**
 * @Author Glow
 * @Date 2022-04-14 13:52:15
 * @Description
 * @Version 1.0
 */
@Mapper
public interface WantedMapper {
       int InsertNewWanted(Wanted wanted);
        List<Wanted>selectAllWanted();
        List<Wanted>selectWantedBydepartmentId(@Param("departmentId")Integer departmentId);
        int updateWantedByWantedRage(Wanted wanted);
    int deleteWantedByWantedRage(@Param("wantedrage")Integer wantedrage);
        List<Wanted>selectWantedByIsaccept(@Param("isaccept")Integer isaccept);
    List<Wanted>selectWantedByDepartmentIdAndIsaccept(@Param("departmentId")Integer departmentId,@Param("isaccept")Integer isaccept);
    int updateWantedIsacceptBywantedrage(@Param("wantedrage")Integer wantedrage,@Param("isaccept")Integer isaccept);
    List<Wanted>selectWantedByDepartmentIdAndworkName(@Param("departmentId")String departmentId,@Param("workName")String workName);
    int userIdIsIllegal(@Param("userId")String userId);
    int workIdIsIllegal(@Param("workId")String workId,@Param("departmentId")String departmentId);
    //因为时间实在不够，所以不分mapper了
    int InsertNewUser(User user);
    int InsertNewEmp(Employee employee);
    int InsertNewDep(Department_emp department_emp);
    int updateWantedDatrByWantedRage(@Param("getDate")String getDate,@Param("wantedrage")String wantedrage);
    int InsertNewWanteds(Wanted wanted);
}
