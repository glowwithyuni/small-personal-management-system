package org.pjx.spm.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.pjx.spm.pojo.Department;
import org.pjx.spm.pojo.Department_emp;

import java.util.List;
import java.util.Map;

/**
 * @Author Glow
 * @Date 2022-03-03 10:40:58
 * @Description 部门员工信息
 * @Version 1.0
 */
@Mapper
public interface Department_empMapper {
    //获取所有部门成员信息
    List<Department_emp>getAllDepartmentEmp();
    //获取指定部门的员工信息
    List<Department_emp>getDepartmentEmpBydepartmentId(@Param("departmentId")Integer departmentId);
    //根据员工Id获取员工所在部门的个人职位信息
    List<Department_emp>getDepartmentEmpByuserId(@Param("userId")String userId);
    //根据部门Id获取部门的部长ID和部长名
    List<Department_emp>getDepartmenterByDepartmentId(@Param("departmentId")Integer departmentId);
    //根据userId设置进入员工加入部门的信息
    int updateEmpByuserId(Department_emp department_emp);
    //统计部门人数
    int getDepartmentNum(@Param("departmentId")Integer departmentId);
    //更新部门人数
    int UpdateDepartmentNum(@Param("departmentId")Integer departmentId);
    //移除指定userId的部门员工置于闲置
    int UpdateRemoveEmpInDepartmentByUserId(@Param("userId")String userId);
    //创建新部门 ，无设定部长的情况下
    int InsertnewDepartmentWithoutEmp(Department department);
    //创建新部门 ，选定部长的情况下
    int InsertnewDepartment(Department department);
    //更换部长1-1
    List<Department_emp> getOriginDepartmenter(@Param("departmentId")Integer departmentId);
    //更换部长1-2
    int updateChangeDepartmenter(Department department);
    //更换部长1-3
    int updatenewDepartmentEmpByuserId(Department_emp department_emp);
    //更换部长1-4
    int updateoldDepartmentEmpByuserId(@Param("userId")String userId);
    //查看未安排的部门成员
    List<Department_emp> getnoworkEmp();
    //查看安排的部门成员
    List<Department_emp> getworkEmp();
    //修改部门名1-1
    int updatedepartmentName(@Param("departmentName")String departmentName,@Param("departmentId")Integer departmentId);
    //修改部门名1-2
    int updatedepartmentEmpName(@Param("departmentName")String departmentName,@Param("departmentId")Integer departmentId);
    //查看所有部门信息
    List<Department> getDepartment();
    //根据部门Id获取部门信息
    List<Department> getDepartmentByDepartmentId(@Param("departmentId")Integer departmentId);
    //获取不是部长的成员的id和名字
    List<Department_emp> getUserIdByNoDepartment();
    //根据userId获取userName
    Map<String,Object> getUserNameByNoDepartment(@Param("userId")String userId);
//    根据userId更改权限
    int updateUserRoleByuserId(@Param("role")String role,@Param("userId") String userId);
    int updateEmpRoleByuserId(@Param("role")String role,@Param("userId") String userId);
    int updateDepRoleByuserId(@Param("role")String role,@Param("userId") String userId);
//根据departmentId更改部门名
    int updateEmpDepartmentNameByDepartmentId(@Param("departmentName")String departmentName,@Param("departmentId")Integer departmentId);
    int updateDepEmpDepartmentNameByDepartmentId(@Param("departmentName")String departmentName,@Param("departmentId")Integer departmentId);
    int updateDepDepartmentNameByDepartmentId(@Param("departmentName")String departmentName,@Param("departmentId")Integer departmentId);

    List<String> getDepartment_empuserIdBydepartmentId(@Param("departmentId")Integer departmentId);
    int ClearDepartment_emp(@Param("userId") String userId);
    int ClearEmployeeDepartment(@Param("userId") String userId);
    int deleteDepartmentByDepartmentId(@Param("departmentId")Integer departmentId);

//    根据depId查询部门成员信息
    List<Department_emp> selectDepByDepartmentId(@Param("departmentId")Integer departmentId);
    //    根据userId查询部门成员信息
    List<Department_emp> selectDepByuserId(@Param("userId")String userId);
//    根据departmentId查询部门详细信息
    Department selectDepMsgByDepartmentId(@Param("departmentId")Integer departmentId);
    int updateDepuserNameByuserId(@Param("username")String username,@Param("workName")String workName,@Param("userId")String userId);
    int updateMinisteruserNameByuserId(@Param("username")String username,
                                       @Param("workName")String workName,
                                       @Param("userId")String userId,
                                       @Param("departmentName") String departmentName,
                                       @Param("departmentId")String departmentId);
//    移除部门成员
    int removeDepartmentMate(@Param("userId")String userId,
                             @Param("departmentId")String departmentId);
//    添加部门成员
    int addnewDepMate(@Param("departmentId")String departmentId,
                  @Param("departmentName") String departmentName,
                  @Param("userId")String userId,
                  @Param("workName")String workName,
                  @Param("workId")String workId
                  );
//获取所有部门id
    List<String>selectDepIdFromDepartment();
//    根据部门Id获取对应DepName
    String selectDepNameFromDepartmentByDepId(@Param("departmentId")String departmentId);
//    获取无安排就职的员工
    List<String>selectNoWorkuserId();

    int UpdateNewDep(@Param("departmentId")String departmentId,
                     @Param("departmentName") String departmentName,
                     @Param("workId")String workId,
                     @Param("workName")String workName,
                     @Param("userId")String userId
                     );
    int clearOldDep(@Param("userId")String userId);
//解散部门
    int UpdateOutputEmpAndDep(@Param("departmentId")String departmentId,@Param("userId")String userId);
//    按照role升序排序的dep——emp
    List<Department_emp>selectdepartment_empByrole();
    int InsertInitDepEmp( @Param("userId")String userId,
                          @Param("username")String username,
                          @Param("role")String role
                          );
    List<Department_emp>selectDepartmentEmpByDepartmentId(@Param("departmentId")Integer departmentId);
    Department selectDepartmentByDepartmentId(@Param("departmentId")Integer departmentId);
    List<Integer>selectAlldepartmentId();
    int updateDepNum(@Param("departmentId")Integer departmentId);
    int updateSalaryPower(@Param("role")String role,@Param("userId")String userId,@Param("salaryMonth")String salaryMonth);
}
