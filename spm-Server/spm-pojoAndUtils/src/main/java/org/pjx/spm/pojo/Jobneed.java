package org.pjx.spm.pojo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

/**
 * @Author Glow
 * @Date 2022-04-14 13:30:49
 * @Description 应聘岗位信息
 * @Version 1.0
 */
@Data
@Getter
@Setter
@Component
public class Jobneed {
    private Integer jobrage;//岗位序列
    private String userId;//员工ID
    private String username;//员工姓名
    private Integer departmentId;//员工部门id
    private String departmentName;//员工部门名
    private String workName;//员工职位
    private double baseSalary;//员工基本工资
    private String description;//职位要求
    private int needNum;//职位需求人数
}
