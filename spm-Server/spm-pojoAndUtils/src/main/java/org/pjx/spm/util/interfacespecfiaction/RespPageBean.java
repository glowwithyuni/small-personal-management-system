package org.pjx.spm.util.interfacespecfiaction;

import java.util.List;

/**
 * @Author Glow
 * @Date 2022-02-18 13:05:29
 * @Description 接口规范工具类
 * @Version 1.0
 */
public class RespPageBean {
    private Long total;
    private List<?> data;

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List<?> getData() {
        return data;
    }

    public void setData(List<?> data) {
        this.data = data;
    }
}
