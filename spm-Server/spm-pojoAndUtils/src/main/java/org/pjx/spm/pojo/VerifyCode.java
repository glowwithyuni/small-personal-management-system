package org.pjx.spm.pojo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author Glow
 * @Date 2022-01-28 23:28:24
 * @Description 验证码
 * @Version 1.0
 */
@Data
@Getter
@Setter
public class VerifyCode {
    private String code;//验证码答案
    private byte[] imgBytes;//验证码图片（Byte格式）
    private long waittime;//最长有效时间
}
