package org.pjx.spm.util.config;

import org.pjx.spm.pojo.VerifyCode;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @Author Glow
 * @Date 2022-01-28 23:31:11
 * @Description 验证码生成接口
 * @Version 1.0
 */
public interface VerifyCodeGeneral {
    /**
     * 生成验证码并将其写入输出流种
     * @param width
     * @param height
     * @param outputStream
     * @return
     * @throws IOException
     */
    String generate(int width, int height, OutputStream outputStream)throws IOException;

    /**
     * 生成验证码对象
     * @param width
     * @param height
     * @return
     * @throws IOException
     */
    VerifyCode generate(int width,int height)throws IOException;
}
