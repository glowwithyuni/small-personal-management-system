package org.pjx.spm.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @Author Glow
 * @Date 2022-03-05 01:27:41
 * @Description 员工培训
 * @Version 1.0
 */
@Data
@Getter
@Setter
public class Employee_train {
    private Integer trainrage;//培训队列
    private String userId;//员工Id
    private String username;//员工姓名
    private String gender;//员工性别
    private String trainskill;//培训内容
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",locale = "zh",timezone = "GMT+8")
    private Date   trainstartdate;//培训起始时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",locale = "zh",timezone = "GMT+8")
    private Date   trainenddate;//培训结束时间
    private String ischeck;//是否被审批通过
    private String isfinish;//是否完成培训
    private String remark;//报表内容
    private Double transportSalary;//车旅费
    private int isadd;//是否被薪资审核
}
