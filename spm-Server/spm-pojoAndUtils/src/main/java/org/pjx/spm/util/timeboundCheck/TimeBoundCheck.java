package org.pjx.spm.util.timeboundCheck;

import java.util.Calendar;
import java.util.Date;

/**
 * @Author Glow
 * @Date 2022-03-16 18:13:51
 * @Description 判断时间是否在同一区间内
 * @Version 1.0
 */
public class TimeBoundCheck {

    /**
     *
     * @param nowTime   当前时间
     * @param startTime	开始时间
     * @param endTime   结束时间
     * @return
     * @author sunran   判断当前时间在时间区间内
     */
    public static boolean isEffectiveDate(Date nowTime, Date startTime, Date endTime) {
        if (nowTime.getTime() == startTime.getTime()
                || nowTime.getTime() == endTime.getTime()) {
            return true;
        }

        Calendar date = Calendar.getInstance();
        date.setTime(nowTime);

        Calendar begin = Calendar.getInstance();
        begin.setTime(startTime);

        Calendar end = Calendar.getInstance();
        end.setTime(endTime);
//        System.out.println("比较时间："+date+"\n"+"开始时间："+begin+"\n"+"结束时间："+end+"\n");
//        System.out.println(date.after(begin));
//        System.out.println(date.before(end));

        if (date.after(begin) && date.before(end)) {

                return true;
        } else {
            return false;
        }
    }
    public static boolean AutoCheckIsEffectiveDate(Date nowTime, Date startTime, Date endTime) {
        if (nowTime.getTime() == startTime.getTime()
                || nowTime.getTime() == endTime.getTime()) {
            return true;
        }

        Calendar date = Calendar.getInstance();
        date.setTime(nowTime);

        Calendar begin = Calendar.getInstance();
        begin.setTime(startTime);

        Calendar end = Calendar.getInstance();
        end.setTime(endTime);
//        System.out.println("比较时间："+date+"\n"+"开始时间："+begin+"\n"+"结束时间："+end+"\n");
//        System.out.println(date.after(begin));
//        System.out.println(date.before(end));
        if(date.before(begin))
        {
            return true;
        }
        if (date.after(begin) && date.before(end)) {

            return true;
        } else {
            return false;
        }
    }
}
