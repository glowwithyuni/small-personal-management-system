package org.pjx.spm.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.sql.Date;

/**
 * @Author Glow
 * @Date 2022-03-23 15:17:55
 * @Description 员工薪资表
 * @Version 1.0
 */
@Data
@Getter
@Setter
public class Employee_Salary {
    private Integer salaryOrder;
    private String userId;
    private String username;
    private String role;
    //基础工资
    private Double baseSalary;
    //月份 2022-03

    private String salaryMonth;
    //全勤奖金
    private Double allAttendMoney;
    //请假罚金
    private Double restAttendanceMoney;
    //早退罚金
    private Double runAttendanceMoney;
    //迟到罚金
    private Double lateAttendanceMoney;
    //缺勤罚金
    private Double absentAttendanceMoney;
    //加班奖励
    private Double workMoney;
    //税金
    private Double taxMoney;
    //车旅费
    private Double transportSalary;
    //正常次数
    private int onTimes;
    //早退次数
    private int runAttendanceTimes;
    //早退次数
    private int restAttendanceTimes;
    //早退次数
    private int lateAttendanceTimes;
    //早退次数
    private int absentAttendanceTimes;
    //月加班时长
    private int workTimes;
    //总实际获得资金
    private Double totalMoney;
//    是否被年度标记
private Integer issearch;
}
