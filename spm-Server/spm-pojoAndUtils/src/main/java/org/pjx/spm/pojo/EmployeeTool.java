package org.pjx.spm.pojo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

/**
 * @Author Glow
 * @Date 2022-03-01 20:05:48
 * @Description 获取未创建雇员列表的userId
 * @Version 1.0
 */
@Data
@Getter
@Setter
@Component
public class EmployeeTool {
    private String userId;
}
