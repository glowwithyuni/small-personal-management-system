package org.pjx.spm.pojo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

/**
 * @Author Glow
 * @Date 2022-03-03 09:50:55
 * @Description 部门表
 * @Version 1.0
 */
@Data
@Getter
@Setter
@Component
public class Department {
    private String departmentId;//部门Id
    private String departmentName;//部门名
    private String userId;//所属部长Id
    private String username;//所属部长名称
    private Integer empNum;//部门人数
}
