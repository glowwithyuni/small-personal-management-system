package org.pjx.spm.pojo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

/**
 * @Author Glow
 * @Date 2022-01-27 13:06:37
 * @Description
 * @Version 1.0
 */


@Data
@Getter
@Setter
@Component
public class User {
    //账号，密码，token(如果没时间就不做拦截器了)，用户名，权限级别，部门名。
    private String userId;
    private String password;
    private String token;
    private String username;
    private String role;
    private String departmentName;
    private String email;
}
