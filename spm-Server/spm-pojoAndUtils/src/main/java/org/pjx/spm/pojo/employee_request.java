package org.pjx.spm.pojo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

/**
 * @Author Glow
 * @Date 2022-02-07 14:26:09
 * @Description 员工请求entity
 * @Version 1.0
 */
@Data
@Getter
@Setter
@Component
public class employee_request {
    private String userId;
    private String requestoftype;
    private String checkcode;
    private int isavailable;
    private String requestcode;
}
