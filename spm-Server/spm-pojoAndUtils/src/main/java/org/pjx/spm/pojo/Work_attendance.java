package org.pjx.spm.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @Author Glow
 * @Date 2022-03-16 15:52:05
 * @Description 考勤表
 * @Version 1.0
 */
@Data
@Getter
@Setter
public class Work_attendance {
    private Integer attendance;//考勤序列
    private String userId;//员工Id
    private String username;//员工姓名
    @JsonFormat(pattern = "yyyy-MM-dd",locale = "zh",timezone = "GMT+8")
    private Date checkdate;//考勤检查开始年月日
    @JsonFormat(pattern = "HH:mm:ss",locale = "zh",timezone = "GMT+8")
    private Date checkstarttime;//考勤检查开始时间
    @JsonFormat(pattern = "HH:mm:ss",locale = "zh",timezone = "GMT+8")
    private Date checkendtime;//考勤检查开始时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",locale = "zh",timezone = "GMT+8")
    private Date checkstartdate;//考勤检查开始时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",locale = "zh",timezone = "GMT+8")
    private Date   checkenddate;//考勤结束时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",locale = "zh",timezone = "GMT+8")
    private Date   attendcheckdate;//上班打卡时间
    private String checktype;//考勤类别
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",locale = "zh",timezone = "GMT+8")
    private Date createdate;//考勤建立时间
    private String attendtype;//考勤类型
    private Integer workhour;//加班时长~
    private Integer issearch;//是否被记录在薪资中

}
