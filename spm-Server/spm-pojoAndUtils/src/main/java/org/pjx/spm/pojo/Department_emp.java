package org.pjx.spm.pojo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

/**
 * @Author Glow
 * @Date 2022-03-02 23:50:31
 * @Description 所有部门员工表
 * @Version 1.0
 */
@Data
@Getter
@Setter
@Component
public class Department_emp {
    private Integer departmentId;//部门Id
    private String departmentName;//部门名
    private String userId;//雇员Id
    private String username;//雇员名
    private Integer workId;//员工工号
    private String workName;//部门职位
    private String role;//雇员权限（user/department）
    private Integer iswork;//是否分配部门
    private Integer istrain;//是否去培训
}
