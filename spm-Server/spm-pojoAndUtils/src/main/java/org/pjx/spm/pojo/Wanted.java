package org.pjx.spm.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @Author Glow
 * @Date 2022-04-14 13:24:23
 * @Description 想求职的人
 * @Version 1.0
 */
@Data
@Getter
@Setter
@Component
public class Wanted {
    private Integer wantedrage;//应聘序列
    private String username;//员工姓名
    private String idcard;//员工身份证
    private String gender;//员工行吧
    private String email;//员工邮箱
    private String role;//员工权限
    private String politicaltype;//员工身份面貌
    private String nationId;//员工族裔
    private String address;//员工地址
    private String phone;//员工号码
    private String education;//员工最高学历
    private String educationsubject;//员工最高学历毕业专业
    private String school;//员工毕业院校
    private Integer departmentId;//员工部门id
    private String departmentName;//员工部门名
    private String workName;//员工职位
    private String workExperience;//求职工作经历及其介绍
    private double baseSalary;//员工基本工资
    private int isaccept;//是否审批完毕（0/未审核 1/面试成功 2/审核通过正在面试 3/面试失败打包回家
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",locale = "zh",timezone = "GMT+8")
    private Date wantedDate;//求职时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",locale = "zh",timezone = "GMT+8")
    private Date getDate;//入职时间
}
