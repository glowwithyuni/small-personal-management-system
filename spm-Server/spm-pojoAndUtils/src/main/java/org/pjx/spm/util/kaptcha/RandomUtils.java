package org.pjx.spm.util.kaptcha;

import java.awt.*;
import java.util.Random;

/**
 * @Author Glow
 * @Date 2022-01-29 20:46:29
 * @Description 验证码生成工具类
 * @Version 1.0
 */
public class RandomUtils  extends org.apache.commons.lang3.RandomUtils{
    private static final char[] codes = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J',
            'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
            'X', 'Y', 'Z', '2', '3', '4', '5', '6', '7', '8', '9' };
    private  static Random random = new Random();
    private static final char[] number_codes = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
    /**
     * 生成随机字符验证码的字符，
     * @param length 传入多少个数就有多少位
     * @return
     */
    public static String randomString(int length)
    {
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0 ; i < length ; i++)
        {
            stringBuilder.append(String.valueOf(codes[random.nextInt(codes.length)]));
        }
        return stringBuilder.toString();
    }

    /**
     * 生成随机数字验证码的字符
     * @param length 传入多少个数就有多少位
     * @return
     */
    public static String randomNumberString(int length)
    {
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0 ; i < length ; i++)
        {
            stringBuilder.append(String.valueOf(number_codes[random.nextInt(codes.length)]));
        }
        return stringBuilder.toString();
    }
    /**
     * 随机rgb三色 #000000 16位的10位表示
     * @param x
     * @param y
     * @return
     */
    public static Color randomColor(int x, int y)
    {
        int x1 = x;
        int y1 = y;
        if(x1 > 255) {
            x1 = 255;
        }
        if(y1 > 255)
        {
            y1 = 255;
        }

        return new Color(x1+random.nextInt(y1-x1),x1+random.nextInt(y1-x1),x1+random.nextInt(y1-x1));
    }

    public static int nextInt(int bound)
    {
        return random.nextInt(bound);
    }

}
