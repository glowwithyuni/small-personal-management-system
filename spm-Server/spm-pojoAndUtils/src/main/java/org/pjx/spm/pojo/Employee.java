package org.pjx.spm.pojo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

/**
 * @Author Glow
 * @Date 2022-02-16 15:49:00
 * @Description
 * @Version 1.0
 */
@Data
@Getter
@Setter
@Component

public class Employee {
    private String userId;//员工ID
    private String userName;//员工姓名
    private String idcard;//员工身份证
    private String gender;//员工行吧
    private String email;//员工邮箱
    private String role;//员工权限
    private String politicaltype;//员工身份面貌
    private String nationId;//员工族裔
    private String address;//员工地址
    private String phone;//员工号码
    private String education;//员工最高学历
    private String educationsubject;//员工最高学历毕业专业
    private String school;//员工毕业院校
    private Integer departmentId;//员工部门id
    private String departmentName;//员工部门名
    private Integer workId;//员工工号
    private String workName;//员工职位
    private double baseSalary;//员工基本工资
}
