package org.pjx.spm.pojo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author Glow
 * @Date 2022-03-05 09:32:54
 * @Description 员工薪资
 * @Version 1.0
 */
@Data
@Getter
@Setter
public class Employeesalary {
    private String userId;
    private String username;
    private String baseSalary;
    private String translationSalary;
    private String punishmentSalary;
    private String rewardSalary;
    private String totalSalary;
}
